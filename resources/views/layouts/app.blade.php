<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('template/img/apple-icon.png') }}">
    <link rel="icon" type="image/ico" href="{{ asset('images/malibu-logo.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Malibu Boats {{ ucfirst(config('country')->country_name) }}</title>

    <!-- START SEO -->
    <meta name="author" content="Malibu Boats Asia">
    <meta name="description"
          content="Malibu Boats Asia: The World's Best Wakesurfing, Wakeboarding & Water Skiing Towboats, #1 in Quality, Luxury & Performance now available in Asia & Middle East">
    <meta name="keywords"
          content="malibu, axis, boat, malibuboats, axiswake, malibuboatsasia, axiswakeasia, wakesurfing, wakeboarding, asia, middle east">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>

    <meta name="og:title" content="Malibu Boats Asia"/>
    <meta name="og:type" content="website"/>
    <meta name="og:url" content="https://www.malibuboatsasia.com"/>
    <meta name="og:image" content="https://www.malibuboatsasia.com/images/logo-malibu.png"/>
    <meta name="og:site_name" content="Malibu Boats Asia"/>
    <meta name="og:description"
          content="Malibu Boats Asia: The World's Best Wakesurfing, Wakeboarding & Water Skiing Towboats, #1 in Quality, Luxury & Performance now available in Asia & Middle East"/>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "Organization",
          "url": "https://www.malibuboatsasia.com",
          "name": "MalibuBoatsAsia",
          "description": "Malibu Boats Asia: The World's Best Wakesurfing, Wakeboarding & Water Skiing Towboats, #1 in Quality, Luxury & Performance now available in Asia & Middle East",
          "image": [
            "https://www.malibuboatsasia.com/images/malibu-logo.png",
           ]
        }
    </script>
    <!-- END SEO -->

    <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <style>
        @font-face {
            font-family: "Forza-Bold";
            src: url("{{ asset('template/fonts/Forza-Bold.otf') }}");
        }

        @font-face {
            font-family: "PxGrotesk-Light";
            src: url("{{ asset('template/fonts/PxGrotesk-Light.otf') }}");
        }
    </style>
    <link rel="stylesheet" href="{{ asset('template/css/paper-kit.css?v=1.0.1') }}"/>
    <link rel="stylesheet" href="{{ asset('template/css/custom.css?v=1.2.4') }}">

    <!-- Fast Loading -->
    <noscript>
        <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker.min.css') }}">
    </noscript>

    <script>
        var giftofspeed = document.createElement('link');
        giftofspeed.rel = 'stylesheet';
        giftofspeed.href = "{{ asset('template/css/font-awesome.min.css') }}";
        var godefer = document.getElementsByTagName('link')[0];
        godefer.parentNode.insertBefore(giftofspeed, godefer);

        var giftofspeed2 = document.createElement('link');
        giftofspeed2.rel = 'stylesheet';
        giftofspeed2.href = "{{ asset('plugins/bootstrap-datepicker.min.css') }}";
        var godefer2 = document.getElementsByTagName('link')[0];
        godefer2.parentNode.insertBefore(giftofspeed2, godefer2);
    </script>
    <!-- End Fast Loading -->
    <style>
        p {
            margin-bottom: 15px !important;
        }
    </style>
    @stack ('css')
</head>

<body class="index-page sidebar-collapse">

@php
    clientSessionCountryId();
@endphp

<div class="page-loading">
    <div class="logo-loading">
        <img src="{{ asset('images/malibu-logo.png') }}" class="logo-file" alt="logo">
        <div class="text-center">
            <h4 class="font-fz-b">
                <span class="text-white">Loading</span>
                <span class="text-loading" data-text=".....">.....</span>
            </h4>
        </div>
    </div>
</div>
<div class="page-completed" style="display: none;">
    @include ('layouts.components.navbar')

    @yield ('content')

    @if (config('country')->country_name == 'asia')
        @include ('layouts.components.contact-us')
    @else
        @if (\Request::route()->getName() != "contact-dealer")
            @include ('layouts.components.contact-dealer')
        @endif
    @endif

    @include ('layouts.components.footer')
    @include ('layouts.components.modals.language')
</div>

</body>

<!--   Core JS Files   -->
<script src="{{ asset('template/js/core/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/js/core/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ asset('template/js/plugins/bootstrap-switch.js') }}"></script>
<!--  Plugin for the Sliders, full documentation here: https://refreshless.com/nouislider/ -->
{{--<script src="{{ asset('template/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>--}}
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{ asset('template/js/plugins/moment.min.js') }}"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
{{--<script src="{{ asset('template/js/plugins/bootstrap-tagsinput.js') }}"></script>--}}
<!--	Plugin for Select, full documentation here: https://silviomoreto.github.io/bootstrap-select -->
{{--<script src="{{ asset('template/js/plugins/bootstrap-selectpicker.js') }}" type="text/javascript"></script>--}}
<!--	Plugin for Datetimepicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{ asset('template/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
<!--  Vertical nav - dots -->
<!--  Photoswipe files -->
{{--<script src="{{ asset('template/js/plugins/photo_swipe/photoswipe.min.js') }}"></script>--}}
{{--<script src="{{ asset('template/js/plugins/photo_swipe/photoswipe-ui-default.min.js') }}"></script>--}}
{{--<script src="{{ asset('template/js/plugins/photo_swipe/init-gallery.js') }}"></script>--}}
<!--  for Jasny fileupload -->
<script src="{{ asset('template/js/plugins/jasny-bootstrap.min.js') }}"></script>
<!-- Control Center for Paper Kit: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('template/js/paper-kit.js?v=1.2.2') }}" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--}}
<!--  Plugin for presentation page - isometric cards  -->
{{--<script src="{{ asset('template/js/plugins/presentation-page/main.js') }}"></script>--}}

<!-- bootstrap-datepicker 1.8.0 -->
<script src="{{ asset('plugins/bootstrap-datepicker.min.js') }}"></script>

<!-- New Design -->
{{--<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>--}}
{{--<script src="{{ asset('template/js/new_design.js') }}"></script>--}}
<!-- End New Design -->

<script src="{{ asset('plugins/sweetalert.min.js') }}"></script>

@stack ('scripts')

<script>
    @if (session()->has('send_mail_contact_us'))
    swal('Thank you for contacting Malibu Boats Asia. We will get in touch with you shortly.', '', 'success');
    @endif

    @if (session('send_mail_contact_dealer') == 'free-test-drive')
    swal('Thank you for contacting Malibu Boats {{ ucfirst(clientOriginalCountryName()) }}. We will get in touch with you shortly.', '', 'success');
    @endif

    @if (session('send_mail_contact_dealer') == 'request-catalog')
    swal('Thank you for contacting Malibu Boats {{ ucfirst(clientOriginalCountryName()) }}. We will get in touch with you shortly.', '', 'success');
    @endif

    @if (session('send_mail_contact_dealer') == 'general')
    swal('Thank you for contacting Malibu Boats {{ ucfirst(clientOriginalCountryName()) }}. We will get in touch with you shortly.', '', 'success');
    @endif

    @if (session('send_mail_contact_dealer') == 'inventory')
    swal('Thank you for contacting Malibu Boats {{ ucfirst(clientOriginalCountryName()) }}. We will get in touch with you shortly.', '', 'success');
    @endif
</script>

<!-- Plugins -->
<script>
    $('.date').datepicker({
        'format': 'yyyy-m-d',
        'autoclose': true
    });

    $(window).on("load", function (e) {
        $('.page-loading').hide();
        $('.page-completed').show();

        // console.log('>>>>> begin');
        // console.log('clientWidth : ', document.body.clientWidth);
        // console.log('width : ', screen.width);
        // console.log('>>>>> end begin');

        /* init navbar */
        let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        // console.log(isMobile);
        let width = screen.width;
        if (isMobile) {
            if (width > 768) {
                document.getElementById('navbar-mobile').style.visibility = 'hidden';
                document.getElementById('navbar-desktop').style.visibility = 'visible';
            } else {
                document.getElementById('navbar-desktop').style.visibility = 'hidden';
                document.getElementById('navbar-mobile').style.visibility = 'visible';
            }
        } else {
            let clientWidth = document.body.clientWidth;
            // console.log(clientWidth);
            // if (clientWidth >= 990 && clientWidth <= 1050) {
            //     $('#navbar-mobile').css('visibility', 'hidden');
            //     $('#navbar-desktop').css('visibility', 'visible');
            //     console.log('>>>>>>>>>>>>>>>>>>>> init true', clientWidth)
            // } else {
            //     console.log('>>>>>>>>>>>>>>>>>>>> init false', clientWidth)
            // if (clientWidth <= 900) {
            //     document.getElementById('navbar-mobile').style.visibility = 'visible';
            //     document.getElementById('navbar-desktop').style.visibility = 'hidden';
            // } else {
            //     document.getElementById('navbar-mobile').style.visibility = 'hidden';
            //     document.getElementById('navbar-desktop').style.visibility = 'visible';
            // }
            // }
            // if (clientWidth >= 970 && clientWidth <= 1200) {
            //     document.getElementById('navbar-desktop').style.visibility = 'visible';
            //     console.log('>= 900 and <= 1200')
            // } else {
            //     document.getElementById('navbar-mobile').style.visibility = 'visible';
            //     console.log('> 1200')
            // }
            //

            if (clientWidth >= 1200) {
                document.getElementById('navbar-desktop').style.visibility = 'visible';
            } else {
                document.getElementById('navbar-mobile').style.visibility = 'visible';
            }

            window.addEventListener("resize", function (event) {
                // let clientWidth = $(window).width();
                //     console.log(clientWidth);
                //     //if (clientWidth >= 970 && clientWidth <= 1200) {
                document.getElementById('navbar-mobile').style.visibility = 'hidden';
                document.getElementById('navbar-desktop').style.visibility = 'hidden';
                // $('#navbar-mobile').css('background-color', 'transparent');
                // $('#navbar-desktop').css('background-color', 'transparent');

                let clientWidth = document.body.clientWidth;
                if (clientWidth === 1200) {
                    document.getElementById('navbar-desktop').style.visibility = 'visible';
                    // $('#navbar-desktop').css('background-color', 'rgb(11, 18, 22)');
                } else {
                    if (clientWidth < 1200) {
                        // console.log('>>>> should be mobile size');
                        document.getElementById('navbar-mobile').style.visibility = 'visible';
                        // $('#navbar-mobile').css('background-color', 'rgb(11, 18, 22)');
                    } else {
                        // console.log('>>>> should be desktop size');
                        document.getElementById('navbar-desktop').style.visibility = 'visible';
                        // $('#navbar-desktop').css('background-color', 'rgb(11, 18, 22)');
                    }
                }
            });
        }

    });
</script>
<!-- End Plugins -->

</html>
