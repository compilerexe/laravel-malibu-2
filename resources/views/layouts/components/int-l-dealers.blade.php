@push ('css')
    <style>
        h2 {
            font-family: "Forza-Bold", sans-serif !important;
        }
    </style>
@endpush

<div class="row">

    @foreach ($dealer_profile as $profile)

        <div class="col-12 col-sm-6">
            <div class="form-group">
                <h2 style="text-transform: uppercase">
                    {{ $profile->dealer->country->country_name }}
                </h2>
                <br>
                {!! $profile->fullAddressEnglish() !!}
            </div>
        </div>

    @endforeach

</div>

