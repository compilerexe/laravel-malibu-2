<section class="section bg-dark-blue" style="padding: 0;">
    <div class="container" style="padding-bottom: 40px;">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto">

                <div class="text-center">
                    <h2 class="title sub-header text-white font-fz-b">{{ lang_contact_us()->contact_us }}</h2>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-8 ml-auto mr-auto">
                        <form action="{{ route('mail.send.contact-us') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="font-fz-b text-white">{{ lang_contact_us()->name }}</label>
                                <input type="text" name="name" id="name" class="form-control"
                                       placeholder="{{ lang_contact_us()->name }}"
                                       required>
                            </div>

                            <div class="form-group">
                                <label for="email" class="font-fz-b text-white">{{ lang_contact_us()->email }}</label>
                                <input type="email" name="email" id="email" class="form-control"
                                       placeholder="{{ lang_contact_us()->email }}"
                                       required>
                            </div>

                            <div class="form-group">
                                <label for="phone-number"
                                       class="font-fz-b text-white">{{ lang_contact_us()->phone_number }}</label>
                                <input type="text" name="phone_number" id="phone-number" class="form-control"
                                       placeholder="{{ lang_contact_us()->phone_number }}" required>
                            </div>

                            <div class="form-group">
                                <label for="country"
                                       class="font-fz-b text-white">{{ lang_contact_us()->country }}</label>
                                <input type="text" name="country" id="country" class="form-control"
                                       placeholder="{{ lang_contact_us()->country }}"
                                       required>
                            </div>

                            <div class="form-group">
                                <label for="reason-for-contact"
                                       class="font-fz-b text-white">{{ lang_contact_us()->reason_for_contact }}</label>
                                <textarea name="reason_for_contact" id="reason-for-contact" class="form-control"
                                          placeholder="{{ lang_contact_us()->reason_for_contact }}"
                                          rows="5" required></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-malibu"
                                        style="padding: 15px 50px 15px 50px">
                                    {{ lang_button()->submit }}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
