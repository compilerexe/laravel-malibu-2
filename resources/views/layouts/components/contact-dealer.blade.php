<section class="section bg-dark-blue" style="padding: 0;">
    <div class="container" style="padding-bottom: 40px;">
        <div class="row">
            <div class="col-12 col-sm-8 ml-auto mr-auto">

                <div class="text-center">
                    <h2 class="title sub-header text-white font-fz-b">
                        {{ lang_contact_dealer()->contact_dealer }}
                    </h2>
                </div>

                <br>
                <p style="color: white !important;">
                    @php
                        $dealer = clientGetDealer();
                        $facebook_link = $dealer->profile->facebook_link;
                        $instagram_link = $dealer->profile->instagram_link;
                        $website_link = $dealer->profile->website_link;
                    @endphp
                    @if (session('flag_en_'.config('country')->short_name))
                        {!! $dealer->profile->short_description_en !!}
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer/#section-about-dealer') }}">
                            {{ lang_button()->read_more }}
                        </a>
                        <br>
                        {{ $dealer->profile->telephone }}
                        <br>
                        {{ $dealer->profile->email }}
                        <br>
                        {{ $dealer->profile->address_en }}
                    @else
                        {!! $dealer->profile->short_description_local !!}
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer/#section-about-dealer') }}">
                            {{ lang_button()->read_more }}
                        </a>
                        <br>
                        {{ $dealer->profile->telephone }}
                        <br>
                        {{ $dealer->profile->email }}
                        <br>
                        {{ $dealer->profile->address_local }}
                    @endif
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer/#section-map') }}">
                        {{ lang_button()->get_direction }}
                    </a>
                </p>
                <br>

                <div class="text-center">
                    <p style="color: white !important;">


                        <a href="{{ $facebook_link }}" target="_blank"
                           style="margin-right: 20px;">
                            <img src="{{ asset('images/icons/fb.png') }}" alt=""
                                 style="width: 32px; height: 32px;">
                        </a>

                        <a href="{{ $instagram_link }}" target="_blank"
                           style="margin-right: 20px;">
                            <img src="{{ asset('images/icons/ig.png') }}" alt=""
                                 style="width: 32px; height: 32px;">
                        </a>

                        <a href="{{ $website_link }}" target="_blank"
                           style="margin-right: 20px;">
                            <img src="{{ asset('images/icons/website.png') }}" alt=""
                                 style="width: 32px; height: 32px;">
                        </a>

                        <a href="javascript:void(0)" id="btn-contact-us">
                            <img src="{{ asset('images/icons/contact-us.png') }}" alt=""
                                 style="width: 32px; height: 32px;">
                        </a>

                    </p>
                </div>
            </div>
        </div>

        <div class="desktop">
            <div class="container">
                <div class="d-flex justify-content-center">
                    <div class="p-2">
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer#form-section') }}">
                            <button type="button" class="btn btn-malibu btn-contact-dealer-test-drive"
                                    style="color: white; font-size: 18px; margin-top: 20px; //width: 100% !important;">
                                {{ lang_button()->test_drive }}
                            </button>
                        </a>
                    </div>
                    <div class="p-2">
                        <a class="btn btn-malibu"
                           href="https://www.malibuboats.com/boat-configurator"
                           target="_blank"
                           style="color: white; font-size: 18px; margin-top: 20px; //width: 100% !important;">
                            {{ lang_button()->build_a_boat }}
                        </a>
                    </div>
                    <div class="p-2">
                        <a class="btn btn-malibu btn-contact-dealer-request-catalog"
                           style="color: white; font-size: 18px; margin-top: 20px; //width: 100% !important;">
                            {{ lang_button()->request_a_catalog }}
                        </a>
                    </div>
                    <div class="p-2">
                        <a class="btn btn-malibu"
                           href="{{ clientNavbarURL(config('country')->country_name, '/inventory') }}"
                           style="color: white; font-size: 18px; margin-top: 20px; //width: 100% !important;">
                            {{ lang_button()->inventory }}
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="mobile" style="display: none;">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer#form-section') }}">
                            <button type="button" class="btn btn-malibu btn-contact-dealer-test-drive"
                                    style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                                {{ lang_button()->test_drive }}
                            </button>
                        </a>
                    </div>
                    <div class="col-12 text-center">
                        <a class="btn btn-malibu"
                           href="https://www.malibuboats.com/boat-configurator"
                           target="_blank"
                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                            {{ lang_button()->build_a_boat }}
                        </a>
                    </div>
                    <div class="col-12 text-center">
                        <a class="btn btn-malibu btn-contact-dealer-request-catalog"
                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                            {{ lang_button()->request_a_catalog }}
                        </a>
                    </div>
                    <div class="col-12 text-center">
                        <a class="btn btn-malibu"
                           href="{{ clientNavbarURL(config('country')->country_name, '/inventory') }}"
                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                            {{ lang_button()->inventory }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push ('scripts')
    <script>

        $('.btn-contact-dealer-request-catalog').on('click', function () {
            $.ajax({
                url: '{{ route('session.contact-dealer.store') }}',
                method: 'post',
                data: {
                    _token: '{{ csrf_token() }}',
                    active_form_section: 'form-request-a-catalog'
                },
                success: function () {
                    window.location.href = '{{ clientNavbarURL(config('country')->country_name, '/contact-dealer/#form-section') }}';
                }
            });
        });

        $('#btn-contact-us').on('click', function () {
            $.ajax({
                url: '{{ route('session.contact-dealer.store') }}',
                method: 'post',
                data: {
                    _token: '{{ csrf_token() }}',
                    active_form_section: 'form-general'
                },
                success: function () {
                    window.location.href = '{{ clientNavbarURL(config('country')->country_name, '/contact-dealer/#form-section') }}';
                }
            });
        });

    </script>
@endpush
