<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top nav-down navbar-transparent" color-on-scroll="500" id="navbar-mobile" style="visibility: hidden;">
    <!-- 1a start -->
    <div class="container-fluid justify-end">
        <a class="mobile-brand-center" href="{{ clientNavbarURL(config('country')->country_name, '/') }}">
            <img src="{{ asset('images/logo-malibu.png') }}" alt="malibu" style="height: 40px;">
        </a>
        <div class="navbar-translate width-auto">
            <div style="height: 40px;"></div>
            <!-- 1a end -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav" style="height: auto;">
                <li class="nav-item">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/all-boats') }}" class="nav-link font-fz-b"
                       aria-expanded="false" style="color: white !important;">
                        {{ lang_navbar()->boats }}
                    </a>
                </li>
                <li class="nav-item mr-auto">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/about-malibu') }}" class="nav-link font-fz-b"
                       aria-expanded="false" style="color: white !important;">
                        {{ lang_navbar()->about_malibu }}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/news') }}" class="nav-link font-fz-b"
                       aria-expanded="false" style="color: white !important;">
                        {{ lang_navbar()->news_and_events }}
                    </a>
                </li>

                <li class="nav-item">
                    @if (clientOriginalCountryName() == 'asia')
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/find-a-dealer') }}"
                           class="nav-link font-fz-b" aria-expanded="false" style="color: white !important;">
                            {{ lang_navbar()->find_a_dealer }}
                        </a>
                    @else
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer') }}"
                           class="nav-link font-fz-b"
                           aria-expanded="false" style="color: white !important;">
                            {{ lang_navbar()->contact_dealer }}
                        </a>
                    @endif
                </li>

                @if (config('country')->lang_switch == 1)
                    <li class="nav-item nav-link">
                        <a href="{{ url(urlCountryReplace(config('country')->country_name).'/set/lang/local') }}"
                           style="color: white !important;"
                           class="link-lang font-fz-b">
                            {{ config('country')->short_name }}
                        </a>
                        <span style="color: white !important;">|</span>
                        <a href="{{ url(urlCountryReplace(config('country')->country_name).'/set/lang/en') }}"
                           style="color: white !important;"
                           class="link-lang font-fz-b">
                            EN
                        </a>
                    </li>
                @else
                    @if (config('country')->country_name == 'asia')
                    <!-- 1b, 1c start -->
                        <li class="nav-link">
                            <a href="#" class="nav-link" area-expanded="false" data-toggle="modal"
                               data-target="#country-modal">
                                <img src="{{ asset('images/icons/globe-asia.png') }}" alt=""
                                     style="width: 16px; height: 16px;">
                            </a>
                        </li>
                        <!-- 1b, 1c end -->
                    @else
                        <li>
                            <a href="javascript:void(0)">
                                <img src="{{ config('country')->icon }}" alt="icon"
                                     style="width: 16px; height: 16px;">
                            </a>
                        </li>
                    @endif
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->

<!-- Desktop -->
<nav class="navbar navbar-expand-lg fixed-top nav-down navbar-transparent" color-on-scroll="500" id="navbar-desktop"
     style="visibility: hidden;">
    {{--<nav class="navbar navbar-expand-lg fixed-top nav-down navbar-transparent" color-on-scroll="500" id="navbar-desktop"--}}
         {{--style="//background-color: rgb(11, 18, 22); visibility: hidden;">--}}
    <div id="header">
        <a href="{{ clientNavbarURL(config('country')->country_name, '/') }}" class="logo">
            <img src="{{ asset('images/logo-malibu.png') }}" alt="" style="height: 40px; width: 105px;">
        </a>
        <ul>
            <li>
                <a href="{{ clientNavbarURL(config('country')->country_name, '/all-boats') }}">
                    {{ lang_navbar()->boats }}
                </a>
            </li>

            <li>
                <a href="{{ clientNavbarURL(config('country')->country_name, '/about-malibu') }}">
                    {{ lang_navbar()->about_malibu }}
                </a>
            </li>

            <li>
                <a href="{{ clientNavbarURL(config('country')->country_name, '/news') }}">
                    {{ lang_navbar()->news_and_events }}
                </a>
            </li>

            @if (config('country')->country_name == 'asia')
                <li>
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/find-a-dealer') }}">
                        {{ lang_navbar()->find_a_dealer }}
                    </a>
                </li>
            @else
                <li>
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer') }}">
                        {{ lang_navbar()->contact_dealer }}
                    </a>
                </li>
            @endif

            @if (config('country')->lang_switch == 1)
                <li style="padding-left: 5px; padding-right: 5px;">
                    <a href="{{ url(urlCountryReplace(config('country')->country_name).'/set/lang/local') }}">{{ config('country')->short_name }}</a>
                    <span style="color: white; padding: 5px;">|</span>
                    <a href="{{ url(urlCountryReplace(config('country')->country_name).'/set/lang/en') }}">EN</a>
                </li>
            @else
                @if (config('country')->country_name == 'asia')
                    <li>
                        <!-- 1b, 1c start -->
                        <a href="#" class="nav-link" area-expanded="false" data-toggle="modal"
                           data-target="#country-modal" style="padding: 0">
                            <img src="{{ asset('images/icons/globe-asia.png') }}" alt=""
                                 style="width: 16px; height: 16px;">
                        </a>
                        <!-- 1b, 1c end -->
                    </li>
                @else
                    <li>
                        <a href="javascript:void(0)" style="cursor:default;">
                            <img src="{{ config('country')->icon }}" alt="icon"
                                 style="width: 16px; height: 16px;">
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>

</nav>
<!-- End Desktop -->
