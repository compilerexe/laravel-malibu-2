@extends ('layouts.app')
@push ('css')
    <style>
        header {
            position: relative;
            /*background-color: #0b1216;*/
            background: url('{{ imageFolder($data['cover_image']) }}') center center no-repeat;
            background-size: cover;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;
            -webkit-background-size: cover;
            background-size: cover;
        }

        header video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        header .container {
            position: relative;
            z-index: 2;
        }

        header .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0.2;
            z-index: 1;
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                background: url('{{ imageFolder($data['cover_image']) }}') black no-repeat center center scroll;
            }

            header video {
                display: none;
            }
        }
    </style>
@endpush
@push ('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
@endpush
@section ('content')

    <section class="page-header" style="border-top: 0 !important;">
        <header>
            <div class="overlay"></div>
            {{--<video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">--}}
            {{--<source src="https://cdn.malibuboats.com/videos/models/2019/2019_M235_WebBack.mp4" type="video/mp4">--}}
            {{--</video>--}}
            <div class="container h-100">
                <div class="d-flex text-center h-100">
                    <div class="my-auto w-100 text-white">
                        <h1 class="display-3 title-header">
                            {{ $data['news_header'] }}
                        </h1>
                    </div>
                </div>
            </div>
        </header>
    </section>

    <section>
        <div class="container" style="padding-top: 60px; padding-bottom: 60px;">
            <div class="row">
                <div class="col-12 col-sm-6 ml-auto mr-auto detail-custom">
                    {!! $data['news_description'] !!}
                </div>
            </div>
        </div>
    </section>

@endsection
