@extends ('layouts.app')
@section ('content')

    <section class="page-header"
             style="background-image: url('https://cdn.malibuboats.com/images/allboats-header.jpg'); border-top: 0 !important;">
        <div class="filter"></div>
        <div class="content-center">
            <div class="motto">
                <h1 class="text-center title-header">{{ lang_all_boats()->all_boats }}</h1>
            </div>
        </div>
    </section>

    <section class="section bg-dark-blue">

        <div class="container">
            <div class="row all-boats-navbar"
                 style="border-top: 1px solid #F2F2F2; border-bottom: 1px solid #F2F2F2; padding-top: 20px">

                <div class="col-12 col-sm-1">
                    <div class="form-group">
                        <h5 class="font-fz-b active-white" id="show-all"
                            style="cursor: pointer !important;">{{ lang_all_boats()->all }}</h5>
                    </div>
                </div>
                <div class="col-12 col-sm-2">
                    <div class="form-group">
                        <h5 class="font-fz-b" id="show-wakesetter"
                            style="cursor: pointer !important;">{{ lang_all_boats()->wakesetter }}</h5>
                    </div>
                </div>
                <div class="col-12 col-sm-2">
                    <div class="form-group">
                        <h5 class="font-fz-b" id="show-response"
                            style="cursor: pointer !important;">{{ lang_all_boats()->response }}</h5>
                    </div>
                </div>
                <div class="col-12 col-sm-1">
                    <div class="form-group">
                        <h5 class="font-fz-b" id="show-m235"
                            style="cursor: pointer !important;">{{ lang_all_boats()->m235 }}</h5>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="form-group" style="padding-top: 30px; ">
                        <p class="text-white" id="nav-description"></p>
                    </div>
                </div>
            </div>

            <div class="row">

                @foreach ($boats as $boat)
                    <div class="col-12 col-sm-4 tag-{{ $boat->tag }}" style="margin-top: 30px;">
                        <div class="form-group all-boats-item" style="padding: 20px 20px 20px 20px">
                            <img src="{{ $boat->boat_front_image }}" alt="" class="img-fluid">
                            <br><br>
                            <small class="small-title font-fz-b  text-white">{{ $boat->tag }}</small>
                            <h3 class="sub-header font-fz-b text-white" style="margin: 0">{{ $boat->model }}</h3>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <a href="{{ clientNavbarURL(config('country')->country_name, '/boat/'.clientBoatUrl($boat->model)) }}"
                                           class="boat-model-explore">
                                            <button type="button" class="btn btn-malibu text-white"
                                                    style="padding-top: 10px; padding-bottom: 10px; margin-right: 10px; width: 100%">
                                                {{ lang_button()->explore }}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <a href="https://www.malibuboats.com/boat-configurator" target="_blank">
                                            <button type="button" class="btn btn-malibu text-white"
                                                    style="padding-top: 10px; padding-bottom: 10px; width: 100%">
                                                {{ lang_button()->build }}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

    </section>

    @push ('scripts')
        <script>
            $('#show-all').on('click', function () {
                $('.tag-Response').fadeIn();
                $('.tag-Wakesetter').fadeIn();
                $('.tag-M235').fadeIn();

                $(this).css('color', 'white');
                $('#show-wakesetter').css('color', '#66615B');
                $('#show-response').css('color', '#66615B');
                $('#show-m235').css('color', '#66615B');

                $('#nav-description').text(null);
            });

            $('#show-wakesetter').on('click', function () {
                $('.tag-Response').fadeOut();
                $('.tag-Wakesetter').fadeIn();
                $('.tag-M235').fadeOut();

                $('#show-all').css('color', '#66615B');
                $(this).css('color', 'white');
                $('#show-response').css('color', '#66615B');
                $('#show-m235').css('color', '#66615B');

                $.get('/api/all-boats/nav-description',
                    function (data, status) {
                        $('#nav-description').text(data.nav_description_wakesetter);
                    });
            });

            $('#show-response').on('click', function () {
                $('.tag-Response').fadeIn();
                $('.tag-Wakesetter').fadeOut();
                $('.tag-M235').fadeOut();

                $('#show-all').css('color', '#66615B');
                $('#show-wakesetter').css('color', '#66615B');
                $(this).css('color', 'white');
                $('#show-m235').css('color', '#66615B');

                $.get('/api/all-boats/nav-description',
                    function (data, status) {
                        $('#nav-description').text(data.nav_description_response);
                    });
            });

            $('#show-m235').on('click', function () {
                $('.tag-Response').fadeOut();
                $('.tag-Wakesetter').fadeOut();
                $('.tag-M235').fadeIn();

                $('#show-all').css('color', '#66615B');
                $('#show-wakesetter').css('color', '#66615B');
                $('#show-response').css('color', '#66615B');
                $(this).css('color', 'white');

                $.get('/api/all-boats/nav-description',
                    function (data, status) {
                        $('#nav-description').text(data.nav_description_m235);
                    });
            });
        </script>
    @endpush

@endsection
