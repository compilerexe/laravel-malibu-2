@extends ('layouts.app')
@php
    $slides = [];
    foreach (json_decode($inventory->boat_photos) as $key => $value) {
        array_push($slides, imageFolder($value->image));
    }

    if (session('flag_en_'.config('country')->short_name)) {
        $boat_detail = $inventory->boat_detail_en;
    } else {
        $boat_detail = $inventory->boat_detail_local;
    }
@endphp
@push ('css')
    <style>
        header {
            position: relative;
            background: url('{{ $slides[0] }}') center center no-repeat;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;
            -webkit-background-size: cover;
            background-size: cover;
        }

        header video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        header .container {
            position: relative;
            z-index: 2;
        }

        header .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0.2;
            z-index: 1;
        }

        .inventory-first-section {
            height: 100vh;
            width: 100%;
            object-fit: cover
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                background: url('{{ $slides[0] }}') center center no-repeat;
            }

            header video {
                display: none;
            }

            .inventory-first-section {
                height: 50vh;
                width: 100%;
                object-fit: cover
            }
        }
    </style>
@endpush
@section ('content')

    <section class="section" style="padding: 0; margin: 0;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12" style="padding: 0">
                    <div class="page-carousel">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach ($slides as $item)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}"
                                        class="{{ ($loop->index == 0) ? 'active' : '' }}"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                @foreach ($slides as $url)
                                    <div class="carousel-item {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        <img class="inventory-first-section"
                                             src="{{ $url }}" alt="image">
                                    </div>
                                @endforeach
                            </div>
                            <a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators"
                               role="button" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators"
                               role="button" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section bg-dark-blue text-white">
        @php
            $explore = App\Models\ExploreBoats::where('boat_id', $inventory->boat_id)
            ->where('country_id', config('country')->id)->first();

            if (config('country')->lang_switch === 0) {
                $explore = json_decode($explore->lang_en);
            } else {
                if (session('flag_en_'.config('country')->short_name)) {
                    $explore = json_decode($explore->lang_en);
                } else {
                    $explore = json_decode($explore->lang_country);
                }
            }
        @endphp
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="from-group text-center">
                        <h2 class="font-fz-b">Malibu {{ $inventory->boat->model }}</h2>
                        <h3 class="font-fz-b" style="text-transform: uppercase;">
                            <i class="fa fa-map-marker"></i>
                            {{ config('country')->country_name }}
                            &ensp;
                            {{ lang_other()->year }} : {{ $inventory->year }}
                            &ensp;
                            {{ lang_other()->condition }} :
                            {{ ($inventory->used == 0) ? lang_other()->new : lang_other()->used }}
                            &ensp;
                            {{ lang_other()->price }} : {{ $inventory->price }}
                        </h3>
                    </div>
                    <br>

                    @if (config('country')->lang_switch === 0)
                        <div class="form-group">
                            {!! $inventory->boat_detail_en !!}
                        </div>
                    @else
                        @if (session('flag_en_'.config('country')->short_name))
                            <div class="form-group">
                                {!! $inventory->boat_detail_en !!}
                            </div>
                        @else
                            <div class="form-group">
                                {!! $inventory->boat_detail_local !!}
                            </div>
                        @endif
                    @endif

                </div>
            </div>
        </div>
    </section>

    <section class="section bg-dark-blue text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="from-group text-center">
                        <h2 class="font-fz-b">
                            Malibu
                            {{ $explore->first_section_header }}
                        </h2>
                    </div>
                    <br>
                    <div class="form-group">
                        <br>
                        <div class="form-group">
                            {!! $explore->second_section_description !!}
                        </div>
                    </div>
                    <br>
                    <div class="form-group text-center">
                        <a href="{{ clientNavbarURL(config('country')->country_name, '/boat/'.clientBoatUrl($inventory->boat->model)) }}">
                            <button type="button"
                                    class="btn btn-malibu text-white uppercase">{{ lang_button()->learn_more }}</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-dark-blue text-white" id="request-form">
        <div class="container">
            <div class="row" id="form-free-test-drive">
                <div class="col-12 col-sm-10 ml-auto mr-auto">
                    <div class="text-center">
                        <h2 class="title font-fz-b text-white">
                            {{ lang_other()->request_form }}
                        </h2>
                    </div>
                    <br>
                    <p class="font-p-l text-white">
                        {{ lang_other()->text_form_1 }}
                    </p>
                    <br>
                    <form action="{{ route('mail.send.contact-dealer.inventory') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="font-fz-b text-white">{{ lang_form_input()->name }}</label>
                            <input type="text" name="name" id="name" class="form-control"
                                   placeholder="{{ lang_form_input()->name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="email" class="font-fz-b text-white">{{ lang_form_input()->email }}</label>
                            <input type="email" name="email" id="email" class="form-control"
                                   placeholder="{{ lang_form_input()->email }}"
                                   required>
                        </div>

                        <div class="form-group">
                            <label for="phone-number"
                                   class="font-fz-b text-white">{{ lang_form_input()->phone_number }}</label>
                            <input type="text" name="phone_number" id="phone-number" class="form-control"
                                   placeholder="{{ lang_form_input()->phone_number }}" required>
                        </div>

                        {{--<div class="form-group date-input">--}}
                        {{--<label for="date" class="font-fz-b text-white">--}}
                        {{--{{ lang_form_input()->date }}--}}
                        {{--</label>--}}
                        {{--<input type="text" name="date" id="date" placeholder="MM/DD/YYYY" class="date form-control"--}}
                        {{--required>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label for="reason-for-contact"
                                   class="font-fz-b text-white">{{ lang_form_input()->what_is_your_request }}</label>
                            <textarea name="reason-for-contact" id="reason-for-contact" class="form-control"
                                      placeholder="{{ lang_form_input()->what_is_your_request }}"
                                      rows="5" required></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-malibu"
                                    style="padding: 15px 50px 15px 50px">
                                {{ lang_button()->submit }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
