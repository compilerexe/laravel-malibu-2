@extends ('layouts.app')
@section ('content')

    <section class="page-header"
             style="background-image: url('https://cdn.malibuboats.com/images/hero-image-semi.jpg'); border-top: 0 !important;">
        <div class="filter"></div>
        <div class="content-center">
            <div class="motto">
                <h1 class="text-center title-header">
                    {{ lang_find_a_dealer()->find_a_dealer }}
                </h1>
            </div>
        </div>
    </section>

    <section class="section secion-blog cd-section bg-dark-blue" id="blogs">

        <div class="blog-1" id="blog-1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 text-center ml-auto mr-auto">

                        <div class="row">
                            {{--<div class="ml-auto mr-auto col-sm-4">--}}
                            {{--<br><br>--}}
                            {{--<button type="button" id="view-dealer-list" class="btn btn-malibu font-fz-b">--}}
                            {{--VIEW DEALER LIST--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            <div class="ml-auto mr-auto col-sm-4">
                                <br><br>
                                <button type="button" id="view-intl-dealers" class="btn btn-malibu font-fz-b">
                                    {{ lang_find_a_dealer()->view_asia_dealers }}
                                    {{--<i class="fa fa-angle-down"></i>--}}
                                </button>
                            </div>
                        </div>

                        <br><br>

                        <div class="row" id="intl-dealers" style="display: none;">
                            <div class="col-12 col-sm-12">
                                <div class="text-left text-white">
                                    @include ('layouts.components.int-l-dealers')
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

@push ('scripts')

    <script>
        $('#view-dealer-list').on('click', function () {
            $('#intl-dealers').fadeOut();
            $('#dealer-list').fadeToggle();
        });

        $('#view-intl-dealers').on('click', function () {
            $('#dealer-list').fadeOut();
            $('#intl-dealers').fadeToggle();
        });
    </script>

@endpush
