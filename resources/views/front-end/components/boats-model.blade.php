<div id="menu-boats-model" style="display: none;">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 bg-dark-blue" id="boat-models">
                <ul>
                    <li class="active">
                        <a href="javascript:void(0)" class="active">ALL</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">WAKESETTER</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">RESPONSE</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">M235</a>
                    </li>
                    <li style="float: right;">
                        <a href="javascript:void(0)" id="close-boats-model">
                            <i class="fa fa-close fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            @for ($i = 1; $i <= 5; $i++)
                <div class="col-sm-2 text-center boat-display" style="border: 1px solid #1E272D;">
                    <br>
                    <h6 class="text-light-blue">response</h6>
                    <img src="https://cdn.malibuboats.com/images/models/response-txi-mo/navigation-rear.png"
                         alt=""
                         class="img-fluid" style="padding: 10px 10px 10px 10px;">

                    <a href="{{ route('boat.response-txi-mo') }}">
                        <button type="button" class="btn btn-primary"
                                style="font-size: 26px; text-transform: none; color: white;">TXiMO
                        </button>
                    </a>
                </div>
            @endfor
            <div class="col-sm-2 text-center boat-display" style="border: 1px solid #1E272D;">
                <br><br><br><br><br>
                <a href="javascript:void(0)" style="text-decoration: none;">
                    <i class="fa fa-angle-double-right fa-4x"></i>
                </a>
                <br><br><br><br><br><br>
            </div>
        </div>
    </div>

</div>