<div id="stay-in-the-loop-content" style="display: none;">

    <div class="container-fluid" style="border-top: 1px solid #1D272D;">
        <div class="row" style="padding: 40px 20px 40px 20px;">
            <div class="col-sm-6 bg-dark-blue text-center" style="font-family: Forza-Bold, sans-serif;">
                Join us to stay up to date with the latest Malibu<br> models, stories, Malibu events and Pro team news.
            </div>
            <div class="col-sm-6 bg-dark-blue">
                <div class="row">
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-lg" placeholder="Email"
                               style="background-color: transparent; color: white; font-size: 16px;">
                    </div>
                    <div class="col-sm-6">
                        <button type="button"
                                style="width: 150px; height: 50px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; background-color: #189BC5; border-color: #189BC5; font-family: Forza-Bold, sans-serif;">
                            SUBMIT
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
