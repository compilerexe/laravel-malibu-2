@extends ('layouts.app')
@section ('content')

    <section class="page-header"
             style="background-image: url('https://cdn.malibuboats.com/2018/News/news_header_18.jpg'); border-top: 0 !important;">
        <div class="filter"></div>
        <div class="content-center">
            <div class="motto">
                <h1 class="text-center title-header">{{ lang_news_and_events()->news_and_events }}</h1>
            </div>
        </div>
    </section>

    <section class="section secion-blog cd-section" id="blogs">

        <div class="blog-1" id="blog-1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 ml-auto mr-auto">
                        <h2 class="title sub-header font-fz-b">{{ lang_news_and_events()->malibu_news }}</h2>
                        <br>

                        @foreach ($news as $data)

                            @php
                                $link = clientNavbarURL(config('country')->country_name, '/news/'.$data->id);
                                if (config('country')->lang_switch === 0) {
                                        $news_header = $data->news_header_en;
                                        $news_description = $data->news_description_en;
                                } else {
                                    if (session('flag_en_'.config('country')->short_name) || config('country')->country_name == 'asia') {
                                        $news_header = $data->news_header_en;
                                        $news_description = $data->news_description_en;
                                    } else {
                                        $news_header = $data->news_header_local;
                                        $news_description = $data->news_description_local;
                                    }
                                }
                            @endphp

                            <div class="card card-plain card-blog">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-5 col-lg-5">
                                        <a href="{{ $link }}"
                                           class="font-fz-b">
                                            <div class="card-image" style="padding-top: 15px;">
                                                <img class="img"
                                                     src="{{ imageFolder($data->cover_image) }}"
                                                     style="width: 367px; height: 245px;">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-7 col-lg-7">
                                        <div class="card-body" style="padding-top: 0; margin-top: 0;">
                                            <h3 class="card-title">
                                                <a href="{{ $link }}"
                                                   class="content-title font-fz-b">
                                                    {{ $news_header }}
                                                </a>
                                            </h3>
                                            <p class="card-description">
                                                {{ strip_tags(str_limit($news_description, 300)) }}
                                            </p>
                                            <br>
                                            <a href="{{ $link }}"
                                               class="font-fz-b">
                                                <button type="button" class="btn btn-malibu" style="color: #179BC5">
                                                    {{ lang_button()->see_details }}
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                        @endforeach

                        <div id="show-more-news"></div>

                        @if ($showMore)
                            <div class="form-group text-center" id="form-show-more">
                                <button type="button" id="btn-show-more"
                                        class="btn btn-malibu text-dark">{{ lang_button()->show_more }}</button>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

@push ('scripts')

    <script>
        let pageStart = 0;

        $('#btn-show-more').on('click', function () {
            pageStart = pageStart + 1;
            $.post('{{ route('api.news.paginate') }}', {
                _token: '{{ csrf_token() }}',
                page: pageStart
            }).done(function (data, status) {

                if (!data[1]) {
                    $('#btn-show-more').hide();
                }

                $('#show-more-news').hide();

                data[0].forEach(news => {
                    $('#show-more-news').append(news);
                });
                $('#show-more-news').fadeIn();
            });
        });
    </script>

@endpush
