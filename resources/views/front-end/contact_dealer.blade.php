@extends ('layouts.app')
@section ('content')

    <section class="section bg-dark-blue" id="section-map">

        <div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 text-center ml-auto mr-auto">
                        <h2 class="title sub-header font-fz-b text-white">
                            {{ lang_contact_dealer()->contact_dealer }}
                        </h2>

                        <iframe
                            src="https://www.google.com/maps/embed?pb={{ $google_maps->source_code }}"
                            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                        <br>
                        <br>

                        <div class="text-left text-white">
                            <p class="font-fz-b">
                                @if (session('flag_en_'.config('country')->short_name))
                                    {!! $dealer_profile->fullAddressEnglish() !!}
                                @else
                                    {!! $dealer_profile->fullAddressLocal() !!}
                                @endif
                            </p>
                            <br>

                            <div class="row">
                                <div class="col-12 col-sm-3">
                                    <div class="form-group text-center">
                                        <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer#form-section') }}">
                                            <button type="button" class="btn btn-malibu btn-contact-dealer-test-drive"
                                                    style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                                                {{ lang_button()->test_drive }}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <div class="form-group text-center">
                                        <a class="btn btn-malibu"
                                           href="https://www.malibuboats.com/boat-configurator"
                                           target="_blank"
                                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                                            {{ lang_button()->build_a_boat }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <div class="form-group text-center">
                                        <a class="btn btn-malibu btn-contact-dealer-request-catalog"
                                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                                            {{ lang_button()->request_a_catalog }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <div class="form-group text-center">
                                        <a class="btn btn-malibu"
                                           href="{{ clientNavbarURL(config('country')->country_name, '/inventory') }}"
                                           style="color: white; font-size: 18px; margin-top: 20px; width: 100% !important;">
                                            {{ lang_button()->inventory }}
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="section bg-dark-blue" id="form-section">

        <div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">

                        <div class="row">
                            <div class="col-12 col-sm-4 text-center">
                                <a href="javascript:void(0)" style="color: #66615B;" id="btn-free-test-drive">
                                    <span class="font-fz-b text-sky" id="contact-m-free-test-drive">
                                        {{ lang_contact_dealer()->free_test_drive }}
                                    </span>
                                </a>
                            </div>
                            <div class="col-12 col-sm-4 text-center">
                                <a href="javascript:void(0)" style="color: #66615B;" id="btn-request-a-catalog">
                                    <span class="font-fz-b" id="contact-m-request-a-catalog">
                                        {{ lang_contact_dealer()->request_a_catalog }}
                                    </span>
                                </a>
                            </div>
                            <div class="col-12 col-sm-4 text-center">
                                <a href="javascript:void(0)" style="color: #66615B;" id="btn-general">
                                    <span class="font-fz-b" id="contact-m-general">
                                        {{ lang_contact_dealer()->general }}
                                    </span>
                                </a>
                            </div>
                        </div>

                        <hr style="border-color: #66615B;">

                        <!-- CONTENT FREE TEST DRIVE -->

                        <div class="row" id="form-free-test-drive">
                            <div class="col-12 col-sm-10 ml-auto mr-auto">
                                <br>
                                <p class="font-p-l text-white">
                                    {{ lang_contact_dealer()->free_test_drive_content }}
                                </p>
                                <br>
                                <form action="{{ route('mail.send.contact-dealer.free-test-drive') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="free-test-drive-name"
                                               class="font-fz-b text-white">{{ lang_form_input()->name }}</label>
                                        <input type="text" name="name" id="free-test-drive-name" class="form-control"
                                               placeholder="{{ lang_form_input()->name }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="free-test-drive-email"
                                               class="font-fz-b text-white">{{ lang_form_input()->email }}</label>
                                        <input type="email" name="email" id="free-test-drive-email" class="form-control"
                                               placeholder="{{ lang_form_input()->email }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="free-test-drive-phone-number"
                                               class="font-fz-b text-white">{{ lang_form_input()->phone_number }}</label>
                                        <input type="text" name="phone_number" id="free-test-drive-phone-number"
                                               class="form-control"
                                               placeholder="{{ lang_form_input()->phone_number }}" required>
                                    </div>

                                    <div class="form-group date-input">
                                        <label for="free-test-drive-date" class="font-fz-b text-white">
                                            {{ lang_form_input()->date }}
                                        </label>
                                        <input type="text" name="date" id="free-test-drive-date"
                                               placeholder="MM/DD/YYYY"
                                               class="date form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="free-test-drive-reason-for-contact"
                                               class="font-fz-b text-white">{{ lang_form_input()->reason_for_test_drive }}</label>
                                        <textarea name="reason_for_contact" id="free-test-drive-reason-for-contact"
                                                  class="form-control"
                                                  placeholder="{{ lang_form_input()->reason_for_test_drive }}"
                                                  rows="5" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-malibu"
                                                style="padding: 15px 50px 15px 50px">
                                            {{ lang_button()->submit }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- CONTENT REQUEST A CATALOG -->

                        <div class="row" id="form-request-a-catalog" style="display: none;">
                            <div class="col-12 col-sm-10 ml-auto mr-auto">
                                <br>
                                <p class="font-p-l text-white">
                                    {{ lang_contact_dealer()->request_a_catalog_content }}
                                </p>
                                <br>
                                <form action="{{ route('mail.send.contact-dealer.request-catalog') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="request-catalog-name"
                                               class="font-fz-b text-white">{{ lang_form_input()->name }}</label>
                                        <input type="text" name="name" id="request-catalog-name" class="form-control"
                                               placeholder="{{ lang_form_input()->name }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="request-catalog-email"
                                               class="font-fz-b text-white">{{ lang_form_input()->email }}</label>
                                        <input type="email" name="email" id="request-catalog-email" class="form-control"
                                               placeholder="{{ lang_form_input()->email }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="request-catalog-phone-number"
                                               class="font-fz-b text-white">{{ lang_form_input()->phone_number }}</label>
                                        <input type="text" name="phone_number" id="request-catalog-phone-number"
                                               class="form-control"
                                               placeholder="{{ lang_form_input()->phone_number }}" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-malibu"
                                                style="padding: 15px 50px 15px 50px">
                                            SUBMIT
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- CONTENT GENERAL -->

                        <div class="row" id="form-general" style="display: none;">
                            <div class="col-12 col-sm-10 ml-auto mr-auto">
                                <form action="{{ route('mail.send.contact-dealer.general') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="general-name"
                                               class="font-fz-b text-white">{{ lang_form_input()->name }}</label>
                                        <input type="text" name="name" id="general-name" class="form-control"
                                               placeholder="{{ lang_form_input()->name }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="general-email"
                                               class="font-fz-b text-white">{{ lang_form_input()->email }}</label>
                                        <input type="email" name="email" id="general-email" class="form-control"
                                               placeholder="{{ lang_form_input()->email }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="general-phone-number"
                                               class="font-fz-b text-white">{{ lang_form_input()->phone_number }}</label>
                                        <input type="text" name="phone_number" id="general-phone-number"
                                               class="form-control"
                                               placeholder="{{ lang_form_input()->phone_number }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="general-reason-for-contact"
                                               class="font-fz-b text-white">{{ lang_form_input()->reason_for_contact }}</label>
                                        <textarea name="reason_for_contact" id="general-reason-for-contact"
                                                  class="form-control"
                                                  placeholder="{{ lang_form_input()->reason_for_contact }}"
                                                  rows="5" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-malibu"
                                                style="padding: 15px 50px 15px 50px">
                                            {{ lang_button()->submit }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="section" id="section-about-dealer">

        <div>
            <div class="container">
                <div class="row">

                    <div class="col-12 col-md-10 ml-auto mr-auto">
                        <div class="form-group text-center">
                            <h2 class="title sub-header font-fz-b">
                                {{ lang_other()->about_dealer }}
                            </h2>
                        </div>
                    </div>

                    <div class="col-12 col-sm-5 ml-auto mr-auto">
                        <div class="form-group">
                            <img src="{{ imageFolder($dealer_profile->dealer_photo) }}" alt="" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-12 col-sm-5 ml-auto mr-auto">
                        <div class="form-group">
                            <p class="text-dark">
                                @if (session('flag_en_'.config('country')->short_name))
                                    {{ $dealer_profile->long_description_en }}
                                @else
                                    {{ $dealer_profile->long_description_local }}
                                @endif
                            </p>
                            <br>
                            <div class="text-center" style="padding-top: 30px;">
                                @if (strlen($dealer_profile->facebook_link) > 0)
                                    <a href="{{ $dealer_profile->facebook_link }}" target="_blank"
                                       style="margin-right: 20px;">
                                        <img src="{{ asset('images/icons/fb.png') }}" alt=""
                                             style="width: 32px; height: 32px;">
                                    </a>
                                @endif

                                @if (strlen($dealer_profile->instagram_link) > 0)
                                    <a href="{{ $dealer_profile->instagram_link }}" target="_blank"
                                       style="margin-right: 20px;">
                                        <img src="{{ asset('images/icons/ig.png') }}" alt=""
                                             style="width: 32px; height: 32px;">
                                    </a>
                                @endif

                                @if (strlen($dealer_profile->website_link) > 0)
                                    <a href="{{ $dealer_profile->website_link }}" target="_blank"
                                       style="margin-right: 20px;">
                                        <img src="{{ asset('images/icons/website.png') }}" alt=""
                                             style="width: 32px; height: 32px;">
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

@endsection

@push ('scripts')

    <script>

        function display_free_test_drive() {
            $('#form-free-test-drive').fadeIn();
            $('#form-request-a-catalog').hide();
            $('#form-general').hide();
            $('#contact-m-free-test-drive').removeClass().addClass('font-fz-b text-sky');
            $('#contact-m-request-a-catalog').removeClass().addClass('font-fz-b');
            $('#contact-m-general').removeClass().addClass('font-fz-b');
        }

        function display_request_catalog() {
            $('#form-free-test-drive').hide();
            $('#form-request-a-catalog').fadeIn();
            $('#form-general').hide();
            $('#contact-m-free-test-drive').removeClass().addClass('font-fz-b');
            $('#contact-m-request-a-catalog').removeClass().addClass('font-fz-b text-sky');
            $('#contact-m-general').removeClass().addClass('font-fz-b');
        }

        function display_general() {
            $('#form-free-test-drive').hide();
            $('#form-request-a-catalog').hide();
            $('#form-general').fadeIn();
            $('#contact-m-free-test-drive').removeClass().addClass('font-fz-b');
            $('#contact-m-request-a-catalog').removeClass().addClass('font-fz-b');
            $('#contact-m-general').removeClass().addClass('font-fz-b text-sky');
        }

        function scroll_to_form_section() {
            $('html,body').animate({
                scrollTop: $('#form-section').offset().top
            }, 'slow');
        }

        function scroll_to_section_about_dealer() {
            $('html,body').animate({
                scrollTop: $('#section-about-dealer').offset().top
            }, 'slow');
        }

        function scroll_to_section_map() {
            $('html,body').animate({
                scrollTop: $('#section-map').offset().top
            }, 'slow');
        }

        $('#btn-free-test-drive').on('click', function () {
            display_free_test_drive();
        });

        $('#btn-request-a-catalog').on('click', function () {
            display_request_catalog();
        });

        $('#btn-general').on('click', function () {
            display_general();
        });

        $('#btn-contact-us').on('click', function () {
            display_general();
            scroll_to_form_section();
        });

        $('.btn-contact-dealer-test-drive').on('click', function () {
            display_free_test_drive();
            scroll_to_form_section();
        });

        $('.btn-contact-dealer-request-catalog').on('click', function () {
            display_request_catalog();
            scroll_to_form_section();
        });

        $('#btn-section-about-dealer').on('click', function () {
            scroll_to_section_about_dealer();
        });

        $('#btn-section-map').on('click', function () {
            scroll_to_section_map();
        });

        @if (session('form-section') == 'form-request-a-catalog')
        display_request_catalog();
        @php
            session(['form-section' => ''])
        @endphp
        @endif

        @if (session('form-section') == 'form-general')
        display_general();
        @php
            session(['form-section' => ''])
        @endphp
        @endif
    </script>

@endpush
