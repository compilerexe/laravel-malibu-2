@extends ('layouts.app')
@push ('css')
    <style>
        header {
            position: relative;
            /*background-color: black;*/
            background: url('{{ $boat->background }}') center center no-repeat;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;
            -webkit-background-size: cover;
            background-size: cover;
        }

        header video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        header .container {
            position: relative;
            z-index: 2;
        }

        header .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0.3;
            z-index: 1;
        }

        .video-section {
            min-height: 100vh;
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                background: url('{{ $boat->background }}') center center no-repeat;
                -webkit-background-size: cover;
                background-size: cover;
            }

            header video {
                display: none;
            }

            .video-section {
                min-height: 10vh;
            }
        }
    </style>
@endpush
@push ('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
@endpush
@section ('content')

    <section class="page-header" style="border-top: 0 !important;">
        <header>
            <div class="overlay"></div>
            @if (strlen($explore_boat->background_video) > 0)
                <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                    <source src="{{ $explore_boat->background_video }}" type="video/mp4">
                </video>
            @endif
            <div class="container h-100">
                <div class="d-flex text-center h-100">
                    <div class="my-auto w-100 text-white">
                        <h1 class="display-3 title-header">
                            {{ $explore_boat->first_section_header }}
                        </h1>
                    </div>
                </div>
            </div>
        </header>
    </section>

    <section class="section secion-blog cd-section" id="blogs">

        <div class="blog-1" id="blog-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 ml-auto mr-auto">
                        <img src="{{ $explore_boat->second_section_boat_image }}" alt=""
                             class="img-fluid">
                        <div class="form-group text-center">
                            <h2 class="title title-boat-detail">
                                {{ $explore_boat->second_section_header }}
                            </h2>
                            <p>
                                {{ $explore_boat->second_section_description }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="section bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12">

                    @php
                        $slides = [];
                        foreach ($explore_boat->third_section_image as $image) {
                            array_push($slides, $image);
                        }
                    @endphp
                    <div class="page-carousel">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach ($slides as $item)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}"
                                        class="{{ ($loop->index == 0) ? 'active' : '' }}"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                @foreach ($slides as $url)
                                    <div class="carousel-item {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        <img class="d-block img-fluid"
                                             src="{{ $url }}">
                                    </div>
                                @endforeach
                            </div>
                            <a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators"
                               role="button" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators"
                               role="button" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <br>

                    <div class="text-center">
                        @php
                            $glance = [];
                            foreach ($explore_boat->third_section_glance as $key => $value) {
                                $glance[$key] = $value;
                            }
                        @endphp
                        <h3 class="text-white font-fz-b">
                            {{ lang_other()->hull_length }} : {{ $glance['hull_length'] }}
                        </h3>
                        <h3 class="text-white font-fz-b">
                            {{ lang_other()->beam }} : {{ $glance['beam'] }}
                        </h3>
                        <h3 class="text-white font-fz-b">
                            {{ lang_other()->draft }} : {{ $glance['draft'] }}
                        </h3>
                        <h3 class="text-white font-fz-b">
                            {{ lang_other()->max_capacity }} : {{ $glance['max_capacity'] }}
                        </h3>
                        <br>
                        <a href="https://www.malibuboats.com/boat-configurator" target="_blank">
                            <button type="button" class="btn btn-malibu">
                                {{ lang_button()->build_a_boat }}
                            </button>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section" style="padding: 0 !important; margin: 0 !important;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12" style="padding: 0;">
                    <div class="video-section">
                        <div style="padding:56.25% 0 0 0;position:relative;">
                            <iframe
                                src="https://player.vimeo.com/video/{{ $explore_boat->fourth_section_video }}?title=0&byline=0&portrait=0"
                                style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
                                webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
