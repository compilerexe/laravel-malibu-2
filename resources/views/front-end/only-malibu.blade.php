@extends ('layouts.app')

@push ('css')
    <style>
        header {
            position: relative;
            background-color: black;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;
        }

        header video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        header .container {
            position: relative;
            z-index: 2;
        }

        header .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0.2;
            z-index: 1;
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                background: url('https://cdn.malibuboats.com/2018/Only Malibu/19_Command_Center.jpg') black no-repeat center center scroll;
            }

            header video {
                display: none;
            }
        }
    </style>
@endpush

@push ('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
@endpush

@section ('content')

    <section class="page-header" style="border-top: 0 !important;">
        <header>
            <div class="overlay"></div>
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                <source src="https://cdn.malibuboats.com/videos/only-malibu.mp4" type="video/mp4">
            </video>
            <div class="container h-100">
                <div class="d-flex text-center h-100">
                    <div class="my-auto w-100 text-white">
                        <h1 class="display-3 title-header">
                            {{ $about_malibu->first_section_header }}
                        </h1>
                    </div>
                </div>
            </div>
        </header>
    </section>

    <section class="section" style="min-height: 50vh;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="from-group text-center">
                        <h2 class="title sub-header font-fz-b">
                            {{ $about_malibu->second_section_header }}
                        </h2>
                    </div>
                    <div>
                        <p>
                            {{ $about_malibu->second_section_description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @foreach (json_decode($about_malibu->third_section_description) as $key => $value)
        <section style="min-height: 100vh;" class="bg-dark-blue" id="{{ str_replace(' ', '-', strtolower($key)) }}">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="from-group text-center">
                            <h2 class="title sub-header font-fz-b text-white">{{ $key }}</h2>
                        </div>
                        <div class="form-group">
                            <div class="row" style="padding: 0 30px 30px 30px;">
                                <div class="col-12 col-sm-8 mr-auto ml-auto">
                                    <p class="text-white">
                                        {{ $value->description }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div style="padding:56.25% 0 0 0;position:relative;">
                            <iframe src="https://player.vimeo.com/video/{{ $value->vimeo }}?title=0&byline=0&portrait=0"
                                    style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
                                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach


@endsection
