@extends ('layouts.app')
@push ('css')
    <style>
        header {
            position: relative;
            background-color: black;
            {{--            background: url('{{ asset('images/cdn-malibuboats/News-Load-25-PR.jpg') }}') center center no-repeat;--}}
 -webkit-background-size: cover;
            background-size: cover;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;
        }

        header video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: 0;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        }

        header .container {
            position: relative;
            z-index: 2;
        }

        header .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0.2;
            z-index: 1;
        }

        @media (pointer: coarse) and (hover: none) {
            header {
                background: url('{{ asset('images/cdn-malibuboats/News-Load-25-PR.jpg') }}') center center no-repeat;
                -webkit-background-size: cover;
                background-size: cover;
            }

            header video {
                display: none;
            }
        }
    </style>
@endpush
@section ('content')

    <section class="page-header" style="border-top: 0 !important;">
        <header>
            <div class="overlay"></div>
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                <source src="{{ asset('video/2019_M235_WebBack.mp4') }}" type="video/mp4">
            </video>
            <div class="container h-100">
                <div class="d-flex text-center h-100">
                    <div class="my-auto w-100 text-white">
                        <h1 class="display-3 title-header">
                            {{--@if (session('flag_en_'.config('country')->short_name))--}}
                            {{--Malibu Boats {{ config('country')->display }}--}}
                            {{--@else--}}
                            {{--{{ lang_home()->malibu_boats_country }}--}}
                            {{--@endif--}}
                            Malibu Boats {{ ucfirst(config('country')->display) }}
                        </h1>

                        <div class="form-group">
                            @if (config('country')->country_name != 'asia')
                                <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer#form-section') }}">
                                    <button type="button" class="btn btn-malibu"
                                            style="margin-right: 30px; margin-top: 30px;">
                                        {{ lang_button()->test_drive }}
                                    </button>
                                </a>
                            @endif
                            <a href="https://www.malibuboats.com/boat-configurator" target="_blank">
                                <button type="button" class="btn btn-malibu"
                                        style="margin-right: 30px; margin-top: 30px;">
                                    {{ lang_button()->build_a_boat }}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </section>

    @if (config('country')->country_name != 'asia')

        <section class="section bg-dark-blue">

            <div class="container" style="//padding-left: 0; //padding-right: 0;">
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group text-center">
                            <h2 class="title sub-header font-fz-b text-white">{{ lang_home()->free_test_drive }}</h2>
                        </div>

                        <div class="form-group">
                            <br>
                            <div class="card card-plain card-blog">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-image">
                                            <img class="img"
                                                 src="https://cdn.malibuboats.com/2018/Models/Malibu/M235/19-M235-header.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-body" style="padding: 0 30px 0 30px;">
                                            {{--<h6 class="card-category small-title text-info font-fz-b">2019 22 LSV</h6>--}}
                                            {{--<h3 class="card-title">--}}
                                            {{--<a href="javascript:void(0)" class="font-fz-b">--}}
                                            {{--WATCH THE--}}
                                            {{--LAUNCH EVENT--}}
                                            {{--</a>--}}
                                            {{--</h3>--}}
                                            <p class="card-description" style="color: white !important;">
                                                {{ lang_contact_dealer()->free_test_drive_content }}
                                            </p>

                                            <div class="text-center">
                                                <a href="{{ clientNavbarURL(config('country')->country_name, '/contact-dealer#form-section') }}">
                                                    <button type="button"
                                                            class="btn btn-malibu btn-contact-dealer-test-drive"
                                                            style="margin-right: 30px; margin-top: 30px;">
                                                        {{ lang_button()->schedule_a_test_drive }}
                                                    </button>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>

    @endif

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5" style="//padding: 0 0 0 50px;">
                    <div style="padding: 15px 15px 15px 15px; color: black;">
                        <p class="font-explore-title">
                            {{ lang_home()->explore_the_right_boat_for_you }}
                        </p>
                        <br>

                        <div class="form-group row">
                            <label class="col-form-label col-6 font-explore-title">
                                {{ lang_home()->malibu_boat }}
                            </label>
                            <div class="col-6">
                                <select name="" id="boat-model" class="form-control">
                                    @foreach ($boats as $boat)
                                        <option value="{{ $loop->index }}">{{ $boat->model }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <br>
                        <h1 class="title-header model-title" id="boat-model-title">
                            25 LSV
                        </h1>
                        <br>
                        <p class="font-fz-b model-content" id="boat-model-tag"
                           style="text-transform: uppercase">
                            Response
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6">
                                <a href="{{ clientNavbarURL(config('country')->country_name, '/boat/25-lsv') }}"
                                   class="boat-model-explore">
                                    <button type="button" class="btn btn-malibu"
                                            style="padding-left: 0; padding-right: 0; margin-top: 30px; color: #189BC5 !important; width: 100%;">
                                        {{ lang_button()->explore_boat }}
                                    </button>
                                </a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6">
                                <a href="https://www.malibuboats.com/boat-configurator" target="_blank">
                                    <button type="button" class="btn btn-malibu"
                                            style="padding-left: 0; padding-right: 0; margin-top: 30px; color: #189BC5 !important; width: 100%;">
                                        {{ lang_button()->build_a_boat }}
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7" style="padding: 0;">
                    <div class="text-center" style="padding: 30px 30px 30px 30px;">
                        <img src="{{ asset('images/2018/25LSV/Studio/25LSV18_bro_vrscan_0001.png') }}" alt=""
                             id="boat-model-image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-dark-blue">

        <div class="container">
            <div class="row">
                <div class="col-12 ml-auto mr-auto">
                    <div class="text-center">
                        <h2 class="title sub-header font-fz-b text-white">{{ lang_home()->news_and_events }}</h2>
                    </div>
                    <br>

                    @foreach ($news as $data)

                        @php
                            $link = clientNavbarURL(config('country')->country_name, '/news/'.$data->id);
                        @endphp

                        <div class="card card-plain card-blog">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-lg-5">
                                    <a href="{{ $link }}"
                                       class="font-fz-b">
                                        <div class="card-image text-center" style="padding-top: 15px;">
                                            <img class="img"
                                                 src="{{ imageFolder($data->cover_image) }}"
                                                 style="width: 367px; height: 245px;">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-12 col-lg-7">
                                    <div class="card-body" style="padding-top: 0; margin-top: 0;">
                                        <h3 class="card-title">
                                            <a href="{{ $link }}"
                                               class="content-title font-fz-b text-white">
                                                {{ $data->news_header_en }}
                                            </a>
                                        </h3>
                                        <p class="card-description" style="color: white !important;">
                                            {{ strip_tags(str_limit($data->news_description_en, 300)) }}
                                        </p>
                                        <br>
                                        <a href="{{ $link }}"
                                           class="font-fz-b text-white">
                                            <button type="button" class="btn btn-malibu">
                                                {{ lang_button()->see_details }}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endforeach

                </div>
            </div>
        </div>

    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title sub-header font-fz-b">{{ lang_home()->about_malibu }}</h2>
                    <h5 class="description">{{ lang_home()->about_malibu_content }}</h5>
                </div>
            </div>
            <div class="space-top"></div>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/about-malibu/#command-center') }}">
                        <div class="card card-pricing" data-background="image"
                             style="background-image: url('https://cdn.malibuboats.com/images/cards-image.jpg')">
                            <div class="card-body">
                                <h6 class="card-category font-fz-b">{{ lang_home()->command_center }}</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/about-malibu') }}">
                        <div class="card card-pricing" data-background="image"
                             style="background-image: url('https://cdn.malibuboats.com/images/image-article-post.jpg')">
                            <div class="card-body">
                                <h6 class="card-category font-fz-b">{{ lang_home()->integrated_surf_platform }}</h6>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{ clientNavbarURL(config('country')->country_name, '/about-malibu/#surf-gate') }}">
                        <div class="card card-pricing" data-background="image"
                             style="background-image: url('https://cdn.malibuboats.com/images/homepage-card-surfgate.jpg')">
                            <div class="card-body">
                                <h6 class="card-category font-fz-b">{{ lang_home()->surf_gate }}</h6>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection

@push ('scripts')
    <script>
        function init_boat_model() {
            $.ajax({
                type: "post",
                url: "{{ url('/api/fetch-explore-boats') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    model: $('#boat-model').find(':selected').text()
                },
                success: function (response) {
                    console.log(response);
                    $('#boat-model-title').text(response.model);
                    $('#boat-model-tag').text(response.tag);
                    $('#boat-model-image').attr('src', response.image);
                    $('.boat-model-explore').attr('href', response.link.toLowerCase().split(' ').join('-'));
                }
            });
        }

        init_boat_model();

        $('#boat-model').on('change', function () {
            init_boat_model();
        });
    </script>
@endpush
