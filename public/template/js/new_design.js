var subMenuSliding = document.getElementById("submenusection");
var boatShowBtn = document.getElementById("boatMenu");
boatShowBtn.onmouseover = function () {
    subMenuSliding.style.display = 'block';
}
boatShowBtn.onmouseout = function () {
    subMenuSliding.style.display = 'none';
}
subMenuSliding.onmouseover = function () {
    subMenuSliding.style.display = 'block';
}
subMenuSliding.onmouseout = function () {
    subMenuSliding.style.display = 'none';
}

var slideCurrentPos = 6;
var small_ball = [document.getElementById("ball_1"),
    document.getElementById("ball_2"),
    document.getElementById("ball_3"),
    document.getElementById("ball_4"),
    document.getElementById("ball_5"),
    document.getElementById("ball_6"),
    document.getElementById("ball_7"),
    document.getElementById("ball_8"),
    document.getElementById("ball_9"),
    document.getElementById("ball_10"),
    document.getElementById("ball_11"),
];

var ballPosInit = new Array();
for (i = 0; i < small_ball.length; i++) {
    var cxVal = small_ball[i].getAttribute("cx");
    ballPosInit.push(cxVal);
}


var lineStart = document.getElementById("line_indicate").getAttribute("x1");
var lineEnd = document.getElementById("line_indicate").getAttribute("x2");
var bigCirclepos_1 = document.getElementById("BigCircleA").getAttribute("cx");
var bigCirclepos_2 = document.getElementById("BigCircleB").getAttribute("cx");
var bigCirclepos_3 = document.getElementById("BigCircleC").getAttribute("cx");

$(document).ready(function () {


    if ($("#datetimepicker").length != 0) {
        $('#datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    }
});

function changeSlidePost(pos) {
    var currentStartPoint = parseInt(small_ball[0].getAttribute("cx"));
    var diff = slideCurrentPos - parseInt(pos);
    var movePoint = parseInt(currentStartPoint + 60 * (diff));
    console.log(diff);
    setTransition("line_indicate", "all 1s");
    document.getElementById("line_indicate").style.transform = "translate(" + movePoint + "px,0px)";

    if (parseInt(pos) == 1) {
        setSVGAttribute("BigCircleA", "r", 0);
        setTransition("BigCircleA", "all 2s");
        setSVGAttribute("BigCircleB", "r", 15);
        setTransition("BigCircleB", "all 2s");
        setSVGAttribute("BigCircleC", "r", 15);
        setTransition("BigCircleC", "all 2s");
    }
    else if (pos == 11) {
        setSVGAttribute("BigCircleA", "r", 15);
        setTransition("BigCircleA", "all 2s");
        setSVGAttribute("BigCircleB", "r", 15);
        setTransition("BigCircleB", "all 2s");
        setSVGAttribute("BigCircleC", "r", 0);
        setTransition("BigCircleC", "all 2s");
    } else {
        setSVGAttribute("BigCircleA", "r", 15);
        setTransition("BigCircleA", "all 2s");
        setSVGAttribute("BigCircleB", "r", 15);
        setTransition("BigCircleB", "all 2s");
        setSVGAttribute("BigCircleC", "r", 15);
        setTransition("BigCircleC", "all 2s");
    }
    var cxBall_1 = 60 * (6 - pos);
    for (i = 0; i < small_ball.length; i++) {

        small_ball[i].style.transition = "all 1s";
        small_ball[i].setAttribute("cx", cxBall_1);

        cxBall_1 = cxBall_1 + 60;
        if ((pos - 1) == i) {
            small_ball[i].setAttribute("fill", "#009ac7");
        } else {
            small_ball[i].setAttribute("fill", "#3D4144");
        }

    }
    //small_ball[pos-1].setAttribute("fill","#009ac7");

    slideCurrentPos = pos;

}

function setEvtAttribute(evt, attr, value) {
    evt.target.setAttribute(attr, value);

}

function setTransition(elementId, value) {
    document.getElementById(elementId).style.transition = value;
}

function setSVGAttribute(elementId, attributeName, value) {
    document.getElementById(elementId).setAttribute(attributeName, value);
}

function textAppear(pos) {
    small_ball[pos - 1].setAttribute("fill", "#009ac7");
    small_ball[pos - 1].setAttribute("r", 4);

    var marginValue = "0%";
    switch (parseInt(pos)) {
        case 1:
            marginValue = "-90%";
            break;
        case 2:
            marginValue = "-70%";
            break;
        case 3:
            marginValue = "-53%";
            break;
        case 4:
            marginValue = "-37%";
            break;
        case 5:
            marginValue = "-20%";
            break;
        case 6:
            marginValue = "0%";
            break;
        case 7:
            marginValue = "16%";
            break;
        case 8:
            marginValue = "33%";
            break;
        case 9:
            marginValue = "50%";
            break;
        case 10:
            marginValue = "67%";
            break;
        case 11:
            marginValue = "85%";
            break;
    }
    var textEle = document.getElementById("tooltiptext1");
    textEle.style.visibility = "visible";
    textEle.style.top = small_ball[pos - 1].getAttribute("cx");
    textEle.style.left = small_ball[pos - 1].getAttribute("cy");

    textEle.style.marginLeft = marginValue;
}

function hideText(pos) {

    small_ball[pos - 1].setAttribute("fill", "#3D4144");
    small_ball[pos - 1].setAttribute("r", 2.5);
    var textEle = document.getElementById("tooltiptext1");
    textEle.style.visibility = "hidden";
}
