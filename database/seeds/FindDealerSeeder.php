<?php

use Illuminate\Database\Seeder;
use App\Models\FindDealers;

class FindDealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country = 1; $country <= 9; $country++) {
            FindDealers::create([
                'country_id' => $country,
                'address' => 'Guangzhou Garhorn Marine
                <br>1ST FLOOR LION BUILDING GUANGZHOU GUANGDONG 511431 CHINA
                <br>jason@garhorn.com'
            ]);
        }
    }
}
