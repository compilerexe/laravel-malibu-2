<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Administrators::create([
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'email' => 'malibuboatsasia@malibuboatsasia.com'
        ]);
    }
}
