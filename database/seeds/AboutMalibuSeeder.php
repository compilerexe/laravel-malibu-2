<?php

use Illuminate\Database\Seeder;
use App\Models\AboutMalibu;

class AboutMalibuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country = 1; $country <= 9; $country++) {
            AboutMalibu::create([
                'country_id' => $country,
                'first_section_header' => 'ABOUT MALIBU',
                'second_section_header' => 'Malibu Innovations',
                'second_section_description' => 'In 2012 when Malibu introduced the revolutionary and industry changing Surf GateTM it was just the beginning of an evolution in design, engineering and innovation. Malibu’s engineering team continued to integrate our surf system into multiple facets of boat design over years to come to create a surf experience like no other.',
                'third_section_description' => json_encode([
                    'Surf Gate' => [
                        'description' => 'With the other components of Malibu’s Integrated Surf Platform, Surf Gate creates the most dynamic playground for wake surfing possible. Period. Malibu’s ISP allows hundreds of customization options. No competitor comes close to offering that adjustability range.',
                        'vimeo' => '285338431'
                    ],
                    'Power Wedge 3' => [
                        'description' => 'The Power Wedge III makes your wakes and waves bigger by pulling down the back of the boat and saves gas by automatically getting you on plane faster. For 2019 it has five more degrees of lift for more usable
 positions and an improved exhaust pipe so it’s more effective and it looks better than ever. Our innovations are always evolving.',
                        'vimeo' => '285338285'
                    ],
                    'Hard Tank Ballast' => [
                        'description' => 'Up to 5,000 lbs. across the wake setter line, our model specific quad hard tank ballast offers you the ability to set the size and shape of your ideal wake with incredible ease.',
                        'vimeo' => '285338144'
                    ],
                    'Command Center' => [
                        'description' => 'Directly ahead of the wheel, the bright, high-resolution 12-inch MaliView touchscreen gives you finger-tap control of the ballast, Power Wedge II, Surf Gate, and navigation, with fast, easy access to customizable rider presets, media controls and a variety of gauge displays.',
                        'vimeo' => '226949216'
                    ],
                    'Malibu Gx Tower' => [
                        'description' => 'Built by Malibu, the all-new Malibu Gx tower introduces an industry standard for innovation and quality. Turn the dial to raise or lower the tower in 9 seconds, half the time it takes for the competition.',
                        'vimeo' => '285547396'
                    ]
                ])
            ]);
        }
    }
}
