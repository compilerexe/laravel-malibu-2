<?php

use Illuminate\Database\Seeder;
use App\Models\FooterContactDealers;

class FooterContactDealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country = 2; $country <= 9; $country++) {
            FooterContactDealers::create([
                'country_id' => $country,
                'description' => json_encode([
                    'address_country' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002 <a href="https://www.malibuboatsasia.com/japan/contact-dealer#section-about-dealer">...Read more</a> 
                Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361 <a href="https://www.malibuboatsasia.com/japan/contact-dealer#section-map">Get direction</a> 
                <br>Call: 042-444-1125<br>Email: tada@joy.hi-ho.ne.jp',
                    'address_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002 <a href="https://www.malibuboatsasia.com/japan/contact-dealer#section-about-dealer">...Read more</a> 
                Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361 <a href="https://www.malibuboatsasia.com/japan/contact-dealer#section-map">Get direction</a> 
                <br>Call: 042-444-1125<br>Email: tada@joy.hi-ho.ne.jp'
                ])
            ]);
        }
    }
}
