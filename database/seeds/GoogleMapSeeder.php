<?php

use Illuminate\Database\Seeder;

class GoogleMapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\GoogleMaps::create([
            'country_id' => 2,
            'source_code' => '!1m18!1m12!1m3!1d3241.956016268786!2d139.55558881525806!3d35.6534552802008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f04610dc0ba7%3A0xeed16100443de292!2z4LiN4Li14LmI4Lib4Li44LmI4LiZIOOAkjE4Mi0wMDE1IFTFjWt5xY0tdG8sIENoxY1mdS1zaGksIFlhZ3Vtb2RhaSwgMiBDaG9tZeKIkjQsIOOCv-OCueOCq-ODvOODnuODquODs-iyqeWjsu-8iOagqu-8iQ!5e0!3m2!1sth!2sth!4v1540181273928'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 3,
            'source_code' => '!1m14!1m8!1m3!1d2310.121513034034!2d127.40522477441282!3d37.72627412853198!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x356331dc4133df55%3A0x278175af0c3362c2!2zNCBTb2RvbG1hbC1naWwsIE9lc2VvLW15ZW9uLCBHYXB5ZW9uZy1ndW4sIEd5ZW9uZ2dpLWRvLCDguYDguIHguLLguKvguKXguLXguYPguJXguYk!5e0!3m2!1sth!2sth!4v1540181468915'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 4,
            'source_code' => '!1m14!1m8!1m3!1d14771.751415852836!2d114.1651883!3d22.2424366!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7725d6493d06de7e!2zU3Vuc2hpbmUgWWFjaHRpbmcgTGltaXRlZCDmsLjkuK3pgYroiYfmnInpmZDlhazlj7g!5e0!3m2!1sth!2sth!4v1540181569120'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 5,
            'source_code' => '!1m18!1m12!1m3!1d3671.5302756369747!2d113.29525231496812!3d23.04101298494363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3402542fb544392b%3A0xfa1204736bf4d4f!2sJiahang+Yacht!5e0!3m2!1sth!2sth!4v1540181600104'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 6,
            'source_code' => '!1m14!1m8!1m3!1d7225.595269354833!2d121.4959694!3d25.1087107!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aee0d8b48aed%3A0xe5a93eccb5397978!2sYingxing+Pier!5e0!3m2!1sth!2sth!4v1540181631531'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 7,
            'source_code' => ''
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 8,
            'source_code' => '!1m18!1m12!1m3!1d3377.476312621735!2d34.793175615166476!3d32.164428181161306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151d48ea93ea5fb7%3A0x3d1f5cac09aae7e8!2zSGFUc2VkZWYgMSwgSGVydHNsaXlhLCDguK3guLTguKrguKPguLLguYDguK3guKU!5e0!3m2!1sth!2sth!4v1540181673155'
        ]);

        \App\Models\GoogleMaps::create([
            'country_id' => 9,
            'source_code' => '!1m18!1m12!1m3!1d3615.6689053414216!2d55.1569018150059!3d25.01136408398289!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e4f6200f22047%3A0xd3f3dd50cca478a1!2sBrodie+Boats!5e0!3m2!1sth!2sth!4v1540181703350'
        ]);

//        \App\Models\GoogleMaps::create([
//            'country_id' => 10,
//            'source_code' => '!1m18!1m12!1m3!1d3475.648629371669!2d34.97783911510034!3d29.40983328211761!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjnCsDI0JzM1LjQiTiAzNMKwNTgnNDguMSJF!5e0!3m2!1sth!2sth!4v1540181725213'
//        ]);
    }
}
