<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(BoatSeeder::class);
        $this->call(ExploreBoatSeeder::class);
//        $this->call(SocialLinkSeeder::class);
        $this->call(NewSeeder::class);
        $this->call(InventorySeeder::class);
        $this->call(AboutMalibuSeeder::class);
//        $this->call(ContactDealerSeeder::class);
//        $this->call(FooterContactDealerSeeder::class);
        $this->call(LangSeeder::class);
//        $this->call(FindDealerSeeder::class);
        $this->call(GoogleMapSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(DealerSeeder::class);
        $this->call(DealerProfileSeeder::class);
    }
}
