<?php

use Illuminate\Database\Seeder;

class DealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country = 2; $country <= 9; $country++) {
            \App\Models\Dealers::create([
                'country_id' => $country,
                'username' => 'demo' . $country,
                'password' => bcrypt('123456'),
                'email' => ''
            ]);
        }
    }
}
