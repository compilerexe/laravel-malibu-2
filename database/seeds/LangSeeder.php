<?php

use Illuminate\Database\Seeder;
use App\Models\Langs;

class LangSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country_id = 1; $country_id <= 9; $country_id++) {
            CreateLangs($country_id);
        }
    }
}
