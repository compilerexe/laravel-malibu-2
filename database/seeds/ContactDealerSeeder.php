<?php

use Illuminate\Database\Seeder;
use App\Models\ContactDealers;

class ContactDealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($country = 2; $country <= 9; $country++) {
            ContactDealers::create([
                'country_id' => $country,
//                'first_section_header' => 'CONTACT DEALER',
                'first_section_address' => json_encode([
                    'address_country' => 'Name: Tasker Marine Group
<br>
Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361
<br>
Tel: 042-444-1125
<br>
Email: tada@joy.hi-ho.ne.jp',
                    'address_en' => 'Name: Tasker Marine Group
<br>
Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361
<br>
Tel: 042-444-1125
<br>
Email: tada@joy.hi-ho.ne.jp'
                ]),
                'third_section_description' => json_encode([
                    'cover' => 'https://www.malibuboatsasia.com/images/japan-malibu.jpg',
                    'description_country' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
                    'description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.'
                ])
            ]);
        }
    }
}
