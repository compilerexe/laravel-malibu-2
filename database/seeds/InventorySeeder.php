<?php

use Illuminate\Database\Seeder;
use App\Models\Inventories;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($dealer = 1; $dealer <= 8; $dealer++) {
            for ($boat = 1; $boat <= 11; $boat++) {
                Inventories::create([
                    'dealer_id' => $dealer,
                    'boat_id' => $boat,
                    'year' => '2018',
                    'price' => '$10,000',
                    'boat_photos' => json_encode([
                        ['sort' => 1, 'image' => 'dealer/inventory/23LSV Bow.jpg'],
                        ['sort' => 2, 'image' => 'dealer/inventory/23LSV Branding.jpg'],
                        ['sort' => 3, 'image' => 'dealer/inventory/23LSV Hero.jpg'],
                        ['sort' => 4, 'image' => 'dealer/inventory/23LSV Rear Interior.jpg'],
                        ['sort' => 5, 'image' => 'dealer/inventory/23LSV Surf View Seating.jpg']
                    ]),
                    'boat_detail_local' => 'The best-selling towboat of all time, and for good reason. At 23 feet, it’s the perfect size to make memories on the water with up to 15 people. The 23 LSV will generate an absolute wall of water when you want it, but with the Diamond Multisport Hull it can create approachable wakes for beginners and even mellow slalom wakes. With a classic traditional bow look, the 23 LSV is the essence of the original Malibu brand and still a legend in the industry.',
                    'boat_detail_en' => 'The best-selling towboat of all time, and for good reason. At 23 feet, it’s the perfect size to make memories on the water with up to 15 people. The 23 LSV will generate an absolute wall of water when you want it, but with the Diamond Multisport Hull it can create approachable wakes for beginners and even mellow slalom wakes. With a classic traditional bow look, the 23 LSV is the essence of the original Malibu brand and still a legend in the industry.',
                    'used' => 0
                ]);
            }
        }

    }
}
