<?php

use Illuminate\Database\Seeder;

class DealerProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DealerProfile::create([
            'dealer_id' => 1,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/malibuboatsjapan/',
            'instagram_link' => 'https://www.instagram.com/malibuboatsjapan/',
            'website_link' => 'https://www.malibuboat.jp'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 2,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/malibuboatskorea/',
            'instagram_link' => 'https://www.instagram.com/malibuboatskorea/',
            'website_link' => 'https://www.malibuboatskorea.kr/'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 3,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/axiswakehk/',
            'instagram_link' => 'https://www.instagram.com/wakesurf_hk/',
            'website_link' => 'https://www.wakeboarding.com.hk'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 4,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => '',
            'instagram_link' => '',
            'website_link' => ''
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 5,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/sisihaha.tw',
            'instagram_link' => 'https://www.instagram.com/superhighwakeschool/',
            'website_link' => 'http://www.superhigh.com.tw'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 6,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/AquaMarinaIsrael/',
            'instagram_link' => 'https://www.instagram.com/aquamarinayachts/',
            'website_link' => 'http://www.aquamarina.co.il'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 7,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/brodieboats/',
            'instagram_link' => 'https://www.instagram.com/brodieboats/',
            'website_link' => 'http://www.brodieboats.com/'
        ]);

        \App\Models\DealerProfile::create([
            'dealer_id' => 8,
            'name_en' => 'Tasker Marine Group',
            'name_local' => 'Tasker Marine Group',
            'address_en' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local' => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone' => 'Telephone: 81-042-444-1125',
            'email' => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local' => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo' => 'dealer/profile/dealer_photo.jpg',
            'facebook_link' => 'https://www.facebook.com/royalmarineint',
            'instagram_link' => 'https://www.instagram.com/royal_marine_international/',
            'website_link' => 'http://www.royalmarineint.com'
        ]);
    }
}
