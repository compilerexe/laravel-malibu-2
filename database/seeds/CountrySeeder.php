<?php

use Illuminate\Database\Seeder;
use App\Models\Countries;
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Countries::create(['country_name' => 'asia', 'display' => 'Asia', 'icon' => 'https://www.malibuboatsasia.com/images/icons/globe-asia.png', 'lang_switch' => 0]);
        Countries::create(['country_name' => 'japan', 'display' => '日本', 'short_name' => 'JP', 'icon' => 'https://image.flaticon.com/icons/svg/206/206789.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'korea', 'display' => '남한', 'short_name' => 'KR', 'icon' => 'https://image.flaticon.com/icons/svg/206/206758.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'hong kong', 'display' => 'Hong Kong', 'short_name' => 'HK', 'icon' => 'https://image.flaticon.com/icons/svg/206/206669.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'china', 'display' => '中国', 'short_name' => 'CN', 'icon' => 'https://image.flaticon.com/icons/svg/206/206818.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'taiwan', 'display' => '台灣', 'short_name' => 'TW', 'icon' => 'https://image.flaticon.com/icons/svg/206/206650.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'israel', 'display' => 'Israel', 'short_name' => 'IL', 'icon' => 'https://image.flaticon.com/icons/svg/206/206697.svg', 'lang_switch' => 1]);
        Countries::create(['country_name' => 'uae', 'display' => 'UAE', 'short_name' => 'UAE', 'icon' => 'https://image.flaticon.com/icons/svg/206/206701.svg', 'lang_switch' => 0]);
        Countries::create(['country_name' => 'jordan', 'display' => 'Jordan', 'short_name' => 'JORDAN', 'icon' => 'https://image.flaticon.com/icons/svg/206/206775.svg', 'lang_switch' => 0]);
    }
}
