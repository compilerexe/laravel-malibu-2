<?php

use Illuminate\Database\Seeder;
use App\Models\News;

class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 9; $i++) {
            News::create([
                'country_id' => $i,
                'cover_image' => 'dealer/news/News-Load-25-PR.jpg',
                'news_header_local' => '2019 MALIBU WAKESETTER 25 LSV',
                'news_description_local' => 'Loudon, TN | September 20, 2018 –  Malibu Boats, Inc. (MBUU), the global leader in watersports towboat sales, has introduced the newest model in the world’s best-selling towboat family, the Wakesetter™ 25 LSV, for 2019. The 25 LSV is the 25-foot flagship of the popular LSV line. Being the biggest, it offers the most interior space, storage capacity, ballast, as well as the biggest wake and wave. The LSV line is the best-selling series of inboard towboats of all time because of its classic traditional-bow shape, user-friendly design and reliable wake—but it’s become so much more than that. With technology and comfort innovations like wireless phone charging, a redesigned helm seat, a starboard locker for cooler and general storage, the new Malibu Multi View Bench Seat™, and an available integrated swim step that flips down from the swimboard, the 25 LSV makes a day on the water more convenient than ever.',
                'news_header_en' => '2019 MALIBU WAKESETTER 25 LSV',
                'news_description_en' => 'Loudon, TN | September 20, 2018 –  Malibu Boats, Inc. (MBUU), the global leader in watersports towboat sales, has introduced the newest model in the world’s best-selling towboat family, the Wakesetter™ 25 LSV, for 2019. The 25 LSV is the 25-foot flagship of the popular LSV line. Being the biggest, it offers the most interior space, storage capacity, ballast, as well as the biggest wake and wave. The LSV line is the best-selling series of inboard towboats of all time because of its classic traditional-bow shape, user-friendly design and reliable wake—but it’s become so much more than that. With technology and comfort innovations like wireless phone charging, a redesigned helm seat, a starboard locker for cooler and general storage, the new Malibu Multi View Bench Seat™, and an available integrated swim step that flips down from the swimboard, the 25 LSV makes a day on the water more convenient than ever.',
                'home_active' => 0
            ]);

            News::create([
                'country_id' => $i,
                'cover_image' => 'dealer/news/News-Load-Content-win.jpg',
                'news_header_local' => 'MALIBU\'S 23 LSV IS THE WAKESURFING BOAT OF THE YEAR',
                'news_description_local' => 'Malibu Boat’s #1 best-selling towboat of all time is now officially the Wakesurfing Boat of the Year after taking home the 2018 WakeWorld Riders Choice Award. Malibu Boats would like to personally thank every person that voted for us. It is a true honor to be recognized by the community, and in return we will continue to bring you a dedication to innovation, quality and performance. Malibu Boats is also proud to have our athletes bring home WakeWorld Riders Choice awards. Dallas Friday, Tarah Mikacich, and Bec Gange were all Top Five Female Riders. Jeff Langley, Raph Derome, and Tony Carroll all landed in Top Ten Male Riders. Brian Grubb took home #1 Wakeskater, and Tom Fooshee was in the 2nd Runner Up for Cable Rider of the Year. Congratulations to our athletes and thank you for all the hard work and dedication you have given.',
                'news_header_en' => 'MALIBU\'S 23 LSV IS THE WAKESURFING BOAT OF THE YEAR',
                'news_description_en' => 'Malibu Boat’s #1 best-selling towboat of all time is now officially the Wakesurfing Boat of the Year after taking home the 2018 WakeWorld Riders Choice Award. Malibu Boats would like to personally thank every person that voted for us. It is a true honor to be recognized by the community, and in return we will continue to bring you a dedication to innovation, quality and performance. Malibu Boats is also proud to have our athletes bring home WakeWorld Riders Choice awards. Dallas Friday, Tarah Mikacich, and Bec Gange were all Top Five Female Riders. Jeff Langley, Raph Derome, and Tony Carroll all landed in Top Ten Male Riders. Brian Grubb took home #1 Wakeskater, and Tom Fooshee was in the 2nd Runner Up for Cable Rider of the Year. Congratulations to our athletes and thank you for all the hard work and dedication you have given.',
                'home_active' => 0
            ]);

            News::create([
                'country_id' => $i,
                'cover_image' => 'dealer/news/news-load-content-22lsv.jpg',
                'news_header_local' => 'THE 2019 MALIBU 22 LSV',
                'news_description_local' => 'Loudon, TN | July 25, 2018 –  Malibu Boats, Inc. (MBUU), the global leader in watersports towboat sales, has introduced the newest model in the world’s best-selling towboat family, the Wakesetter™ 22 LSV, for 2019. The 22 LSV™ offers the most space in a perfectly sized package. The 22-foot length allows for easy towing and garage storage with all the accolades the LSV family offers. It has luxurious seating for 14 in a beautiful traditional-bow design with easy access to the spacious front lounge area. New advances in the 2019 22 LSV include a fresh interior featuring a redesigned helm seat and an additional Malibu Wake View Bench Seat™ to provide plenty of seating options for a full day on the water.',
                'news_header_en' => 'THE 2019 MALIBU 22 LSV',
                'news_description_en' => 'Loudon, TN | July 25, 2018 –  Malibu Boats, Inc. (MBUU), the global leader in watersports towboat sales, has introduced the newest model in the world’s best-selling towboat family, the Wakesetter™ 22 LSV, for 2019. The 22 LSV™ offers the most space in a perfectly sized package. The 22-foot length allows for easy towing and garage storage with all the accolades the LSV family offers. It has luxurious seating for 14 in a beautiful traditional-bow design with easy access to the spacious front lounge area. New advances in the 2019 22 LSV include a fresh interior featuring a redesigned helm seat and an additional Malibu Wake View Bench Seat™ to provide plenty of seating options for a full day on the water.',
                'home_active' => 0
            ]);
        }
    }
}
