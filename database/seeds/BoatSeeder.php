<?php

use Illuminate\Database\Seeder;
use App\Models\Boats;

class BoatSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Boats::create(['model' => 'TXi MO', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png']);
        // Boats::create(['model' => 'TXi MO CB', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Front.png']);
        // Boats::create(['model' => '20 VTX', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/20VTX/Studio/20VTX18_bro_vrscan_0001.png']);
        // Boats::create(['model' => '21 VLX', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/21VLX/Studio/21VLX18_bro_vrscan_0001.png']);
        // Boats::create(['model' => '21 MLX', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/21MLX/Studio/21MLX_Front.png']);
        // Boats::create(['model' => '22 LSV', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/22VLX/Studio/22VLX18_bro_vrscan_0001.png']);
        // Boats::create(['model' => '23 LSV', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/23LSV/Studio/23LSV_Front.png']);
        // Boats::create(['model' => '25 LSV', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/25LSV/Studio/25LSV18_bro_vrscan_0001.png']);
        // Boats::create(['model' => '22 MXZ', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0001.png']);
        // Boats::create(['model' => '24 MXZ', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0001.png']);
        // Boats::create(['model' => 'M235', 'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/M235/Studio/M23518_bro_vrscan_0001.png']);
        
        Boats::create(
            [
                'model'            => '25 LSV',
                'tag'              => 'Response',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-25lsv-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/25LSV/Studio/25LSV18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => '23 LSV',
                'tag'              => 'Response',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-23lsv-header-small.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/23LSV/Studio/23LSV_Front.png',
            ]
        );
        Boats::create(
            [
                'model'            => '22 LSV',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-22lsv-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/22VLX/Studio/22VLX18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => '22 MXZ',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-22MXZ-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => '24 MXZ',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-24MXZ-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => 'M235',
                'tag'              => 'M235',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-M235-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/M235/Studio/M23518_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => '21 MLX',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-21MLX-header-small.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/21MLX/Studio/21MLX_Front.png',
            ]
        );
        Boats::create(
            [
                'model'            => '21 VLX',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-21VLX-header-small.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/21VLX/Studio/21VLX18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => '20 VTX',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-20VTX-header-new.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/20VTX/Studio/20VTX18_bro_vrscan_0001.png',
            ]
        );
        Boats::create(
            [
                'model'            => 'TXi MO',
                'tag'              => 'Wakesetter',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-TXiOB-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png',
            ]
        );
        Boats::create(
            [
                'model'            => 'TXi MO CB',
                'tag'              => 'Response',
                'background'       => 'https://www.malibuboatsasia.com/images/boats/background/19-TXiCB-header.jpg',
                'boat_front_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Front.png',
            ]
        );
    }
}
