<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->string('cover_image');
            $table->string('news_header_local')->nullable();
            $table->text('news_description_local')->nullable();
            $table->string('news_header_en');
            $table->text('news_description_en');

            $table->string('copy_cover_image')->nullable();
            $table->string('copy_news_header_local')->nullable();
            $table->text('copy_news_description_local')->nullable();
            $table->string('copy_news_header_en')->nullable();
            $table->text('copy_news_description_en')->nullable();

            $table->unsignedTinyInteger('home_active');
            $table->unsignedTinyInteger('create_approve')->nullable()->default(0);
            $table->unsignedInteger('update_count')->nullable()->default(0);
            $table->unsignedInteger('update_approve')->nullable()->default(0);
            $table->unsignedTinyInteger('delete_approve')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
