<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dealer_id');
            $table->string('name_en');
            $table->string('name_local')->nullable();
            $table->string('address_en');
            $table->string('address_local')->nullable();
            $table->string('telephone');
            $table->string('email');
            $table->text('short_description_en');
            $table->text('short_description_local')->nullable();
            $table->text('long_description_en');
            $table->text('long_description_local')->nullable();
            $table->string('dealer_photo');
            $table->string('facebook_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('website_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_profiles');
    }
}
