<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExploreBoats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('explore_boats', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('boat_id');
            $table->unsignedInteger('country_id');
            $table->text('lang_country');
            $table->text('lang_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('explore_boats');
    }
}
