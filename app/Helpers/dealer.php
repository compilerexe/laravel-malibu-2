<?php

/* ===== Explain this file =====
This file use for short hand when call auth function for dealer.
===== End Explain ===== */

use Illuminate\Support\Facades\Auth;

function dealer()
{
    return Auth::guard('dealer')->user();
}

function dealerCountry()
{
    if (Auth::guard('dealer')->check()) {
        $country = \App\Models\Countries::find(dealer()->country_id);
    } else {
        $country = \App\Models\Countries::find(session('accessCountryId'));
    }

    return $country;
}

function checkLangSwitch()
{
    return dealerCountry()->lang_switch;
}
