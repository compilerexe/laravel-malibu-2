<?php

/* ===== Explain this file =====
This file use for create explore boat with dummy data.
===== End Explain ===== */

function explore_boats_create($country_id)
{
    // TXi MO
    \App\Models\ExploreBoats::create([
        'boat_id' => 10,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'TXi OPEN BOW',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Top.png',
            'second_section_header' => '11-Time World Record Holder',
            'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Side.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Rear.png'
            ],
            'third_section_glance' => [
                'hull_length' => '20\'6\"/6.25 M',
                'beam' => '95\"/2.41 M',
                'draft' => '22\"/0.56 M',
                'max_capacity' => '7'
            ],
            'fourth_section_video' => '233206915'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'TXi OPEN BOW',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Top.png',
            'second_section_header' => '11-Time World Record Holder',
            'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Side.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Rear.png'
            ],
            'third_section_glance' => [
                'hull_length' => '20\'6\"/6.25 M',
                'beam' => '95\"/2.41 M',
                'draft' => '22\"/0.56 M',
                'max_capacity' => '7'
            ],
            'fourth_section_video' => '233206915'
        ])
    ]);

    // TXi MO CB
    \App\Models\ExploreBoats::create([
        'boat_id' => 11,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'TXi CLOSED BOW',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Top.png',
            'second_section_header' => '11-Time World Record Holder',
            'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Side.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Front.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Rear.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '20\'6\"/6.25 M',
                'beam' => '95\"/2.41 M',
                'draft' => '22\"/0.56 M',
                'max_capacity' => '5'
            ],
            'fourth_section_video' => '233206915'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'TXi CLOSED BOW',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Top.png',
            'second_section_header' => '11-Time World Record Holder',
            'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
            'third_section_image' => json_encode([
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Side.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Front.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Rear.png',
                'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Closedbow_Top.png'
            ]),
            'third_section_glance' => json_encode([
                'hull_length' => '20\'6"/6.25 M',
                'beam' => '95"/2.41 M',
                'draft' => '22"/0.56 M',
                'max_capacity' => '5'
            ]),
            'fourth_section_video' => '233206915'
        ])
    ]);

    // 20 VTX
    \App\Models\ExploreBoats::create([
        'boat_id' => 9,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '20 VTX: UNMATCHED VERSATILITY',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/20VTX/Studio/20VTX18_bro_vrscan_0003.png',
            'second_section_header' => 'Triple Threat',
            'second_section_description' => 'The 20 VTX is the perfect compromise because it excels at everything. Its unique Malibu Cut Diamond Hull™ is tournament approved—so you get pro-level slalom wakes—but fill the ballast, drop the Power Wedge III and deploy Surf Gate for a wave you’re not going to believe came off a 20-foot boat. Besides that, the 20 VTX trailers like a dream and fits in a standard garage. For families who want to do everything, this is the ultimate compromise.',
            'third_section_image' => json_encode([
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Top.png'
            ]),
            'third_section_glance' => [
                'hull_length' => '20\'6\"/6.10 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '11'
            ],
            'fourth_section_video' => '287154783'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '20 VTX: UNMATCHED VERSATILITY',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/20VTX/Studio/20VTX18_bro_vrscan_0003.png',
            'second_section_header' => 'Triple Threat',
            'second_section_description' => 'The 20 VTX is the perfect compromise because it excels at everything. Its unique Malibu Cut Diamond Hull™ is tournament approved—so you get pro-level slalom wakes—but fill the ballast, drop the Power Wedge III and deploy Surf Gate for a wave you’re not going to believe came off a 20-foot boat. Besides that, the 20 VTX trailers like a dream and fits in a standard garage. For families who want to do everything, this is the ultimate compromise.',
            'third_section_image' => json_encode([
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/20VTX/20VTX_Top.png'
            ]),
            'third_section_glance' => [
                'hull_length' => '20\'6\"/6.10 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '11'
            ],
            'fourth_section_video' => '287154783'
        ])
    ]);

    // 21 VLX
    \App\Models\ExploreBoats::create([
        'boat_id' => 8,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '21 VLX',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/21VLX/Studio/21VLX18_bro_vrscan_0003.png',
            'second_section_header' => 'Seats Up To 13',
            'second_section_description' => 'The traditional bow 21 VLX crossover seats up to 13 and comes equipped with Integrated Surf Platform™, G3.5 tower with speakers and board racks, Viper II Command Center, matching custom trailer, bimini top and canvas cover. Choose between the Diamond Multisport Hull or the Wake Plus Hull for wake and surf specialization.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'\"/6.40 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '13'
            ],
            'fourth_section_video' => '292162308'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '21 VLX',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/21VLX/Studio/21VLX18_bro_vrscan_0003.png',
            'second_section_header' => 'Seats Up To 13',
            'second_section_description' => 'The traditional bow 21 VLX crossover seats up to 13 and comes equipped with Integrated Surf Platform™, G3.5 tower with speakers and board racks, Viper II Command Center, matching custom trailer, bimini top and canvas cover. Choose between the Diamond Multisport Hull or the Wake Plus Hull for wake and surf specialization.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/21VLX/21VLX_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'\"/6.40 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '13'
            ],
            'fourth_section_video' => '292162308'
        ])
    ]);

    // 21 MLX
    \App\Models\ExploreBoats::create([
        'boat_id' => 7,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '21 MLX: IT’S TIME FOR MALIBU',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/21MLX/Studio/21MLX_Top.png',
            'second_section_header' => 'An Incredible Malibu Value',
            'second_section_description' => 'The 2018 21 MLX is a well-equipped pickle-fork powerhouse that seats up to 14, making it an ideal towboat for families with surfers and boarders. With standard features such as Power Wedge II, Surf Gate, Surf Band, Wake Plus Hull and the Hard Tank Ballast System, it delivers customized wakes and waves for every rider. The 21 MLX also comes standard with the Viper II Command Center, G3.5 Tower, bimini top, canvas cover and matching Malibu Trailer.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'\"/6.40 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '14'
            ],
            'fourth_section_video' => '292162308'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '21 MLX: IT’S TIME FOR MALIBU',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/21MLX/Studio/21MLX_Top.png',
            'second_section_header' => 'An Incredible Malibu Value',
            'second_section_description' => 'The 2018 21 MLX is a well-equipped pickle-fork powerhouse that seats up to 14, making it an ideal towboat for families with surfers and boarders. With standard features such as Power Wedge II, Surf Gate, Surf Band, Wake Plus Hull and the Hard Tank Ballast System, it delivers customized wakes and waves for every rider. The 21 MLX also comes standard with the Viper II Command Center, G3.5 Tower, bimini top, canvas cover and matching Malibu Trailer.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Side.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/21MLX/21MLX_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'\"/6.40 M',
                'beam' => '98\"/2.49 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '14'
            ],
            'fourth_section_video' => '292162308'
        ])
    ]);

    // 22 LSV
    \App\Models\ExploreBoats::create([
        'boat_id' => 3,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '22 LSV: UNPARALLELED PASSION',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/22VLX/Studio/22VLX18_bro_vrscan_0003.png',
            'second_section_header' => 'All-New 2019 LSV',
            'second_section_description' => 'With seating for up to 14 and two different hull choices, this all-new traditional bow can be set up for how your family plays. Choose the Diamond Multisport hull to get the most versatile wakeboarding/surfing/waterski surface on the planet, or opt for the Wake Plus Hull to go hardcore surf and wake.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Side.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Front.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'11\"/6.40 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '14'
            ],
            'fourth_section_video' => '281836525'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '22 LSV: UNPARALLELED PASSION',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/22VLX/Studio/22VLX18_bro_vrscan_0003.png',
            'second_section_header' => 'All-New 2019 LSV',
            'second_section_description' => 'With seating for up to 14 and two different hull choices, this all-new traditional bow can be set up for how your family plays. Choose the Diamond Multisport hull to get the most versatile wakeboarding/surfing/waterski surface on the planet, or opt for the Wake Plus Hull to go hardcore surf and wake.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Side.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Front.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/22LSV/22LSV_top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '21\'11\"/6.40 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '14'
            ],
            'fourth_section_video' => '281836525'
        ])
    ]);

    // 23 LSV
    \App\Models\ExploreBoats::create([
        'boat_id' => 2,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '23 LSV: THE BEST-SELLING TOWBOAT OF ALL TIME',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/23LSV/Studio/23LSV_Top.png',
            'second_section_header' => 'Legendary PERFORMANCE',
            'second_section_description' => 'The best-selling towboat of all time, and for good reason. At 23 feet, it’s the perfect size to make memories on the water with up to 15 people. The 23 LSV will generate an absolute wall of water when you want it, but with the Diamond Multisport Hull it can create approachable wakes for beginners and even mellow slalom wakes. With a classic traditional bow look, the 23 LSV is the essence of the original Malibu brand and still a legend in the industry.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-side.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-Front-port.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-rear-port.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-rear-star.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '23\'\"/7 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '15'
            ],
            'fourth_section_video' => '289001648'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '23 LSV: THE BEST-SELLING TOWBOAT OF ALL TIME',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/23LSV/Studio/23LSV_Top.png',
            'second_section_header' => 'Legendary PERFORMANCE',
            'second_section_description' => 'The best-selling towboat of all time, and for good reason. At 23 feet, it’s the perfect size to make memories on the water with up to 15 people. The 23 LSV will generate an absolute wall of water when you want it, but with the Diamond Multisport Hull it can create approachable wakes for beginners and even mellow slalom wakes. With a classic traditional bow look, the 23 LSV is the essence of the original Malibu brand and still a legend in the industry.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-side.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-Front-port.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-rear-port.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-rear-star.png',
                'https://www.malibuboatsasia.com/images/boats/23LSV/23LSV-top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '23\'\"/7 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '15'
            ],
            'fourth_section_video' => '289001648'
        ])
    ]);

    // 25 LSV
    \App\Models\ExploreBoats::create([
        'boat_id' => 1,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '25 LSV: THE NEW STANDARD',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/25LSV/Studio/25LSV18_bro_vrscan_0003.png',
            'second_section_header' => 'All-New for 2019',
            'second_section_description' => 'Based on the footprint of the 23 LSV—the best-selling towboat in history—the 25 LSV is one of the biggest boats in the Malibu line. With seating for 18 and over two tons of available ballast, even for Malibu this is a massive wake. Face the action with the Malibu Multi View Bench Seat™ when it’s time to ride or flip it over to set up a table when it’s time to relax. The 25 LSV is performance wrapped in luxury that will exceed your expectations.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_Side.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_front_port.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_rear_port.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_rear_star.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '25\'\"/7.62 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '18'
            ],
            'fourth_section_video' => '290347380'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => '25 LSV: THE NEW STANDARD',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/25LSV/Studio/25LSV18_bro_vrscan_0003.png',
            'second_section_header' => 'All-New for 2019',
            'second_section_description' => 'Based on the footprint of the 23 LSV—the best-selling towboat in history—the 25 LSV is one of the biggest boats in the Malibu line. With seating for 18 and over two tons of available ballast, even for Malibu this is a massive wake. Face the action with the Malibu Multi View Bench Seat™ when it’s time to ride or flip it over to set up a table when it’s time to relax. The 25 LSV is performance wrapped in luxury that will exceed your expectations.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_Side.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_front_port.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_rear_port.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_rear_star.png',
                'https://www.malibuboatsasia.com/images/boats/25LSV/25LSV_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '25\'\"/7.62 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '18'
            ],
            'fourth_section_video' => '290347380'
        ])
    ]);

    // 22 MXZ
    \App\Models\ExploreBoats::create([
        'boat_id' => 4,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'THE ALL NEW 22 MXZ',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0003.png',
            'second_section_header' => 'Maximized Lifestyle',
            'second_section_description' => 'The Malibu 2018 22 MXZ is engineered for comfort and entertainment, and is designed with extraordinary luxury and accessibility. The 22 MXZ features multiple industry-leading technologies, including the unparalleled MaxBallast system that delivers ultra-precise measurement of the L-shaped ballast tanks for limitless wake create, with convenient touch screen control from the digital Command Center. Experience maximum fun with your crew and enjoy unforgettable wakeboarding, wakesurfing and skiing on the new 2018 22 MXZ.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0000.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0001.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0002.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0003.png'
            ],
            'third_section_glance' => [
                'hull_length' => '22\'5\"/6.68 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '15'
            ],
            'fourth_section_video' => ''
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'THE ALL NEW 22 MXZ',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0003.png',
            'second_section_header' => 'Maximized Lifestyle',
            'second_section_description' => 'The Malibu 2018 22 MXZ is engineered for comfort and entertainment, and is designed with extraordinary luxury and accessibility. The 22 MXZ features multiple industry-leading technologies, including the unparalleled MaxBallast system that delivers ultra-precise measurement of the L-shaped ballast tanks for limitless wake create, with convenient touch screen control from the digital Command Center. Experience maximum fun with your crew and enjoy unforgettable wakeboarding, wakesurfing and skiing on the new 2018 22 MXZ.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0000.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0001.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0002.png',
                'https://www.malibuboatsasia.com/images/2018/22MXZ/Studio/22MXZ18_bro_vrscan_0003.png'
            ],
            'third_section_glance' => [
                'hull_length' => '22\'5\"/6.68 M',
                'beam' => '102\"/2.59 M',
                'draft' => '27\"/0.69 M',
                'max_capacity' => '15'
            ],
            'fourth_section_video' => ''
        ])
    ]);

    // 24 MXZ
    \App\Models\ExploreBoats::create([
        'boat_id' => 5,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'THE ALL NEW 24 MXZ',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0003.png',
            'second_section_header' => 'Maximize The Fun',
            'second_section_description' => 'A clear sky, calm water and good friends. Add a new 2018 Malibu 24 MXZ to the picture, and it’s a perfect day. In addition to best-in-class performance, the 24 MXZ is engineered for comfort and entertainment for up to 18 people. MultiZone Control offers unprecedented audio performance, while the walk-through design, wrap-around interior and WakeView seating provide extraordinary luxury and accessibility. Standard for the 2018 24 MXZ, the MaxBallast system boasts high-capacity, L-shaped tanks and optional Plug n’ Play ballast bags for incomparable wake and wave formation, while the touchscreen Command Center puts customization at your fingertips. New for 2018, Bow Thrusters are an exclusive option for the 24 MXZ, offering maximum maneuverability and directional control. Grab your friends and maximize your fun on the new 24 MXZ.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0000.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0001.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0002.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0003.png'
            ],
            'third_section_glance' => [
                'hull_length' => '24\'5\"/7.44 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '18'
            ],
            'fourth_section_video' => '229303640'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'THE ALL NEW 24 MXZ',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0003.png',
            'second_section_header' => 'Maximize The Fun',
            'second_section_description' => 'A clear sky, calm water and good friends. Add a new 2018 Malibu 24 MXZ to the picture, and it’s a perfect day. In addition to best-in-class performance, the 24 MXZ is engineered for comfort and entertainment for up to 18 people. MultiZone Control offers unprecedented audio performance, while the walk-through design, wrap-around interior and WakeView seating provide extraordinary luxury and accessibility. Standard for the 2018 24 MXZ, the MaxBallast system boasts high-capacity, L-shaped tanks and optional Plug n’ Play ballast bags for incomparable wake and wave formation, while the touchscreen Command Center puts customization at your fingertips. New for 2018, Bow Thrusters are an exclusive option for the 24 MXZ, offering maximum maneuverability and directional control. Grab your friends and maximize your fun on the new 24 MXZ.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0000.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0001.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0002.png',
                'https://www.malibuboatsasia.com/images/2018/24MXZ/Studio/24MXZ18_bro_vrscan_0003.png'
            ],
            'third_section_glance' => [
                'hull_length' => '24\'5\"/7.44 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '18'
            ],
            'fourth_section_video' => '229303640'
        ])
    ]);

    // M235
    \App\Models\ExploreBoats::create([
        'boat_id' => 6,
        'country_id' => $country_id,
        'lang_country' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'M235: ULTIMATE EVERYTHING',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/M235/Studio/M23518_bro_vrscan_0003.png',
            'second_section_header' => 'The most ultra-premium performance towboat the world has ever seen.',
            'second_section_description' => 'If you want the top of the line, look no further than the Alpha. The 23-foot M235 churns up wakes the pros dream about along with best-in-class luxury features. The M235 has no weaknesses.
                The 23-foot, 17-passenger M235 also delivers best-in-class luxury and performance, boasting Malibu’s most luxurious cabin, SeaDek flooring and an exclusive walk-through design. Of course, the M235 features the super-deep Wake Plus hull, the Quad Hard Tank Ballast System, the Plug ’n’ Play soft-bag system and Malibu’s exclusive Integrated Surf Platform with the all-new Power Wedge III for the most customized and extreme wake and wave-creation capabilities bar none.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Side.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '23\'5\"/7.14 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '17'
            ],
            'fourth_section_video' => '285574355'
        ]),
        'lang_en' => json_encode([
            'background_video' => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
            'first_section_header' => 'M235: ULTIMATE EVERYTHING',
            'second_section_boat_image' => 'https://www.malibuboatsasia.com/images/2018/M235/Studio/M23518_bro_vrscan_0003.png',
            'second_section_header' => 'The most ultra-premium performance towboat the world has ever seen.',
            'second_section_description' => 'If you want the top of the line, look no further than the Alpha. The 23-foot M235 churns up wakes the pros dream about along with best-in-class luxury features. The M235 has no weaknesses.
                The 23-foot, 17-passenger M235 also delivers best-in-class luxury and performance, boasting Malibu’s most luxurious cabin, SeaDek flooring and an exclusive walk-through design. Of course, the M235 features the super-deep Wake Plus hull, the Quad Hard Tank Ballast System, the Plug ’n’ Play soft-bag system and Malibu’s exclusive Integrated Surf Platform with the all-new Power Wedge III for the most customized and extreme wake and wave-creation capabilities bar none.',
            'third_section_image' => [
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Side.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Front_Port.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Rear_Port.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Rear_Star.png',
                'https://www.malibuboatsasia.com/images/boats/M235/M235_Top.png'
            ],
            'third_section_glance' => [
                'hull_length' => '23\'5\"/7.14 M',
                'beam' => '102\"/2.59 M',
                'draft' => '32\"/0.81 M',
                'max_capacity' => '17'
            ],
            'fourth_section_video' => '285574355'
        ])
    ]);
}
