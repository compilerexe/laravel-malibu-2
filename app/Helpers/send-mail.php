<?php

/* ===== Explain this file =====
This file use for create notifications between admin and dealer by send email.
===== End Explain ===== */

use Illuminate\Support\Facades\Mail;

function getAdminMail()
{
    $admin = \App\Models\Administrators::find(1);
    
    return $admin->email;
}

function sendMailToAdmin($message)
{
    $dealer  = \App\Models\Dealers::find(dealer()->id);
    $country = \App\Models\Countries::find($dealer->country_id);
    
    $data = [
        'country_name' => $country->country_name,
        'username'     => $dealer->username,
        'message'      => $message,
        'created_at'   => \Carbon\Carbon::now(),
    ];
    
    Mail::to(getAdminMail())->send(new \App\Mail\AdminNotifications($data));
}

function SendMailToDealer($message)
{
    $dealer = \App\Models\Dealers::find(session('accessDealerId'));
    $email  = $dealer->email;
    
    $data = [
        'message'    => $message,
        'created_at' => \Carbon\Carbon::now(),
    ];
    
    Mail::to($email)->send(new \App\Mail\DealerNotifications($data));
}

function sendMailContactUs($request)
{
    $data  = [
        'message'    => $request,
        'created_at' => \Carbon\Carbon::now(),
    ];
    $admin = \App\Models\Administrators::find(1);
    
    Mail::to($admin->email)->send(new \App\Mail\ContactUs($data));
}

function sendMailContactDealerFreeTestDrive($request)
{
    $country     = App\Models\Countries::find(session('country_id'));
    $data        = [
        'country_name' => $country->country_name,
        'message'      => $request,
        'created_at'   => \Carbon\Carbon::now(),
    ];
    $dealer_mail = $country->dealer->email;
    Mail::to($dealer_mail)->send(
        new \App\Mail\ContactDealerFreeTestDrive($data)
    );
    Mail::to(getAdminMail())->send(
        new \App\Mail\ContactDealerFreeTestDrive($data)
    );
}

function sendMailContactDealerRequestCatalog($request)
{
    $country     = App\Models\Countries::find(session('country_id'));
    $data        = [
        'country_name' => $country->country_name,
        'message'      => $request,
        'created_at'   => \Carbon\Carbon::now(),
    ];
    $dealer_mail = $country->dealer->email;
    Mail::to($dealer_mail)->send(
        new \App\Mail\ContactDealerRequestCatelog($data)
    );
    Mail::to(getAdminMail())->send(
        new \App\Mail\ContactDealerRequestCatelog($data)
    );
}

function sendMailContactDealerGeneral($request)
{
    $country     = App\Models\Countries::find(session('country_id'));
    $data        = [
        'country_name' => $country->country_name,
        'message'      => $request,
        'created_at'   => \Carbon\Carbon::now(),
    ];
    $dealer_mail = $country->dealer->email;
    Mail::to($dealer_mail)->send(
        new \App\Mail\ContactDealerGeneral($data)
    );
    Mail::to(getAdminMail())->send(
        new \App\Mail\ContactDealerGeneral($data)
    );
}

function sendMailContactDealerInventory($request)
{
    $country     = App\Models\Countries::find(session('country_id'));
    $data        = [
        'country_name' => $country->country_name,
        'message'      => $request,
        'created_at'   => \Carbon\Carbon::now(),
    ];
    $dealer_mail = $country->dealer->email;
    Mail::to($dealer_mail)->send(
        new \App\Mail\ContactDealerInventory($data)
    );
    Mail::to(getAdminMail())->send(
        new \App\Mail\ContactDealerInventory($data)
    );
}
