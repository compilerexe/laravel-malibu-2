<?php

/* ===== Explain this file =====
This file use for language translate between local & country by session.
===== End Explain ===== */

use Illuminate\Support\Facades\Auth;

function permission_allow()
{
    $data = null;
    
    if (Auth::check()) {
        $data = Auth::user();
    } else {
        $data = Auth::guard('dealer')->user();
    }
    
    return $data;
}

function convertDealerId()
{
    $dealer = \App\Models\Dealers::where('country_id', session('country_id'))
        ->first();
    
    return $dealer->id;
}

function getCountry()
{
	$country = \App\Models\Countries::find(session('country_id'));
	
	return $country;
}

function convertCountryName()
{
    if ( ! session()->has('country_id')) {
        session(['country_id' => 1]);
    }
    
    $country = \App\Models\Countries::find(session('country_id'));
    
    return $country->country_name;
}

function country_name()
{
    $country = \App\Models\Countries::find(get_country_id());
    
    return $country->country_name;
}

function get_country_id()
{
    $query = \App\Models\Countries::where('country_name', convertCountryName())
        ->first();
    
    return $query->id;
}

function get_short_country_name()
{
    $country = \App\Models\Countries::find(get_country_id());
    
    return $country->short_name;
}

function lang_navbar()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'navbar'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'navbar'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_home()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'home_page_header'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'home_page_header'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_all_boats()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'all_boats'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'all_boats'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_news_and_events()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'news_and_events'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'news_and_events'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_contact_dealer()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'contact_dealer'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'contact_dealer'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_button()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'button'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'button'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_form_input()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'form_input'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'form_input'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_contact_us()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'contact_us'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'contact_us'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_find_a_dealer()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'find_a_dealer'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'find_a_dealer'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_footer()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where(
            'tag', 'footer'
        )->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'footer'
        )->first();
    }
    
    return json_decode($lang->data);
}

function lang_other()
{
    if (session('flag_en_'.config('country')->short_name) === true) {
        $lang = \App\Models\Langs::where('country_id', 1)->where('tag', 'other')
            ->first();
    } else {
        $lang = \App\Models\Langs::where('country_id', get_country_id())->where(
            'tag', 'other'
        )->first();
    }
    
    return json_decode($lang->data);
}

function _format_json($json, $html = false)
{
    $tabcount   = 0;
    $result     = '';
    $inquote    = false;
    $ignorenext = false;
    if ($html) {
        $tab     = "&nbsp;&nbsp;&nbsp;";
        $newline = "<br/>";
    } else {
        $tab     = "\t";
        $newline = "\n";
    }
    for ($i = 0; $i < strlen($json); $i++) {
        $char = $json[$i];
        if ($ignorenext) {
            $result     .= $char;
            $ignorenext = false;
        } else {
            switch ($char) {
            case '{':
                $tabcount++;
                $result .= $char . $newline . str_repeat($tab, $tabcount);
                break;
            case '}':
                $tabcount--;
                $result = trim($result) . $newline . str_repeat($tab, $tabcount)
                    . $char;
                break;
            case ',':
                $result .= $char . $newline . str_repeat($tab, $tabcount);
                break;
            case '"':
                $inquote = ! $inquote;
                $result  .= $char;
                break;
            case '\\':
                if ($inquote) {
                    $ignorenext = true;
                }
                $result .= $char;
                break;
            default:
                $result .= $char;
            }
        }
    }
    
    return $result;
}

function found_errors()
{
    return 'Something went wrong!';
}

function txt_errors($msg)
{
    switch ($msg) {
    case 'try_again':
        $text = 'Please try again';
        break;
    default:
        $text = 'not found.';
        break;
    }
    
    return $text;
}

function txt_success()
{
    return 'Update successful.';
}
