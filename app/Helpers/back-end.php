<?php

/* ===== Explain this file =====
This file use for create new country with dummy data.
===== End Explain ===== */

function createAboutMalibu($country_id)
{
    \App\Models\AboutMalibu::create(
        [
            'country_id'                 => $country_id,
            'first_section_header'       => 'ABOUT MALIBU',
            'second_section_header'      => 'Malibu Innovations',
            'second_section_description' => 'In 2012 when Malibu introduced the revolutionary and industry changing Surf GateTM it was just the beginning of an evolution in design, engineering and innovation. Malibu’s engineering team continued to integrate our surf system into multiple facets of boat design over years to come to create a surf experience like no other.',
            'third_section_description'  => json_encode(
                [
                    'Surf Gate'         => [
                        'description' => 'With the other components of Malibu’s Integrated Surf Platform, Surf Gate creates the most dynamic playground for wake surfing possible. Period. Malibu’s ISP allows hundreds of customization options. No competitor comes close to offering that adjustability range.',
                        'vimeo'       => '285338431',
                    ],
                    'Power Wedge 3'     => [
                        'description' => 'The Power Wedge III makes your wakes and waves bigger by pulling down the back of the boat and saves gas by automatically getting you on plane faster. For 2019 it has five more degrees of lift for more usable
 positions and an improved exhaust pipe so it’s more effective and it looks better than ever. Our innovations are always evolving.',
                        'vimeo'       => '285338285',
                    ],
                    'Hard Tank Ballast' => [
                        'description' => 'Up to 5,000 lbs. across the wake setter line, our model specific quad hard tank ballast offers you the ability to set the size and shape of your ideal wake with incredible ease.',
                        'vimeo'       => '285338144',
                    ],
                    'Command Center'    => [
                        'description' => 'Directly ahead of the wheel, the bright, high-resolution 12-inch MaliView touchscreen gives you finger-tap control of the ballast, Power Wedge II, Surf Gate, and navigation, with fast, easy access to customizable rider presets, media controls and a variety of gauge displays.',
                        'vimeo'       => '226949216',
                    ],
                    'Malibu Gx Tower'   => [
                        'description' => 'Built by Malibu, the all-new Malibu Gx tower introduces an industry standard for innovation and quality. Turn the dial to raise or lower the tower in 9 seconds, half the time it takes for the competition.',
                        'vimeo'       => '285547396',
                    ],
                ]
            ),
        ]
    );
}

function createCountry($country_name, $display, $icon, $short_name, $lang_switch
) {
    $country = \App\Models\Countries::create(
        [
            'country_name' => $country_name,
            'display'      => $display,
            'short_name'   => $short_name,
            'icon'         => $icon,
            'lang_switch'  => $lang_switch,
        ]
    );
    
    return $country;
}

function createDealer($country_id, $username, $password, $email)
{
    $dealer = \App\Models\Dealers::create(
        [
            'country_id' => $country_id,
            'username'   => $username,
            'password'   => bcrypt($password),
            'email'      => $email,
        ]
    );
    
    return $dealer;
}

function createDealerProfile($dealer_id)
{
    \App\Models\DealerProfile::create(
        [
            'dealer_id'               => $dealer_id,
            'name_en'                 => 'Tasker Marine Group',
            'name_local'              => 'Tasker Marine Group',
            'address_en'              => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'address_local'           => 'Address: 2-4-5 Yagumodai chofushi, Tochigi, Tokyo, Japan 82361',
            'telephone'               => 'Telephone: 81-042-444-1125',
            'email'                   => 'Email: tada@joy.hi-ho.ne.jp',
            'short_description_en'    => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'short_description_local' => 'Tasker Marine is the only official Malibu boat dealer in Japan. We have established since 2002',
            'long_description_en'     => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'long_description_local'  => 'Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.","description_en":"Extremely we promotion remainder eagerness enjoyment an. Ham her demands removal brought minuter raising invited gay. Contented consisted continual curiosity contained get sex. Forth child dried in in aware do. You had met they song how feel lain evil near. Small she avoid six yet table china. And bed make say been then dine mrs. To household rapturous fulfilled attempted on so.',
            'dealer_photo'            => 'dealer/profile/dealer_photo.jpg',
            'facebook_link'           => 'https://www.facebook.com/malibuboatsjapan/',
            'instagram_link'          => 'https://www.instagram.com/malibuboatsjapan/',
            'website_link'            => 'https://www.malibuboat.jp',
        ]
    );
}

function createExploreBoat($country_id)
{
    foreach (\App\Models\Boats::all() as $boat) {
        \App\Models\ExploreBoats::create(
            [
                'boat_id'      => $boat->id,
                'country_id'   => $country_id,
                'lang_country' => json_encode(
                    [
                        'background_video'           => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
                        'first_section_header'       => 'TEXT',
                        'second_section_boat_image'  => asset(
                            'images/mockup.jpg'
                        ),
                        'second_section_header'      => 'TEXT',
                        'second_section_description' => 'TEXT',
                        'third_section_image'        => [
                            asset('images/mockup.jpg'),
                            asset('images/mockup.jpg'),
                            asset('images/mockup.jpg'),
                        ],
                        'third_section_glance'       => [
                            'hull_length'  => 'TEXT',
                            'beam'         => 'TEXT',
                            'draft'        => 'TEXT',
                            'max_capacity' => 'TEXT',
                        ],
                        'fourth_section_video'       => '233206915',
                    ]
                ),
                'lang_en'      => json_encode(
                    [
                        'background_video'           => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
                        'first_section_header'       => 'TEXT',
                        'second_section_boat_image'  => asset(
                            'images/mockup.jpg'
                        ),
                        'second_section_header'      => 'TEXT',
                        'second_section_description' => 'TEXT',
                        'third_section_image'        => [
                            asset('images/mockup.jpg'),
                            asset('images/mockup.jpg'),
                            asset('images/mockup.jpg'),
                        ],
                        'third_section_glance'       => [
                            'hull_length'  => 'TEXT',
                            'beam'         => 'TEXT',
                            'draft'        => 'TEXT',
                            'max_capacity' => 'TEXT',
                        ],
                        'fourth_section_video'       => '233206915',
                    ]
                ),
            ]
        );
    }
}

function createGoogleMaps($country_id)
{
    \App\Models\GoogleMaps::create(
        [
            'country_id'  => $country_id,
            'source_code' => '!1m18!1m12!1m3!1d3241.956016268786!2d139.55558881525806!3d35.6534552802008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f04610dc0ba7%3A0xeed16100443de292!2z4LiN4Li14LmI4Lib4Li44LmI4LiZIOOAkjE4Mi0wMDE1IFTFjWt5xY0tdG8sIENoxY1mdS1zaGksIFlhZ3Vtb2RhaSwgMiBDaG9tZeKIkjQsIOOCv-OCueOCq-ODvOODnuODquODs-iyqeWjsu-8iOagqu-8iQ!5e0!3m2!1sth!2sth!4v1540181273928',
        ]
    );
}

function CreateLangs($country_id)
{
    $country = App\Models\Countries::find($country_id);
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'navbar',
            'data'       => json_encode(
                [
                    'boats'           => 'BOATS',
                    'about_malibu'    => 'ABOUT MALIBU',
                    'news_and_events' => 'NEWS AND EVENTS',
                    'contact_dealer'  => 'CONTACT DEALER',
                    'find_a_dealer'   => 'FIND A DEALER',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'home_page_header',
            'data'       => json_encode(
                [
                    'malibu_boats_country'           => 'Malibu Boats '
                        . ucfirst(
                            $country->country_name
                        ),
                    'free_test_drive'                => 'FREE TEST DRIVE',
                    'explore_the_right_boat_for_you' => 'EXPLORE THE RIGHT BOAT FOR YOU',
                    'malibu_boat'                    => 'MALIBU BOAT',
                    'news_and_events'                => 'NEWS & EVENTS',
                    'about_malibu'                   => 'ABOUT MALIBU',
                    'about_malibu_content'           => 'Malibu\'s innovations have reshaped the watersports industry again and again.
                        Only Malibu offers the wave-making control of the Integrated Surf Platform™ (ISP) with Surf
                        Gate™, Power Wedge™ III Hydrofoil, Surf Band™ wrist remote and Malibu Command Center™ to control
                        all systems.',
                    'command_center'                 => 'COMMAND CENTER',
                    'integrated_surf_platform'       => 'INTEGRATED SURF PLATFORM',
                    'surf_gate'                      => 'SURF GATE',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'all_boats',
            'data'       => json_encode(
                [
                    'all_boats'                  => 'ALL BOATS',
                    'all'                        => 'ALL',
                    'wakesetter'                 => 'WAKESETTER',
                    'response'                   => 'RESPONSE',
                    'm235'                       => 'M235',
                    'nav_description_wakesetter' => 'Introduced in 1998, the Wakesetter is our top-selling series within the performance sport boat category. The Wakesetter is designed for customers seeking the highest-performance water sport and boating experience. Wakesetter offers consumers a highly- customizable boat with our Integrated Surf Platform (ISP), premium features, color options and interior finishes.',
                    'nav_description_response'   => 'The Response series, created in 1995, was designed for consumers who desire a high-performance water ski boat. Primarily because of its direct drive engine setup, the Response series produces the smallest wake of any of our boats and it is designed to accommodate both professional and recreational skiers by allowing for a range of speeds and line lengths.',
                    'nav_description_m235'       => 'For 35 years, Malibu has been pushing the envelope for towboat performance, capability and luxury, continuously raising the bar for ourselves and for the industry. With the Malibu M235, we’ve set a benchmark—one that even we will have a tough time topping. The new alpha, the M235 remains the definitive expression of Malibu’s next design and technology. Developed in utmost secrecy, Malibu’s new M235 was designed to be nothing less than the most ultra-premium performance towboat the world has ever seen, giving you the edge on the water anywhere, anytime.',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'news_and_events',
            'data'       => json_encode(
                [
                    'news_and_events' => 'NEWS & EVENTS',
                    'malibu_news'     => 'MALIBU NEWS',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'contact_dealer',
            'data'       => json_encode(
                [
                    'contact_dealer'            => 'CONTACT DEALER',
                    'free_test_drive'           => 'FREE TEST DRIVE',
                    'free_test_drive_content'   => 'EnjoyfreeMalibuboat test drive and surf the best wave produced by the revolutionary technology behind Malibu boat at your local dealer',
                    'request_a_catalog'         => 'REQUEST A CATALOG',
                    'request_a_catalog_content' => 'GetMalibuBoats catalog by filling in the form below and we will send the catalog to your email address',
                    'general'                   => 'GENERAL',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'button',
            'data'       => json_encode(
                [
                    'test_drive'            => 'TEST DRIVE',
                    'build'                 => 'BUILD',
                    'build_a_boat'          => 'BUILD A BOAT',
                    'schedule_a_test_drive' => 'SCHEDULE A TEST DRIVE',
                    'explore'               => 'EXPLORE',
                    'explore_boat'          => 'EXPLORE BOAT',
                    'request_a_catalog'     => 'REQUEST A CATALOG',
                    'inventory'             => 'INVENTORY',
                    'see_details'           => 'SEE DETAILS',
                    'submit'                => 'SUBMIT',
                    'show_more'             => 'Show More',
                    'read_more'             => '... Read More',
                    'get_direction'         => '... Get Direction',
                    'learn_more'            => 'LEARN MORE',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'footer',
            'data'       => json_encode(
                [
                    'boats'             => 'BOATS',
                    'about_malibu'      => 'ABOUT MALIBU',
                    'news_and_events'   => 'NEWS AND EVENTS',
                    'contact_dealer'    => 'CONTACT DEALER',
                    'find_dealer'       => 'FIND DEALER',
                    'test_drive'        => 'TEST DRIVE',
                    'build_a_boat'      => 'BUILD A BOAT',
                    'request_a_catalog' => 'REQUEST A CATALOG',
                    'inventory'         => 'INVENTORY',
                    'malibu_boats_asia' => 'MALIBU BOATS ASIA',
                    'axis_wake_asia'    => 'AXIS WAKE ASIA',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'form_input',
            'data'       => json_encode(
                [
                    'name'                  => 'NAME',
                    'email'                 => 'EMAIL',
                    'phone_number'          => 'PHONE NUMBER',
                    'date'                  => 'DATE',
                    'reason_for_test_drive' => 'REASON FOR TEST DRIVE',
                    'reason_for_contact'    => 'REASON FOR CONTACT',
                    'what_is_your_request'  => 'WHAT IS YOUR REQUEST',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'contact_us',
            'data'       => json_encode(
                [
                    'contact_us'         => 'CONTACT US',
                    'name'               => 'NAME',
                    'email'              => 'EMAIL',
                    'phone_number'       => 'PHONE NUMBER',
                    'country'            => 'COUNTRY',
                    'reason_for_contact' => 'REASON FOR CONTACT',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'find_a_dealer',
            'data'       => json_encode(
                [
                    'find_a_dealer'     => 'FIND A DEALER',
                    'view_asia_dealers' => 'VIEW ASIA DEALERS',
                ]
            ),
        ]
    );
    
    \App\Models\Langs::create(
        [
            'country_id' => $country_id,
            'tag'        => 'other',
            'data'       => json_encode(
                [
                    'about_dealer' => 'ABOUT DEALER',
                    'request_form' => 'REQUEST FORM',
                    'request_info' => 'REQUEST INFO',
                    'view_detail'  => 'VIEW DETAILS',
                    'year'         => 'YEAR',
                    'condition'    => 'CONDITION',
                    'new'          => 'NEW',
                    'used'         => 'USED',
                    'price'        => 'PRICE',
                    'hull_length'  => 'HULL LENGTH',
                    'beam'         => 'BEAM',
                    'draft'        => 'DRAFT',
                    'max_capacity' => 'MAX CAPACITY',
                    'text_form_1'  => 'You can request an additional information and schedule a boat
demonstration by filling in the form below',
                ]
            ),
        ]
    );
}
