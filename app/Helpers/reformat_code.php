<?php

/* ===== Explain this file =====
This file use create functions for client (front-end).
===== End Explain ===== */

function clientSessionCountryId()
{
    if ( ! session()->has('country_id')) {
        session(['country_id' => 1]);
    } else {
        $url_only_country = explode('/', \Request::path());
        if ($url_only_country[0] === '') {
            $url_only_country[0] = 'asia';
        }
        $country = \App\Models\Countries::where(
            'country_name', str_replace('-', ' ', $url_only_country[0])
        )->first();
        config()->set(
            'country', \App\Models\Countries::find($country->id)
        );
        session(['country_id' => $country->id]);
    }
    
    return session('country_id');
}

function clientSessionCountryName()
{
    if ( ! session()->has('client_country_name')) {
        return clientCountryName();
    }
    
    return urlCountryReplace(session('client_country_name'));
}

function clientOriginalCountryName()
{
    return session('client_country_name');
}

function clientCountryName()
{
    $country = \App\Models\Countries::find(clientSessionCountryId());
    session(['client_country_name' => $country->country_name]);
    
    return urlCountryReplace(session('client_country_name'));
}

function urlCountryReplace($country_name)
{
    return str_replace(' ', '-', $country_name);
}

function clientCountryUrl($path)
{
    return url(urlCountryReplace(clientSessionCountryName())) . $path;
}

function clientBoatUrl($model)
{
    return str_replace(' ', '-', strtolower($model));
}

function clientGetDealer()
{
    $dealer = \App\Models\Dealers::where('country_id', config('country')->id)
        ->first();
    
    return $dealer;
}

function clientGetDealerProfile()
{
    $dealer_profile = \App\Models\DealerProfile::find(clientGetDealer()->id);
    
    return $dealer_profile;
}

function clientNavbarURL($country_name, $path)
{
    return url('/' . urlCountryReplace($country_name) . $path);
}

function ipstack()
{
    $curl = curl_init();
    curl_setopt_array(
        $curl, [
            CURLOPT_URL => "http://api.ipstack.com/" . \Request::ip() . "?access_key=" . env('IPSTACK_API_KEY'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ],
        ]
    );
    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);
    
    if ($err) {
        return false;
    } else {
        //print_r(json_decode($response));
        return json_decode($response);
    }
}
