<?php

/* ===== Explain this file =====
This file use for create simple functions or frequently used.
===== End Explain ===== */

function imageFolder($path)
{
    return asset('app/' . $path);
}

function getDealerId()
{
    $dealer = \App\Models\Dealers::where('country_id', session('country_id'))
        ->first();
    
    if (is_null($dealer)) {
        return redirect('/asia');
    }
    
    return $dealer->id;
}
