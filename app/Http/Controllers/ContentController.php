<?php

/* ===== Explain this file =====
*** This file use for admin to manage content e.g. about malibu. ***
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\AboutMalibu;
use App\Models\Countries;
use App\Models\ExploreBoats;
use App\Models\GoogleMaps;
use App\Models\Langs;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    
    public function main()
    {
        $multiple_status = false;
        
        if (Auth::check()) {
            $countries = Countries::all();
            $multiple_status = true;
        } else {
            $country_id = permission_allow()->country_id;
            $countries  = Countries::find($country_id);
        }
        
        return view(
            'back-end.content.main', compact('countries', 'multiple_status')
        );
    }
    
    public function panel($country_id)
    {
        $country = Countries::find($country_id);
        session(['country_id' => $country_id]);
        
        return view('back-end.content.panel', compact('country'));
    }
    
    public function post_about_malibu(Request $request, $country_id)
    {
        AboutMalibu::where('country_id', $country_id)->update(
            $request->except('_token')
        );
        
        return redirect()->back()->with('update_status', true);
    }
    
    public function get_google_maps()
    {
        $google_maps = GoogleMaps::where(
            'country_id', dealer()->country_id
        )->first();
        
        return view(
            'back-end.new_design.dealer.google_maps.index',
            compact('google_maps')
        );
    }
    
    public function post_google_maps(Request $request, $id)
    {
        GoogleMaps::find($id)->update($request->except('_token'));
        SendMailToAdmin('Google Maps updated.');
        
        return redirect()->back()->with('update_status', 'success');
    }
    
    public function post_lang(Request $request, $country_id, $tag)
    {
        Langs::where('country_id', $country_id)
            ->where('tag', $tag)
            ->update($request->except('_token'));
        
        return redirect()->back()->with('update_status', true);
    }
}
