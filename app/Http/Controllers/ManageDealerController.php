<?php

/* ===== Explain this file =====
This file use for manage dealer information by admin only, e.g. news, inventory, profile.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\Countries;
use App\Models\DealerProfile;
use App\Models\Dealers;
use App\Models\GoogleMaps;
use App\Models\Inventories;
use App\Models\News;
use App\Models\Boats;
use Illuminate\Http\Request;

class ManageDealerController extends Controller
{
    
    /* ===== Profile ===== */
    public function getProfile($dealer_id)
    {
        $dealer = Dealers::find($dealer_id);
        session(['accessDealerId' => $dealer_id]);
        session(['accessCountryId' => $dealer->country_id]);
        session(['accessCountry' => Countries::find($dealer->country_id)]);
        
        return view('back-end.new_design.manage.profile', compact('dealer'));
    }
    
    public function postProfile(Request $request)
    {
        $dealerId     = session('accessDealerId');
        $dealer_photo = $request->file('dealer_photo');
        if ($dealer_photo) {
            $path = $dealer_photo->store('dealer/profile');
            DealerProfile::find($dealerId)->update(['dealer_photo' => $path]);
        }
        DealerProfile::find($dealerId)->update(
            $request->except('_token', 'dealer_photo')
        );
        
        return redirect()->back()->with('update_status', true);
    }
    /* ===== End Profile ===== */
    
    /* ===== News ===== */
    public function getNews($dealer_id)
    {
        $dealer    = Dealers::find($dealer_id);
        $countryId = session('accessCountryId');
        $news      = News::where('country_id', $countryId)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        
        return view(
            'back-end.new_design.manage.news.index', compact('news', 'dealer')
        );
    }
    
    public function getNewsCreate()
    {
        return view('back-end.new_design.manage.news.create');
    }
    
    public function getNewsEdit($id)
    {
        $news = News::find($id);
        
        return view('back-end.new_design.manage.news.edit', compact('news'));
    }
    
    public function postNewsStore(Request $request)
    {
        $news_header_local      = $request->news_header_local;
        $news_description_local = $request->news_description_local;
        $news_header_en         = $request->news_header_en;
        $news_description_en    = $request->news_description_en;
        $cover_image            = $request->file('cover_image');
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            News::create(
                [
                    'country_id'             => session('accessCountryId'),
                    'cover_image'            => $path,
                    'news_header_local'      => $news_header_local,
                    'news_description_local' => $news_description_local,
                    'news_header_en'         => $news_header_en,
                    'news_description_en'    => $news_description_en,
                    'home_active'            => 0,
                ]
            );
        }
        
        return redirect()->route(
            'manage.dealer.news.index',
            ['dealer_id' => session('accessDealerId')]
        )->with('insert_status', true);
    }
    
    public function postNewsUpdate(Request $request, $id)
    {
        $news_header_local      = $request->news_header_local;
        $news_description_local = $request->news_description_local;
        $news_header_en         = $request->news_header_en;
        $news_description_en    = $request->news_description_en;
        $cover_image            = $request->file('cover_image');
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            News::find($id)->update(
                [
                    'cover_image' => $path,
                ]
            );
        }
        
        News::find($id)->update(
            [
                'country_id'             => session('accessCountryId'),
                'news_header_local'      => $news_header_local,
                'news_description_local' => $news_description_local,
                'news_header_en'         => $news_header_en,
                'news_description_en'    => $news_description_en,
            ]
        );
        
        return redirect()->route(
            'manage.dealer.news.index',
            ['dealer_id' => session('accessDealerId')]
        )->with('update_status', true);
    }
    
    public function postNewsDestroy($id)
    {
        News::destroy($id);
    }
    
    public function postRequestNewsCreate($id)
    {
        News::find($id)->update(['create_approve' => 1]);
        SendMailToDealer('Request create news is approved from admin.');
    }
    
    public function postRequestNewsUpdate($id)
    {
        $news = News::find($id);
        $news->update(
            [
                'news_header_local'      => $news->copy_news_header_local,
                'news_description_local' => $news->copy_news_description_local,
                'news_header_en'         => $news->copy_news_header_en,
                'news_description_en'    => $news->copy_news_description_en,
                'update_approve'         => $news->update_count,
            ]
        );
        SendMailToDealer('Request news update is approved from admin.');
    }
    
    public function postRequestNewsRemove($id)
    {
        //News::find($id)->update(['delete_approve' => 2]);
        News::destroy($id);
        SendMailToDealer('Request remove news is approved from admin.');
    }
    /* ===== End News ===== */
    
    /* ===== Inventory ===== */
    public function getInventory()
    {
        $dealer      = Dealers::find(session('accessDealerId'));
        $inventories = Inventories::where(
            'dealer_id', session('accessDealerId')
        )
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        
        return view(
            'back-end.new_design.manage.inventory.index',
            compact('inventories', 'dealer')
        );
    }
    
    public function getInventoryCreate()
    {
        $boats = Boats::all();
        
        return view(
            'back-end.new_design.manage.inventory.create', compact('boats')
        );
    }
    
    public function getInventoryEdit($id)
    {
        $boats     = Boats::all();
        $inventory = Inventories::find($id);
        
        return view(
            'back-end.new_design.manage.inventory.edit',
            compact('inventory', 'boats')
        );
    }
    
    public function postInventoryStore(Request $request)
    {
        $boat_model        = $request->boat_model;
        $year              = $request->year;
        $price             = $request->price;
        $boat_detail_local = $request->boat_detail_local;
        $boat_detail_en    = $request->boat_detail_en;
        $photos            = [];
        $used              = $request->used;
        
        /* --- file uploads --- */
        $file_uploads = $request->file('file_uploads');
        if ($file_uploads) {
            $sort_index = 1;
            foreach ($file_uploads as $file) {
                $path = $file->store('dealer/inventory');
                array_push(
                    $photos, [
                        'sort'  => $sort_index,
                        'image' => $path,
                    ]
                );
                $sort_index++;
            }
        }
        
        Inventories::create(
            [
                'dealer_id'         => session('accessDealerId'),
                'boat_id'           => $boat_model,
                'year'              => $year,
                'price'             => $price,
                'boat_photos'       => json_encode($photos),
                'boat_detail_local' => $boat_detail_local,
                'boat_detail_en'    => $boat_detail_en,
                'used'              => $used,
            ]
        );
        
        return redirect()->route(
            'manage.dealer.inventory.index',
            ['dealer_id' => session('accessDealerId')]
        )->with('insert_status', true);
    }
    
    public function postInventoryUpdate(Request $request, $id)
    {
        $boat_model        = $request->boat_model;
        $year              = $request->year;
        $price             = $request->price;
        $boat_detail_local = $request->boat_detail_local;
        $boat_detail_en    = $request->boat_detail_en;
        $inventory         = Inventories::find($id);
        $sort              = $request->sort;
        $sort_photos       = [];
        $used              = $request->used;
        
        if ($sort) {
            $boat_photos = json_decode($inventory->boat_photos);
            foreach ($boat_photos as $key => $photo) {
                foreach ($sort as $k => $v) {
                    if ($key == $k) {
                        array_push(
                            $sort_photos, [
                                'sort'  => intval($v),
                                'image' => $photo->image,
                            ]
                        );
                    }
                }
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
        
        /* --- file uploads --- */
        $file_uploads = $request->file('file_uploads');
        if ($file_uploads) {
            foreach ($file_uploads as $file) {
                $path = $file->store('dealer/inventory');
                if ( ! empty($sort)) {
                    $sort_id = max($sort) + 1;
                } else {
                    $sort_id = 1;
                }
                array_push(
                    $sort_photos, [
                        'sort'  => $sort_id,
                        'image' => $path,
                    ]
                );
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
        
        $inventory->update(
            [
                'boat_id'           => $boat_model,
                'year'              => $year,
                'price'             => $price,
                'boat_detail_local' => $boat_detail_local,
                'boat_detail_en'    => $boat_detail_en,
                'used'              => $used,
            ]
        );
        
        return redirect()->back()->with('update_status', true);
    }
    
    public function postInventoryPhotoRemove($id, $sort)
    {
        $inventory = Inventories::find($id);
        
        if ($sort) {
            $boat_photos = json_decode($inventory->boat_photos);
            $sort_photos = [];
            foreach ($boat_photos as $key => $photo) {
                if ($photo->sort != $sort) {
                    array_push(
                        $sort_photos, [
                            'sort'  => intval($photo->sort),
                            'image' => $photo->image,
                        ]
                    );
                }
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
    }
    
    public function postInventoryDestroy($id)
    {
        Inventories::destroy($id);
    }
    /* ===== End Inventory ===== */
    
    /* ===== Google Maps ===== */
    
    public function getGoogleMaps()
    {
        $google_maps = GoogleMaps::where(
            'country_id', session('accessCountryId')
        )->first();
        
        return view(
            'back-end.new_design.manage.google_maps.index',
            compact('google_maps')
        );
    }
    
    public function postGoogleMaps($id, Request $request)
    {
        GoogleMaps::find($id)->update($request->except('_token'));
        
        return redirect()->back()->with('update_status', true);
    }
    
    /* ===== End Google Maps ===== */
}
