<?php

/* ===== Explain this file =====
This file use for create API functions for client e.g. fetch explore boat.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\AboutMalibu;
use App\Models\ExploreBoats;
use App\Models\News;
use Illuminate\Http\Request;
use App\Models\Boats;

class APIController extends Controller
{
    
    public function fetch_explore_boats(Request $request)
    {
        $boat = Boats::where('model', $request->model)->first();
        $data = [
            'model' => $boat->model,
            'tag'   => $boat->tag,
            'image' => $boat->boat_front_image,
            'link'  => url(
                str_replace(
                    ' ', '-', convertCountryName() . '/boat/' . $boat->model
                )
            ),
        ];
        
        return response()->json($data);
    }
    
    public function fetch_nav_description()
    {
        return response()->json(lang_all_boats());
    }
    
    /* ----- JSON ----- */
    public function json_about_malibu(Request $request)
    {
        $data = AboutMalibu::where('country_id', $request->country_id)->first();
        
        return response()->json($data->third_section_description);
    }
    
    //    public function json_contact_dealer(Request $request)
    //    {
    //        $data = ContactDealers::where('country_id', $request->country_id)->first();
    //        $obj = [
    //            'first_section_address' => $data->first_section_address,
    //            'third_section_description' => $data->third_section_description
    //        ];
    //
    //        return response()->json($obj);
    //    }
    
    public function json_explore_boat(Request $request)
    {
        $data = ExploreBoats::where('country_id', $request->country_id)
            ->where('id', $request->explore_boat_id)
            ->first();
        
        $boat = Boats::find($data->boat_id);
        
        return $boat->model;
    }
    
    public function json_explore_boat_detail(Request $request)
    {
        $data = ExploreBoats::where('country_id', $request->country_id)
            ->where('id', $request->explore_boat_id)
            ->first();
        
        $obj = [
            'lang_country' => $data->lang_country,
            'lang_en'      => $data->lang_en,
        ];
        
        return response()->json($obj);
    }
    
    public function set_home_active(Request $request)
    {
        $news_id = $request->news_id;
        $news    = News::find($news_id);
        $status  = false;
        
        if ($news->home_active) {
            $news->update(['home_active' => false]);
        } else {
            $news->update(['home_active' => true]);
            $status = true;
        }
        
        return $status;
    }
    
    public function fetchNewsPaginate(Request $request)
    {
        $page = $request->page;
        
        $news = News::where('country_id', session('country_id'))
            ->orderBy('created_at', 'desc')->get();
        
        if (convertCountryName() !== 'asia') {
            $news = $news->where('create_approve', 1);
        }
        
        $showMore  = false;
        $totalNews = $news->count();
        $news      = $news->slice($page * 3, 3);
        
        if ($totalNews > (($page * 3) + 1)) {
            $showMore = true;
        }
        $result = [];
        
        foreach ($news as $data) {
            
            $link = url(
                country_name() . '/news/' . $data->id
            );
            if (session('flag_en') || country_name() == 'asia') {
                $news_header      = $data->news_header_en;
                $news_description = $data->news_description_en;
            } else {
                $news_header      = $data->news_header_local;
                $news_description = $data->news_description_local;
            }
            
            $news_description = strip_tags(str_limit($news_description, 300));
            
            array_push(
                $result, '<div class="card card-plain card-blog">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-5 col-lg-5">
                                        <a href="' . $link . '"
                                           class="sub-header font-fz-b">
                                            <div class="card-image" style="padding-top: 15px;">
                                                <img class="img"
                                                     src="' . imageFolder(
                    $data->cover_image
                ) . '"
                                                     style="width: 367px; height: 245px;">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-7 col-lg-7">
                                        <div class="card-body" style="padding-top: 0; margin-top: 0;">
                                            <h3 class="card-title">
                                                <a href="' . $link . '"
                                                   class="sub-header font-fz-b">
                                                    ' . $news_header . '
                        </a>
                    </h3>
                    <p class="card-description">
' . $news_description . '
                        </p>
                        <br>
                        <a href="' . $link . '" class="font-fz-b">'
                . lang_button()->see_details . '</a>
                </div>
            </div>
        </div>
    </div>
    <br>'
            );
        }
        
        $data = [$result, $showMore];
        
        return $data;
    }
}
