<?php

/* ===== Explain this file =====
This file use for users send form data (front-end).
===== End Explain ===== */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SendMailController extends Controller
{
    
    public function postContactUs(Request $request)
    {
        sendMailContactUs($request);
        
        return redirect()->back()->with('send_mail_contact_us', true);
    }
    
    public function postContactDealerFreeTestDrive(Request $request)
    {
        sendMailContactDealerFreeTestDrive($request);
        
        return redirect()->back()->with(
            'send_mail_contact_dealer', 'free-test-drive'
        );
    }
    
    public function postContactDealerRequestCatalog(Request $request)
    {
        sendMailContactDealerRequestCatalog($request);
        
        return redirect()->back()->with(
            'send_mail_contact_dealer', 'request-catalog'
        );
    }
    
    public function postContactDealerGeneral(Request $request)
    {
        sendMailContactDealerGeneral($request);
        
        return redirect()->back()->with(
            'send_mail_contact_dealer', 'general'
        );
    }
    
    public function postContactDealerInventory(Request $request)
    {
        sendMailContactDealerInventory($request);
        
        return redirect()->back()->with(
            'send_mail_contact_dealer', 'inventory'
        );
    }
}
