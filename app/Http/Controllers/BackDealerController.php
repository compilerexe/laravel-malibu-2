<?php

/* ===== Explain this file =====
This file use for back-end dealer login only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Mail\AdminNotifications;
use App\Models\Boats;
use App\Models\Countries;
use App\Models\DealerProfile;
use App\Models\Dealers;
use App\Models\Inventories;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BackDealerController extends Controller
{
    
    public function profile()
    {
        $dealer_profile = DealerProfile::where('dealer_id', dealer()->id)
            ->first();
        session(
            [
                'accessCountry' => Countries::find(
                    $dealer_profile->dealer->country_id
                ),
            ]
        );
        
        return view(
            'back-end.new_design.dealer.profile', compact('dealer_profile')
        );
    }
    
    public function news()
    {
        $news = News::where('country_id', dealer()->country_id)
            //->where('create_approve', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        
        return view('back-end.new_design.dealer.news.index', compact('news'));
    }
    
    public function news_create()
    {
        return view('back-end.new_design.dealer.news.create');
    }
    
    public function news_edit($id)
    {
        $news = News::find($id);
        
        return view('back-end.new_design.dealer.news.edit', compact('news'));
    }
    
    public function inventory()
    {
        $inventories = Inventories::where('dealer_id', dealer()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        
        return view(
            'back-end.new_design.dealer.inventory.index', compact('inventories')
        );
    }
    
    public function inventory_create()
    {
        $boats = Boats::all();
        
        return view(
            'back-end.new_design.dealer.inventory.create', compact('boats')
        );
    }
    
    public function inventory_edit($id)
    {
        $boats     = Boats::all();
        $inventory = Inventories::find($id);
        
        return view(
            'back-end.new_design.dealer.inventory.edit',
            compact('inventory', 'boats')
        );
    }
    
    public function account_setting()
    {
        return view('back-end.new_design.dealer.account_setting');
    }
    
    public function post_account_setting(Request $request)
    {
        Dealers::find(dealer()->id)->update(['email' => $request->email]);
        SendMailToAdmin('Account setting updated.');
        
        return redirect()->back()->with('user_information', true);
    }
    
    /* ----- POST REQUEST ----- */
    
    public function post_profile(Request $request)
    {
        $dealer_photo = $request->file('dealer_photo');
        if ($dealer_photo) {
            $path = $dealer_photo->store('dealer/profile');
            DealerProfile::find(dealer()->id)->update(
                ['dealer_photo' => $path]
            );
        }
        DealerProfile::find(dealer()->id)->update(
            $request->except('_token', 'dealer_photo')
        );
        
        SendMailToAdmin('Profile updated.');
        
        return redirect()->back()->with('update_status', 'success');
    }
    
    public function news_store(Request $request)
    {
        $news_header_local      = $request->news_header_local;
        $news_description_local = $request->news_description_local;
        $news_header_en         = $request->news_header_en;
        $news_description_en    = $request->news_description_en;
        $cover_image            = $request->file('cover_image');
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            News::create(
                [
                    'country_id'             => dealer()->country_id,
                    'cover_image'            => $path,
                    'news_header_local'      => $news_header_local,
                    'news_description_local' => $news_description_local,
                    'news_header_en'         => $news_header_en,
                    'news_description_en'    => $news_description_en,
                    'home_active'            => 0,
                ]
            );
        }
        
        SendMailToAdmin('Request for approve create news.');
        
        return redirect()->route('dealer.news.index')->with(
            'update_status', 'wait_approve'
        );
    }
    
    public function news_update(Request $request, $id)
    {
        $news_header_local      = $request->news_header_local;
        $news_description_local = $request->news_description_local;
        $news_header_en         = $request->news_header_en;
        $news_description_en    = $request->news_description_en;
        $cover_image            = $request->file('cover_image');
        
        $news = News::find($id);
        
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            $news->update(
                [
                    'copy_cover_image' => $path,
                ]
            );
        }
        
        $update_count = $news->update_count + 1;
        
        $news->update(
            [
                'copy_news_header_local'      => $news_header_local,
                'copy_news_description_local' => $news_description_local,
                'copy_news_header_en'         => $news_header_en,
                'copy_news_description_en'    => $news_description_en,
                'update_count'                => $update_count,
            ]
        );
        
        SendMailToAdmin('Request for approve update news.');
        
        return redirect()->route('dealer.news.index')->with(
            'update_status', 'wait_approve'
        );
    }
    
    public function news_destroy($id)
    {
        $news = News::find($id);
        
        if ($news->create_approve === 0) {
            $news->delete();
            return 'deleted';
        } else {
            SendMailToAdmin('Request for approve remove news.');
            $news->update(['delete_approve' => 1]);
        }
    }
    
    public function inventory_store(Request $request)
    {
        $boat_model        = $request->boat_model;
        $year              = $request->year;
        $price             = $request->price;
        $boat_detail_local = $request->boat_detail_local;
        $boat_detail_en    = $request->boat_detail_en;
        $photos            = [];
        $used              = $request->used;
        
        /* --- file uploads --- */
        $file_uploads = $request->file('file_uploads');
        if ($file_uploads) {
            $sort_index = 1;
            foreach ($file_uploads as $file) {
                $path = $file->store('dealer/inventory');
                array_push(
                    $photos, [
                        'sort'  => $sort_index,
                        'image' => $path,
                    ]
                );
                $sort_index++;
            }
        }
        
        Inventories::create(
            [
                'dealer_id'         => dealer()->id,
                'boat_id'           => $boat_model,
                'year'              => $year,
                'price'             => $price,
                'boat_photos'       => json_encode($photos),
                'boat_detail_local' => $boat_detail_local,
                'boat_detail_en'    => $boat_detail_en,
                'used'              => $used,
            ]
        );
        
        SendMailToAdmin('Inventory created.');
        
        return redirect()->route('dealer.inventory.index')->with(
            'insert_status', true
        );
    }
    
    public function inventory_update(Request $request, $id)
    {
        $boat_model        = $request->boat_model;
        $year              = $request->year;
        $price             = $request->price;
        $boat_detail_local = $request->boat_detail_local;
        $boat_detail_en    = $request->boat_detail_en;
        $inventory         = Inventories::find($id);
        $sort              = $request->sort;
        $sort_photos       = [];
        $used              = $request->used;
        
        if ($sort) {
            $boat_photos = json_decode($inventory->boat_photos);
            foreach ($boat_photos as $key => $photo) {
                foreach ($sort as $k => $v) {
                    if ($key == $k) {
                        array_push(
                            $sort_photos, [
                                'sort'  => intval($v),
                                'image' => $photo->image,
                            ]
                        );
                    }
                }
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
        
        /* --- file uploads --- */
        $file_uploads = $request->file('file_uploads');
        if ($file_uploads) {
            foreach ($file_uploads as $file) {
                $path = $file->store('dealer/inventory');
                if ( ! empty($sort)) {
                    $sort_id = max($sort) + 1;
                } else {
                    $sort_id = 1;
                }
                array_push(
                    $sort_photos, [
                        'sort'  => $sort_id,
                        'image' => $path,
                    ]
                );
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
        
        $inventory->update(
            [
                'boat_id'           => $boat_model,
                'year'              => $year,
                'price'             => $price,
                'boat_detail_local' => $boat_detail_local,
                'boat_detail_en'    => $boat_detail_en,
                'used'              => $used,
            ]
        );
        
        SendMailToAdmin('Inventory updated.');
        
        return redirect()->back()->with('update_status', 'success');
    }
    
    public function inventory_photo_remove($id, $sort)
    {
        $inventory = Inventories::find($id);
        
        if ($sort) {
            $boat_photos = json_decode($inventory->boat_photos);
            $sort_photos = [];
            foreach ($boat_photos as $key => $photo) {
                if ($photo->sort != $sort) {
                    array_push(
                        $sort_photos, [
                            'sort'  => intval($photo->sort),
                            'image' => $photo->image,
                        ]
                    );
                }
            }
            $inventory->update(['boat_photos' => json_encode($sort_photos)]);
        }
    }
    
    public function inventory_destroy($id)
    {
        SendMailToAdmin('Inventory remove.');
        
        Inventories::destroy($id);
    }
    
    /* ------------------------ */
    
    public function getGoogleMaps()
    {
        return view('back-end.new_design.dealer.google_maps.index');
    }
}
