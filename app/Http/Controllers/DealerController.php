<?php

/* ===== Explain this file =====
This file use for manage dealer by admin only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\Dealers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DealerController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = Dealers::all();
        
        return view('back-end.dealers.index', compact('dealers'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.dealers.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Dealers::create($request->except('_token'));
        
        return redirect()
            ->route('dealers.index')
            ->with('update_status', true);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dealers $dealer
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Dealers $dealer)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dealers $dealer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Dealers $dealer)
    {
        return view('back-end.dealers.edit', compact('dealer'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Dealers      $dealer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dealers $dealer)
    {
        $username = $request->username;
        $password = $request->password;
        $email    = $request->email;
        
        if ($password) {
            $this->validate(
                $request, [
                    'username' => [
                        'required',
                        'min:4',
                        Rule::unique('dealers')->ignore($dealer->id),
                    ],
                    'password' => 'required|min:6|confirmed',
                ]
            );
            
            $dealer->update(
                [
                    'username' => $username,
                    'password' => bcrypt($password),
                    'email'    => $email,
                ]
            );
        } else {
            $this->validate(
                $request, [
                    'username' => [
                        'required',
                        'min:4',
                        Rule::unique('dealers')->ignore($dealer->id),
                    ],
                ]
            );
    
            $dealer->update(
                [
                    'username' => $username,
                    'email'    => $email,
                ]
            );
        }
        
        return redirect()
            ->route('dealers.index')
            ->with('update_status', true);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dealers $dealer
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dealers $dealer)
    {
        //
    }
}
