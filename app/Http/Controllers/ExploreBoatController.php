<?php

/* ===== Explain this file =====
This file use for manage explore boats by admin only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\ExploreBoats;
use Illuminate\Http\Request;

class ExploreBoatController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $country_id = $request->country_id;
        $explore_boats = ExploreBoats::where('country_id', $country_id)
            ->paginate(20);
        
        return view(
            'back-end.content.explore_boat.index',
            compact('explore_boats', 'country_id')
        );
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $explore_boat = ExploreBoats::find($id);
        
        return view(
            'back-end.content.explore_boat.edit', compact('explore_boat')
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ExploreBoats::find($id)->update($request->except('_token', '_method'));
        
        return redirect()
            ->route(
                'manage.content.explore_boat.index', ['country_id' => session('country_id')]
            )
            ->with('update_status', true);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ExploreBoats::destroy($id);
    }
}
