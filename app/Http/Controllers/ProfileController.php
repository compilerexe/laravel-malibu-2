<?php

/* ===== Explain this file =====
This file use for manage login information between admin or dealer.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\Administrators;
use App\Models\Dealers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    
    public function index()
    {
        return view('back-end.profile.index');
    }
    
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            Administrators::find($id)->update(
                [
                    'email' => $request->email,
                ]
            );
        } else {
            Dealers::find($id)->update(
                [
                    'email' => $request->email,
                ]
            );
        }
        
        return redirect()->back()->with('user_information', true);
    }
    
    public function password_update(Request $request, $id)
    {
        if ($request->password !== $request->password_confirmation) {
            return redirect()->back()->with('password_status', 'not match');
        }
        
        if (Auth::check()) {
            Administrators::find($id)->update(
                [
                    'password' => bcrypt($request->password),
                ]
            );
        } else {
            Dealers::find($id)->update(
                [
                    'password' => bcrypt($request->password),
                ]
            );
        }
        
        return redirect()->back()->with('password_update', true);
    }
}
