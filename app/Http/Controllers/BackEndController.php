<?php

/* ===== Explain this file =====
This file use for global back-end functions.
===== End Explain ===== */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BackEndController extends Controller
{
    
    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('language.index');
        } else {
            if (Auth::guard('dealer')->check()) {
                return redirect()->route('dealer.profile');
            }
        }
        
        return view('back-end.login');
    }
    
    public function post_login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        
        if (Auth::attempt($credentials)) {
            if (Auth::guard('dealer')->check()) {
                Auth::guard('dealer')->logout();
            }
            
            return redirect()->route('language.index');
        } else {
            if (Auth::guard('dealer')->attempt($credentials)) {
                if (Auth::check()) {
                    Auth::logout();
                }
                session(
                    [
                        'accessCountryId' => Auth::guard('dealer')->user()->country_id,
                    ]
                );
                
                return redirect()->route('dealer.profile');
            }
        }
        
        return redirect()->back()->with('errors', txt_errors('try_again'));
    }
    
    public function logout()
    {
        Auth::logout();
        
        return redirect('/admin');
    }
    
    public function dealer_logout()
    {
        Auth::guard('dealer')->logout();
        
        return redirect('/admin');
    }
}
