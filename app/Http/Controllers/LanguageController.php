<?php

/* ===== Explain this file =====
This file use for manage language by admin only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\AboutMalibu;
use App\Models\Countries;
use App\Models\DealerProfile;
use App\Models\Dealers;
use App\Models\ExploreBoats;
use App\Models\GoogleMaps;
use App\Models\Inventories;
use App\Models\Langs;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LanguageController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $multiple_status = false;
        
        if (Auth::check()) {
            $countries       = Countries::all();
            $multiple_status = true;
        } else {
            $country_id = permission_allow()->country_id;
            $countries  = Countries::find($country_id);
        }
        
        return view(
            'back-end.language.index', compact('countries', 'multiple_status')
        );
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.language.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country_name = $request->country_name;
        $display      = $request->display;
        $icon         = $request->icon;
        $short_name   = $request->short_name;
        $lang_switch  = $request->lang_switch;
        $username     = $request->username;
        $email        = $request->email;
        $password     = $request->password;
        
        $this->validate(
            $request, [
                'username' => 'required|min:4|unique:dealers,username',
                'email'    => 'required|unique:dealers,email',
                'password' => 'required|min:6|confirmed',
            ]
        );
        
        $country = createCountry(
            $country_name, $display, $icon, $short_name, $lang_switch
        );
        $dealer  = createDealer($country->id, $username, $password, $email);
        createDealerProfile($dealer->id);
        createAboutMalibu($country->id);
        createExploreBoat($country->id);
        createGoogleMaps($country->id);
        CreateLangs($country->id);
        createExploreBoat($country->id);
        
        return redirect()->route(
            'access.manage.dealer.profile', ['dealer_id' => $dealer->id]
        );
        
        //return redirect()->route('language.index')->with('insert_status', true);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Countries::find($id);
        
        return view('back-end.language.edit', compact('country'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Countries::find($id)->update($request->except('_token', '_method'));
        
        return redirect()->back()->with('language_update', true);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AboutMalibu::where('country_id', $id)->delete();
        Countries::destroy($id);
        $dealer    = Dealers::where('country_id', $id)->first();
        $dealer_id = $dealer->id;
        $dealer->delete();
        DealerProfile::where('dealer_id', $dealer_id)->delete();
        ExploreBoats::where('country_id', $id)->delete();
        GoogleMaps::where('country_id', $id)->delete();
        Inventories::where('dealer_id', $dealer_id)->delete();
        Langs::where('country_id', $id)->delete();
        News::where('country_id', $id)->delete();
    }
}
