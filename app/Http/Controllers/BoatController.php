<?php

/* ===== Explain this file =====
This file use for manage boats only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\Boats;
use App\Models\Countries;
use App\Models\ExploreBoats;
use Illuminate\Http\Request;

class BoatController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boats = Boats::paginate(20);
        
        return view('back-end.boats.index', compact('boats'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Countries::all();
        
        return view('back-end.boats.create', compact('countries'));
    }
    
    public function create_boat($boat_id, $country_id)
    {
        ExploreBoats::create(
            [
                'boat_id'      => $boat_id,
                'country_id'   => $country_id,
                'lang_country' => json_encode(
                    [
                        'background_video'           => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
                        'first_section_header'       => 'TXi OPEN BOW',
                        'second_section_boat_image'  => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Top.png',
                        'second_section_header'      => '11-Time World Record Holder',
                        'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
                        'third_section_image'        => [
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Side.png',
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png',
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Rear.png',
                        ],
                        'third_section_glance'       => [
                            'hull_length'  => '20\'6\"/6.25 M',
                            'beam'         => '95\"/2.41 M',
                            'draft'        => '22\"/0.56 M',
                            'max_capacity' => '7',
                        ],
                        'fourth_section_video'       => '233206915',
                    ]
                ),
                'lang_en'      => json_encode(
                    [
                        'background_video'           => 'https://cdn.malibuboats.com/videos/models/response-txi/header.mp4',
                        'first_section_header'       => 'TXi OPEN BOW',
                        'second_section_boat_image'  => 'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Top.png',
                        'second_section_header'      => '11-Time World Record Holder',
                        'second_section_description' => 'The original Malibu Response TXi is a legendary ski boat, pulling pro riders to 11 world records. Now, the all-new 2018 TXi is designed to deliver the next generation of dominance—and it’s already rewriting the record books. The three-finned T-Cut Diamond Hull creates astoundingly soft, flat wakes, and the TXi’s laser-like tracking and precision handling enable it to pull with incredible consistency. Blending function with renowned Malibu luxury and bold styling, the TXi comes equipped with an ISOTTA steering wheel, acrylic walk-through windscreen, dry storage with a USB charging port, side combing pads with storage beneath and billet aluminum grab handles, folding ski racks and storage compartment lids. Choose between a traditional closed bow or seven-seat open bow, or select the elite Malibu Open Edition in either bow option.',
                        'third_section_image'        => [
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Side.png',
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Front.png',
                            'https://www.malibuboatsasia.com/images/2018/TXi/boat/TXI_Openbow_Rear.png',
                        ],
                        'third_section_glance'       => [
                            'hull_length'  => '20\'6\"/6.25 M',
                            'beam'         => '95\"/2.41 M',
                            'draft'        => '22\"/0.56 M',
                            'max_capacity' => '7',
                        ],
                        'fourth_section_video'       => '233206915',
                    ]
                ),
            ]
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_country = $request->country;
        $boat          = Boats::create($request->except('_token'));
        
        if ($input_country == '0') {
            $count_countries = Countries::count();
            for ($country = 1; $country <= $count_countries; $country++) {
                $this->create_boat($boat->id, $country);
            }
        } else {
            $this->create_boat($boat->id, $input_country);
        }
        
        return redirect()
            ->route('boats.index')
            ->with('update_status', true);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boat = Boats::find($id);
        
        return view('back-end.boats.edit', compact('boat'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Boats::find($id)->update($request->except('_token', '_method'));
        
        return redirect()
            ->route('boats.index')
            ->with('update_status', true);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Boats::destroy($id);
    }
}
