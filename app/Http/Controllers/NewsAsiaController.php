<?php

/* ===== Explain this file =====
This file use for manage news asia by admin only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsAsiaController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where('country_id', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        
        return view('back-end.news-asia.index', compact('news'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.news-asia.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news_header_local      = $request->news_header_local;
        $news_description_local = $request->news_description_local;
        $news_header_en         = $request->news_header_en;
        $news_description_en    = $request->news_description_en;
        $cover_image            = $request->file('cover_image');
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            News::create(
                [
                    'country_id'             => 1, // Asia
                    'cover_image'            => $path,
                    'news_header_local'      => $news_header_local,
                    'news_description_local' => $news_description_local,
                    'news_header_en'         => $news_header_en,
                    'news_description_en'    => $news_description_en,
                    'home_active'            => 0,
                ]
            );
        }
        
        return redirect()->route('news-asia.index')->with(
            'insert_status', true
        );
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        
        return view('back-end.news-asia.edit', compact('news'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news_header_en      = $request->news_header_en;
        $news_description_en = $request->news_description_en;
        $cover_image         = $request->file('cover_image');
        
        $news = News::find($id);
        
        if ($cover_image) {
            $path = $cover_image->store('dealer/news');
            $news->update(
                [
                    'cover_image' => $path,
                ]
            );
        }
        
        $news->update(
            [
                'news_header_en'      => $news_header_en,
                'news_description_en' => $news_description_en,
            ]
        );
        
        return redirect()->route('news-asia.index')->with(
            'update_status', true
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::destroy($id);
    }
}
