<?php

/* ===== Explain this file =====
This file use for front-end only.
===== End Explain ===== */

namespace App\Http\Controllers;

use App\Models\AboutMalibu;
use App\Models\Countries;
use App\Models\DealerProfile;
use App\Models\ExploreBoats;
use App\Models\GoogleMaps;
use App\Models\Inventories;
use App\Models\News;
use Illuminate\Http\Request;
use App\Models\Boats;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    
    public function __construct()
    {
        //        clientSessionCountryId();
    }
    
    public function setLangEn()
    {
        clientSessionCountryId();
        session(['flag_en_' . config('country')->short_name => true]);
        
        return redirect()->back();
    }
    
    public function setLangLocal()
    {
        clientSessionCountryId();
        session(['flag_en_' . config('country')->short_name => false]);
        
        return redirect()->back();
    }
    
    public function session_contact_dealer_store(Request $request)
    {
        session(['form-section' => $request->active_form_section]);
    }
    
    public function home()
    {
        if (session('ipstack') === true) {
            if (ipstack()) {
                $ipstack = ipstack();
                $country = \App\Models\Countries::where(
                    'country_name',
                    $ipstack->country_name
                )->first();
            
                if ($country) { // found country in database
                    $country_name = str_replace(
                        ' ', '-', strtolower($country->country_name)
                    );
                
                    config()->set(
                        'country', \App\Models\Countries::find($country->id)
                    );
                    session(['country_id' => $country->id]);
                    session(['ipstack' => $country_name]);
                
                    return redirect(
                        '/' . session('ipstack')
                    );
                } else {
                    session(['ipstack' => false]);
                }
            }
        }
    
        if (session('ipstack') === false || is_string(session('ipstack'))) {
            clientSessionCountryId();
        }
        
        /* check country languages switch */
        if (config('country')->lang_switch == 0) {
            $short_name = config('country')->short_name;
            if (is_null($short_name)) {
                $short_name = 'asia';
            }
            session(
                [
                    'flag_en_' . $short_name => true,
                ]
            );
        } else {
            if ( ! session()->has('flag_en_' . config('country')->short_name)) {
                session(
                    [
                        'flag_en_' . config('country')->short_name => false,
                    ]
                );
            }
        }
        /* end check country */
        
        $boats = Boats::all();
        $news  = News::where('country_id', config('country')->id)
            ->orderBy('created_at', 'desc')
            ->where('home_active', 1)
            ->limit(2)
            ->get();
        
        return view('front-end.home', compact('boats', 'news'));
    }
    
    public function only_malibu()
    {
        clientSessionCountryId();
    
        if (session('flag_en_' . config('country')->short_name) == true) {
            $about_malibu = AboutMalibu::where('country_id', 1)->first();
        } else {
            $about_malibu = AboutMalibu::where(
                'country_id', clientSessionCountryId()
            )
                ->first();
        }
        
        return view('front-end.only-malibu', compact('about_malibu'));
    }
    
    public function news()
    {
        clientSessionCountryId();
        
        $news = News::where('country_id', clientSessionCountryId())
            ->orderBy('created_at', 'desc')->get();
        
        if (config('country')->country_name !== 'asia') {
            $news = $news->where('create_approve', 1);
        }
        
        $showMore  = false;
        $totalNews = $news->count();
        $news      = $news->slice(0, 3);
        
        if ($totalNews > count($news)) {
            $showMore = true;
        }
        
        return view('front-end.news', compact('news', 'showMore'));
    }
    
    public function news_detail($country_name, $id)
    {
        clientSessionCountryId();
        
        $news = News::find($id);
    
        if (config('country')->lang_switch === 0) {
            $data = [
                'cover_image'      => $news->cover_image,
                'news_header'      => $news->news_header_en,
                'news_description' => $news->news_description_en,
            ];
        } else {
            if (session('flag_en_' . config('country')->short_name)
                || config('country')->country_name == 'asia'
            ) {
                $data = [
                    'cover_image'      => $news->cover_image,
                    'news_header'      => $news->news_header_en,
                    'news_description' => $news->news_description_en,
                ];
            } else {
                $data = [
                    'cover_image'      => $news->cover_image,
                    'news_header'      => $news->news_header_local,
                    'news_description' => $news->news_description_local,
                ];
            }
        }
        
        return view('front-end.news_detail', compact('data'));
    }
    
    public function boat_detail($country_name, $boat_model)
    {
        clientSessionCountryId();
        
        $model        = str_replace('-', ' ', $boat_model);
        $boat         = Boats::where('model', $model)->first();
        $explore_boat = ExploreBoats::where('boat_id', $boat->id)->where(
            'country_id', get_country_id()
        )->first();
        
        if (session('flag_en_' . config('country')->short_name) == true) {
            $explore_boat = json_decode($explore_boat->lang_en);
        } else {
            $explore_boat = json_decode($explore_boat->lang_country);
        }
        
        return view('front-end.boat-detail', compact('boat', 'explore_boat'));
    }
    
    public function find_dealer()
    {
        clientSessionCountryId();
        
        $dealer_profile = DealerProfile::all();
        
        return view('front-end.find_dealer', compact('dealer_profile'));
    }
    
    public function contact_dealer()
    {
        clientSessionCountryId();
        
        if (clientSessionCountryId() == 1) {
            return redirect('/asia');
        }
        
        $dealer_profile = DealerProfile::where(
            'dealer_id', clientGetDealerProfile()->id
        )
            ->first();
        $google_maps    = GoogleMaps::where(
            'country_id', config('country')->id
        )
            ->first();
        
        return view(
            'front-end.contact_dealer', compact('dealer_profile', 'google_maps')
        );
    }
    
    public function all_boats()
    {
        clientSessionCountryId();
        
        $boats = Boats::all();
        
        return view('front-end.all-boats', compact('boats'));
    }
    
    public function inventory()
    {
        clientSessionCountryId();
        
        $inventories = Inventories::where(
            'dealer_id', clientGetDealerProfile()->id
        )
            ->orderBy('created_at', 'desc')
            ->get();
        
        return view('front-end.inventory', compact('inventories'));
    }
    
    public function inventory_detail($country_name, $id)
    {
        clientSessionCountryId();
        
        $inventory = Inventories::find($id);
        
        return view('front-end.inventory_detail', compact('inventory'));
    }
}
