<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';

    protected $guarded = [];
    
    public function dealer()
    {
        return $this->hasOne('App\Models\Dealers', 'country_id', 'id');
    }
}
