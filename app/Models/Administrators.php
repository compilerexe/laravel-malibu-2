<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Administrators extends Model
{
    protected $table = 'administrators';

    protected $fillable = [
        'username',
        'password',
        'email'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];
}
