<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    
    use SoftDeletes;
    
    protected $table = 'news';
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    
    public function country()
    {
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }
}
