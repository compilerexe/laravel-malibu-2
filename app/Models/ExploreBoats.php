<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExploreBoats extends Model
{
    protected $table = 'explore_boats';

    protected $guarded = [];

    public function boat()
    {
        return $this->belongsTo('App\Models\Boats', 'boat_id');
    }
}
