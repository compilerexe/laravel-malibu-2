<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dealers extends Model
{
    protected $table = 'dealers';
    
    protected $guarded = [];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    public function getEmailAttribute()
    {
        $email = '-';

        if (!empty($this->attributes['email']) > 0) {
            $email = $this->attributes['email'];
        }

        return $email;
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Countries', 'country_id');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\DealerProfile', 'id');
    }
}
