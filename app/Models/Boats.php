<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boats extends Model
{
    
    protected $table = 'boats';
    
    protected $guarded = [];
    
    public function explore_boat()
    {
        return $this->hasOne('App\Models\ExploreBoats', 'id');
    }
}
