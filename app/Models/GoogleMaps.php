<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleMaps extends Model
{
    protected $table = 'google_maps';
    
    protected $guarded = [];
}
