<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventories extends Model
{
    
    use SoftDeletes;
    
    protected $table = 'inventories';
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    
    public function boat()
    {
        return $this->belongsTo('App\Models\Boats', 'boat_id');
    }
}
