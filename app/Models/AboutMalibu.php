<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutMalibu extends Model
{
    protected $table = 'about_malibu';
    
    protected $guarded = [];
}
