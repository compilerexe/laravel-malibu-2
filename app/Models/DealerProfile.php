<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerProfile extends Model
{
    protected $table = 'dealer_profiles';

    protected $guarded = [];

    public function dealer()
    {
        return $this->belongsTo('App\Models\Dealers');
    }

    public function fullAddressEnglish()
    {
        $address = $this->attributes['name_en'] . "<br>" . $this->attributes['address_en'] . "<br>" . $this->attributes['telephone'] . "<br>" . $this->attributes['email'];

        return $address;
    }

    public function fullAddressLocal()
    {
        $address = $this->attributes['name_local'] . "<br>" . $this->attributes['address_local'] . "<br>" . $this->attributes['telephone'] . "<br>" . $this->attributes['email'];

        return $address;
    }
    
    public function getEmailOnly()
    {
        return str_replace('Email: ', '', $this->attributes['email']);
    }
}
