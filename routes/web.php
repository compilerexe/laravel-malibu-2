<?php
Route::get('/', 'HomeController@home')->name('home');

$client_route = false;
foreach (\App\Models\Countries::all() as $country) {
    $path = explode('/', \Request::path());
    if (strtolower(str_replace(' ', '-', $country->country_name)) === $path[0]) {
        $client_route = true;
    }
}

if (auth()->check() || auth()->guard('dealer')->check()) {
    return redirect('/admin');
} else {
    if ($client_route) {
        /* ----- Switch Language ----- */
        Route::get('{country_name}/set/lang/en', 'HomeController@setLangEn');
        Route::get('{country_name}/set/lang/local', 'HomeController@setLangLocal');
        
        /* ----- Countries ----- */
        Route::get('/{country_name}', 'HomeController@home');
        Route::get('/{country_name}/news', 'HomeController@news')->name('news');
        Route::get('/{country_name}/news/{id}', 'HomeController@news_detail')->name('news_detail');
        Route::get('/{country_name}/boat/{boat_model}', 'HomeController@boat_detail');
        Route::get('/{country_name}/about-malibu', 'HomeController@only_malibu')->name('about-malibu');
        Route::get('/{country_name}/find-a-dealer', 'HomeController@find_dealer')->name('find-dealer');
        Route::get('/{country_name}/contact-dealer', 'HomeController@contact_dealer')->name('contact-dealer');
        Route::get('/{country_name}/all-boats', 'HomeController@all_boats')->name('all-boats');
        Route::get('/{country_name}/inventory', 'HomeController@inventory')->name('inventory');
        Route::get('/{country_name}/inventory/{id}', 'HomeController@inventory_detail')->name('inventory_detail');
    }
    
    /* ----- Login for admin or dealer ----- */
    Route::get('admin', 'BackEndController@login')->name('login');
    Route::post('login', 'BackEndController@post_login')->name('post.login');
    
    /* ----- Contact Dealer ----- */
    Route::post('/session/contact-dealer/store', 'HomeController@session_contact_dealer_store')->name('session.contact-dealer.store');
    
    /* ----- TinyMCE ----- */
    Route::post('tinymce/news/upload', 'TinyMCEController@post_news_upload');
    
    /* ----- Send Mail ----- */
    Route::prefix('mail/send')->group(function () {
        Route::post('contact-us', 'SendMailController@postContactUs')->name('mail.send.contact-us');
        Route::prefix('contact-dealer')->group(function () {
            Route::post('free-test-drive', 'SendMailController@postContactDealerFreeTestDrive')->name('mail.send.contact-dealer.free-test-drive');
            Route::post('request-catalog', 'SendMailController@postContactDealerRequestCatalog')->name('mail.send.contact-dealer.request-catalog');
            Route::post('general', 'SendMailController@postContactDealerGeneral')->name('mail.send.contact-dealer.general');
            Route::post('inventory', 'SendMailController@postContactDealerInventory')->name('mail.send.contact-dealer.inventory');
        });
    });
    
    /* ----- API ----- */
    Route::prefix('api')->group(function () {
        Route::post('set/home_active', 'APIController@set_home_active')->name('api.set.home_active');
        Route::post('fetch/news/paginate', 'APIController@fetchNewsPaginate')->name('api.news.paginate');
        Route::post('fetch-explore-boats', 'APIController@fetch_explore_boats');
        Route::get('all-boats/nav-description', 'APIController@fetch_nav_description');
    });
    
    /* ----- Start Middleware ----- */
    Route::middleware('auth.admin')->group(
        function () {
            Route::prefix('manage')->group(
                function () {
                    Route::resource('news-asia', 'NewsAsiaController');
                    Route::resource('profile', 'ProfileController');
                    Route::post('password_update/{id}', 'ProfileController@password_update')->name('admin.password.update');
                    Route::resource('language', 'LanguageController');
                    Route::resource('boats', 'BoatController');
                    Route::resource('dealers', 'DealerController');
                    
                    Route::prefix('content')->group(
                        function () {
                            Route::get('/', 'ContentController@main')->name('manage.content.index');
                            Route::get('panel/{country_id}', 'ContentController@panel')->name('manage.content.panel');
                            Route::post('about_malibu/{country_id}', 'ContentController@post_about_malibu')->name('manage.content.about_malibu');
                            Route::post('lang/{country_id}/{tag}', 'ContentController@post_lang')->name('manage.content.lang');
                            Route::resource('explore_boat', 'ExploreBoatController')->names([
                                'index' => 'manage.content.explore_boat.index',
                                'edit' => 'manage.content.explore_boat.edit',
                                'update' => 'manage.content.explore_boat.update',
                                'destroy' => 'manage.content.explore_boat.destroy',
                            ]);
                        }
                    );
                    
                    Route::prefix('dealer')->group( // url : /manage/dealer/...
                        function () {
                            Route::prefix('profile')->group( // url : /manage/dealer/profile
                                function () {
                                    Route::name('manage.dealer.profile.')->group(function () { // route name
                                        // url : /manage/dealer/profile/{dealer_id}
                                        Route::get('{dealer_id}', 'ManageDealerController@getProfile')->name('get');
                                        Route::post('/', 'ManageDealerController@postProfile')->name('post');
                                    });
                                }
                            );
                            
                            Route::prefix('google_maps')->group( // url : /manage/dealer/google_maps
                                function () {
                                    Route::name('manage.dealer.google_maps.')->group(function () { // route name
                                        // url : /manage/dealer/google_maps/{id}
                                        Route::get('/', 'ManageDealerController@getGoogleMaps')->name('get');
                                        Route::post('{id}', 'ManageDealerController@postGoogleMaps')->name('post');
                                    });
                                }
                            );
                            
                            Route::prefix('news')->group( // url : /manage/dealer/news
                                function () {
                                    Route::name('manage.dealer.news.')->group(function () { // route name
                                        // url : /manage/dealer/news/...
                                        Route::get('/index/{dealer_id}', 'ManageDealerController@getNews')->name('index');
                                        Route::get('/create', 'ManageDealerController@getNewsCreate')->name('create');
                                        Route::get('/edit/{id}', 'ManageDealerController@getNewsEdit')->name('edit');
                                        
                                        //									/* POST, PUT, DELETE */
                                        Route::post('/store', 'ManageDealerController@postNewsStore')->name('store');
                                        Route::put('/edit/{id}', 'ManageDealerController@postNewsUpdate')->name('update');
                                        Route::delete('/destroy/{id}', 'ManageDealerController@postNewsDestroy')->name('destroy');
                                        
                                        /* Send request to admin */
                                        Route::post('/request/create/{id}', 'ManageDealerController@postRequestNewsCreate')->name('request.create');
                                        Route::post('/request/update/{id}', 'ManageDealerController@postRequestNewsUpdate')->name('request.update');
                                        Route::post('/request/remove/{id}', 'ManageDealerController@postRequestNewsRemove')->name('request.remove');
                                    });
                                }
                            );
                            
                            Route::prefix('inventory')->group( // url : /manage/dealer/inventory
                                function () {
                                    Route::name('manage.dealer.inventory.')->group(function () { // route name
                                        // url : /manage/dealer/inventory/...
                                        Route::get('/index/{dealer_id}', 'ManageDealerController@getInventory')->name('index');
                                        Route::get('/create', 'ManageDealerController@getInventoryCreate')->name('create');
                                        Route::get('/edit/{id}', 'ManageDealerController@getInventoryEdit')->name('edit');
                                        
                                        /* POST, PUT, DELETE Inventory */
                                        Route::post('/store', 'ManageDealerController@postInventoryStore')->name('store');
                                        Route::put('/edit/{id}', 'ManageDealerController@postInventoryUpdate')->name('update');
                                        Route::delete('/destroy/{id}', 'ManageDealerController@postInventoryDestroy')->name('destroy');
                                    });
                                }
                            );
                        }
                    );
                }
            );
            
            Route::get('logout', 'BackEndController@logout')->name('logout');
        }
    );
    
    Route::middleware('auth.dealer')->group(
        function () {
            Route::name('dealer.')->group(
                function () {
                    Route::prefix('dealer')->group(
                        function () {
                            Route::get('profile', 'BackDealerController@profile')->name('profile');
                            Route::post('profile/update', 'BackDealerController@post_profile')->name('profile.update');
                            Route::get('google_maps', 'ContentController@get_google_maps')->name('content.get.google_maps');
                            Route::post('google_maps/{id}', 'ContentController@post_google_maps')->name('content.post.google_maps');
                            Route::prefix('news')->group(
                                function () {
                                    Route::name('news.')->group(
                                        function () {
                                            Route::get('/', 'BackDealerController@news')->name('index');
                                            Route::post('/', 'BackDealerController@news_store')->name('store');
                                            Route::get('create', 'BackDealerController@news_create')->name('create');
                                            Route::get('{id}/edit', 'BackDealerController@news_edit')->name('edit');
                                            Route::put('{id}/edit', 'BackDealerController@news_update')->name('update');
                                            Route::delete('{id}/destroy', 'BackDealerController@news_destroy')->name('destroy');
                                        }
                                    );
                                }
                            );
                            
                            Route::prefix('inventory')->group(
                                function () {
                                    Route::name('inventory.')->group(
                                        function () {
                                            Route::get('/', 'BackDealerController@inventory')->name('index');
                                            Route::post('/', 'BackDealerController@inventory_store')->name('store');
                                            Route::get('create', 'BackDealerController@inventory_create')->name('create');
                                            Route::get('{id}/edit', 'BackDealerController@inventory_edit')->name('edit');
                                            Route::put('{id}/update', 'BackDealerController@inventory_update')->name('update');
                                            Route::delete('{id}/destroy', 'BackDealerController@inventory_destroy')->name('destroy');
                                            Route::delete('{id}/photo/{sort}', 'BackDealerController@inventory_photo_remove')->name('photo.remove');
                                        }
                                    );
                                }
                            );
                            
                            Route::get('account_setting', 'BackDealerController@account_setting')->name('account_setting');
                            Route::post('account_setting', 'BackDealerController@post_account_setting')->name('post_account_setting');
                            Route::post('password_update/{id}', 'ProfileController@password_update')->name('password.update');
                            Route::get('logout', 'BackEndController@dealer_logout')->name('logout');
                        }
                    );
                }
            );
        }
    );
    /* ----- End Middleware ------------ */
}
