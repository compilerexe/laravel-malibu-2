<?php
Route::get('/', 'HomeController@home')->name('home');
/* ----- API ----- */
Route::post('/api/set/home_active', 'APIController@set_home_active')->name('api.set.home_active');
Route::post('/api/fetch/news/paginate', 'APIController@fetchNewsPaginate')->name('api.news.paginate');
Route::post('/api/fetch-explore-boats', 'APIController@fetch_explore_boats');
Route::get('/api/all-boats/nav-description', 'APIController@fetch_nav_description');

/* ----- Start Middleware ----- */
Route::get('admin', 'BackEndController@login')->name('login');
Route::post('login', 'BackEndController@post_login')->name('post.login');
Route::middleware('auth.admin')->group(
    function () {
        
        /* ===== Manage ===== */
        Route::get(
            'access/manage/dealer/{dealer_id}/profile',
            'ManageDealerController@getProfile'
        )->name('access.manage.dealer.profile');
        
        /* POST Profile */
        Route::post(
            'access/manage/dealer/profile', 'ManageDealerController@postProfile'
        )->name('post.access.manage.dealer.profile');
        
        /* News Request */
        Route::name('access.manage.dealer.news.')->group(
            function () {
                Route::get(
                    'access/manage/dealer/{dealer_id}/news',
                    'ManageDealerController@getNews'
                )->name('index');
                Route::get(
                    'access/manage/dealer/news/create',
                    'ManageDealerController@getNewsCreate'
                )->name('create');
                Route::get(
                    'access/manage/dealer/news/{id}/edit',
                    'ManageDealerController@getNewsEdit'
                )->name('edit');
                
                /* POST, PUT, DELETE News */
                Route::post(
                    'access/manage/dealer/news',
                    'ManageDealerController@postNewsStore'
                )->name('store');
                Route::put(
                    'access/manage/dealer/news/{id}/edit',
                    'ManageDealerController@postNewsUpdate'
                )->name('update');
                Route::delete(
                    'access/manage/dealer/news/{id}/destroy',
                    'ManageDealerController@postNewsDestroy'
                )->name('destroy');
                
                Route::post(
                    'request/news/create/{id}',
                    'ManageDealerController@postRequestNewsCreate'
                )->name('request.create');
                Route::post(
                    'request/news/update/{id}',
                    'ManageDealerController@postRequestNewsUpdate'
                )->name('request.update');
                Route::post(
                    'request/news/remove/{id}',
                    'ManageDealerController@postRequestNewsRemove'
                )->name('request.remove');
            }
        );
        
        /* Inventory Request */
        Route::name('access.manage.dealer.inventory.')->group(
            function () {
                Route::get(
                    'access/manage/dealer/{dealer_id}/inventory',
                    'ManageDealerController@getInventory'
                )->name('index');
                Route::get(
                    'access/manage/dealer/create',
                    'ManageDealerController@getInventoryCreate'
                )->name('create');
                Route::get(
                    'access/manage/dealer/{id}/edit',
                    'ManageDealerController@getInventoryEdit'
                )->name('edit');
                
                /* POST, PUT, DELETE Inventory */
                Route::post(
                    'access/manage/dealer/inventory',
                    'ManageDealerController@postInventoryStore'
                )->name('store');
                Route::put(
                    'access/manage/dealer/{id}/edit',
                    'ManageDealerController@postInventoryUpdate'
                )->name('update');
                Route::delete(
                    'access/manage/dealer/{id}/destroy',
                    'ManageDealerController@postInventoryDestroy'
                )->name('destroy');
            }
        );
        
        Route::name('access.manage.dealer.google_maps.')->group(
            function () {
                Route::get(
                    'access/manage/dealer/google_maps',
                    'ManageDealerController@getGoogleMaps'
                )->name('index');
                Route::post(
                    'access/manage/dealer/google_maps/{id}',
                    'ManageDealerController@postGoogleMaps'
                )->name('update');
            }
        );
        /* ================== */
        
        Route::get('logout', 'BackEndController@logout')->name('logout');
        
        Route::prefix('manage')->group(
            function () {
                
                Route::resource('news-asia', 'NewsAsiaController');
                
                Route::resource('profile', 'ProfileController');
                Route::post(
                    'password_update/{id}', 'ProfileController@password_update'
                )->name('password.update');
                Route::resource('language', 'LanguageController');
                Route::resource('boats', 'BoatController');
                Route::resource('dealers', 'DealerController');
                
                Route::prefix('content')->group(
                    function () {
                        Route::get('/', 'ContentController@main')->name(
                            'content'
                        );
                        Route::get(
                            'panel/{country_id}', 'ContentController@panel'
                        )->name('content.panel');
                    }
                );
                
                Route::prefix('content')->group(
                    function () {
                        Route::post(
                            'about_malibu/{country_id}',
                            'ContentController@post_about_malibu'
                        )->name('content.post.about_malibu');
                        Route::post(
                            'contact_dealer/{country_id}',
                            'ContentController@post_contact_dealer'
                        )->name('content.post.contact_dealer');
                        Route::post(
                            'find_dealer/{country_id}',
                            'ContentController@post_find_dealer'
                        )->name('content.post.find_dealer');
                        Route::post(
                            'footer_contact_dealer/{country_id}',
                            'ContentController@post_footer_contact_dealer'
                        )->name('content.post.footer_contact_dealer');
                        Route::post(
                            'google_maps/{id}',
                            'ContentController@post_google_maps'
                        )->name('content.post.google_maps');
                        Route::post(
                            'lang/{country_id}/{tag}',
                            'ContentController@post_lang'
                        )->name('content.post.lang');
                        Route::post(
                            'social_link/{country_id}',
                            'ContentController@post_social_link'
                        )->name('content.post.social_link');
                        
                        Route::resource(
                            'explore_boat', 'ExploreBoatController'
                        );
                        Route::resource('news', 'NewsController');
                        Route::resource('inventories', 'InventoryController');
                    }
                );
            }
        );
    }
);

Route::middleware('auth.dealer')->group(
    function () {
        Route::name('dealer.')->group(
            function () {
                Route::prefix('dealer')->group(
                    function () {
                        
                        Route::get('profile', 'BackDealerController@profile')
                            ->name('profile');
                        Route::post(
                            'profile', 'BackDealerController@post_profile'
                        )->name('profile');
                        
                        Route::prefix('google_maps')->group(
                            function () {
                                Route::get(
                                    '/', 'BackDealerController@getGoogleMaps'
                                )->name('google_maps.index');
                            }
                        );
                        
                        Route::prefix('news')->group(
                            function () {
                                Route::name('news.')->group(
                                    function () {
                                        Route::get(
                                            '/', 'BackDealerController@news'
                                        )->name('index');
                                        Route::post(
                                            '/',
                                            'BackDealerController@news_store'
                                        )->name('store');
                                        Route::get(
                                            'create',
                                            'BackDealerController@news_create'
                                        )->name('create');
                                        Route::get(
                                            '{id}/edit',
                                            'BackDealerController@news_edit'
                                        )->name('edit');
                                        Route::put(
                                            '{id}/edit',
                                            'BackDealerController@news_update'
                                        )->name('update');
                                        Route::delete(
                                            '{id}/destroy',
                                            'BackDealerController@news_destroy'
                                        )->name('destroy');
                                    }
                                );
                            }
                        );
                        
                        Route::prefix('inventory')->group(
                            function () {
                                Route::name('inventory.')->group(
                                    function () {
                                        Route::get(
                                            '/',
                                            'BackDealerController@inventory'
                                        )->name('index');
                                        Route::post(
                                            '/',
                                            'BackDealerController@inventory_store'
                                        )->name('store');
                                        Route::get(
                                            'create',
                                            'BackDealerController@inventory_create'
                                        )->name('create');
                                        Route::get(
                                            '{id}/edit',
                                            'BackDealerController@inventory_edit'
                                        )->name('edit');
                                        Route::put(
                                            '{id}/update',
                                            'BackDealerController@inventory_update'
                                        )->name('update');
                                        Route::delete(
                                            '{id}/destroy',
                                            'BackDealerController@inventory_destroy'
                                        )->name('destroy');
                                        Route::delete(
                                            '{id}/photo/{sort}',
                                            'BackDealerController@inventory_photo_remove'
                                        )->name('photo.remove');
                                    }
                                );
                            }
                        );
                        
                        Route::get(
                            'account_setting',
                            'BackDealerController@account_setting'
                        )->name('account_setting');
                        Route::post(
                            'account_setting',
                            'BackDealerController@post_account_setting'
                        )->name('post_account_setting');
                        
                        Route::get('logout', 'BackEndController@dealer_logout')
                            ->name(
                                'logout'
                            );
                    }
                );
            }
        );
    }
);

/* ----- End Middleware ------------ */

Route::post(
    '/session/contact-dealer/store',
    'HomeController@session_contact_dealer_store'
)->name('session.contact-dealer.store');

/* ----- Switch Language ----- */
Route::get('{country_name}/set/lang/en', 'HomeController@setLangEn');
Route::get('{country_name}/set/lang/local', 'HomeController@setLangLocal');
/* ----- End Switch Language ----- */

/* ----- Countries ----- */
Route::get('/{country_name}', 'HomeController@home');
Route::get('/{country_name}/news', 'HomeController@news')->name('news');
Route::get('/{country_name}/news/{id}', 'HomeController@news_detail')->name(
    'news_detail'
);
Route::get('/{country_name}/boat/{boat_model}', 'HomeController@boat_detail');
Route::get('/{country_name}/about-malibu', 'HomeController@only_malibu')->name(
    'about-malibu'
);
Route::get('/{country_name}/find-a-dealer', 'HomeController@find_dealer')->name(
    'find-dealer'
);
Route::get('/{country_name}/contact-dealer', 'HomeController@contact_dealer')
    ->name('contact-dealer');
Route::get('/{country_name}/blog', 'HomeController@blog')->name('blog');
Route::get('/{country_name}/blog/post/{id}', 'HomeController@blog_post')->name(
    'blog-post'
);
Route::get('/{country_name}/all-boats', 'HomeController@all_boats')->name(
    'all-boats'
);
Route::get('/{country_name}/inventory', 'HomeController@inventory')->name(
    'inventory'
);
Route::get('/{country_name}/inventory/{id}', 'HomeController@inventory_detail')
    ->name('inventory_detail');
/* ----- End Countries ----- */

/* TinyMCE */

Route::post('tinymce/news/upload', 'TinyMCEController@post_news_upload');

/* End TinyMCE */


/* User Send Mail */
Route::post('mail/send/contact-us', 'SendMailController@postContactUs')->name(
    'mail.send.contact-us'
);

Route::post(
    'mail/send/contact-dealer/free-test-drive',
    'SendMailController@postContactDealerFreeTestDrive'
)->name('mail.send.contact-dealer.free-test-drive');

Route::post(
    'mail/send/contact-dealer/request-catalog',
    'SendMailController@postContactDealerRequestCatalog'
)->name('mail.send.contact-dealer.request-catalog');

Route::post(
    'mail/send/contact-dealer/general',
    'SendMailController@postContactDealerGeneral'
)->name('mail.send.contact-dealer.general');


Route::post(
    'mail/send/contact-dealer/inventory',
    'SendMailController@postContactDealerInventory'
)->name('mail.send.contact-dealer.inventory');
/* End User Send Mail */
